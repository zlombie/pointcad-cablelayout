﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using FlexNet;
using InstrumentationLayout.Interfaces;
using Microsoft.Win32;

namespace InstrumentationLayout
{
	public partial class CableLayoutToolbar : UserControl
	{
		public CableLayoutToolbar()
		{
			InitializeComponent();
            var pid = Process.GetCurrentProcess().Id;
            btnMain.Click += (sender, args) => { StartApplication(@"Layout.exe", null, $"pid={pid}", btnMain.ToolTipText); };
            btnLoadDwg.Click += (sender, args) => { StartApplication(@"Layout.exe", null, $"pid={pid} Dwgload", btnLoadDwg.ToolTipText); };
            btnUpdateDwg.Click += (sender, args) => { StartApplication(@"Layout.exe", null, $"pid={pid} Dwgupdate", btnUpdateDwg.ToolTipText); };
            btnCalculate.Click += (sender, args) => { StartApplication(@"Layout.exe", null, $"pid={pid} Calc", btnCalculate.ToolTipText); };
            btnSettings.Click+= (sender, args) => { StartApplication(@"Layout.exe", null, $"pid={pid} Settings", btnSettings.ToolTipText); };
            // завершение загрузки компонента
            base.OnCreateControl();
		}

        public int GetToolStripWidth()
        {
            return 16 + toolStrip1.Items.Cast<ToolStripItem>().Sum(toolStripItem => toolStripItem.Width);
        }

        /// <summary>
        /// Запуск приложения.
        /// </summary>
        /// <param name="fileName">Имя исполняемого файла</param>
        /// <param name="subFolder">Под-папка или путь</param>
        /// <param name="args">Строка аргументов</param>
        /// <param name="description">Описание для вывода в окно сообщений E3.series.</param>
        private static void StartApplication(string fileName, string subFolder = "", string args = "", string description = "")
		{
			try
			{
				// Не задано имя исполняемого файла
				if (string.IsNullOrEmpty(fileName))
				{
					try
					{
						//E3Connector.Instance.E3Application?.PutWarning(0, $"{DateTime.Now}. Не задано имя исполняемого файла для кнопки!");
						//E3Connector.Instance.Dispose();
					}
					catch (Exception)
					{
					}
					return;
				}

				// Путь к библиотеке
				string libraryPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

				string directoryName = Path.GetDirectoryName(libraryPath);
				if (string.IsNullOrEmpty(directoryName))
				{
					try
					{
						//E3Connector.Instance.E3Application?.PutWarning(0, $"{DateTime.Now}. Не корректный путь к библиотеке \"{libraryPath}\"!");
						//E3Connector.Instance.Dispose();
					}
					catch (Exception)
					{
					}

					return;
				}

				string libraryDirPath = Uri.UnescapeDataString(directoryName);

				string fullPath = string.IsNullOrEmpty(subFolder)
									  ? Path.Combine(libraryDirPath, fileName)
									  : Path.Combine(libraryDirPath, subFolder, fileName);

				if (File.Exists(fullPath))
				{
					try
					{
						//E3Connector.Instance.E3Application?.PutMessage($"{DateTime.Now}. Запуск \"{description}\"!");
						//E3Connector.Instance.Dispose();
					}
					catch (Exception)
					{
					}
					if (!string.IsNullOrEmpty(args))
					{
                        Process.Start(fullPath, args);
					}
					else
					{
						Process.Start(fullPath);
					}
				}
				else
				{
					try
					{
						//E3Connector.Instance.E3Application?.PutWarning(0, $"{DateTime.Now}. Не найден путь к исполняемому файлу \"{fullPath}\"!");
						//E3Connector.Instance.Dispose();
					}
					catch (Exception)
					{
					}
				}
			}
#pragma warning disable CS0168 // Переменная "e" объявлена, но ни разу не использована.
			catch (Exception e)
#pragma warning restore CS0168 // Переменная "e" объявлена, но ни разу не использована.
			{
				try
				{
					//E3Connector.Instance.E3Application?.PutWarning(0, $"{DateTime.Now}. Ошибка: {e.Message}!");
					//E3Connector.Instance.Dispose();
				}
				catch (Exception)
				{
				}
			}
		}


    }
}