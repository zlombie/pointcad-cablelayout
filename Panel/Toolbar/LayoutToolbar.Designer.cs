﻿namespace InstrumentationLayout
{
	partial class CableLayoutToolbar
	{
		/// <summary> 
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором компонентов

		/// <summary> 
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CableLayoutToolbar));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnMain = new System.Windows.Forms.ToolStripButton();
            this.btnLoadDwg = new System.Windows.Forms.ToolStripButton();
            this.btnUpdateDwg = new System.Windows.Forms.ToolStripButton();
            this.btnCalculate = new System.Windows.Forms.ToolStripButton();
            this.btnSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMain,
            this.btnLoadDwg,
            this.btnUpdateDwg,
            this.btnCalculate,
            this.btnSettings});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(133, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnMain
            // 
            this.btnMain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMain.Image = ((System.Drawing.Image)(resources.GetObject("btnMain.Image")));
            this.btnMain.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnMain.Name = "btnMain";
            this.btnMain.Size = new System.Drawing.Size(23, 22);
            this.btnMain.Text = "toolStripButton1";
            this.btnMain.ToolTipText = "Главное окно модуля \"Планы расположения\"";
            // 
            // btnLoadDwg
            // 
            this.btnLoadDwg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLoadDwg.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadDwg.Image")));
            this.btnLoadDwg.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnLoadDwg.Name = "btnLoadDwg";
            this.btnLoadDwg.Size = new System.Drawing.Size(23, 22);
            this.btnLoadDwg.Text = "toolStripButton2";
            this.btnLoadDwg.ToolTipText = "Загрузка обновляемой подложки из DXF/DWG файла";
            // 
            // btnUpdateDwg
            // 
            this.btnUpdateDwg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUpdateDwg.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateDwg.Image")));
            this.btnUpdateDwg.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnUpdateDwg.Name = "btnUpdateDwg";
            this.btnUpdateDwg.Size = new System.Drawing.Size(23, 22);
            this.btnUpdateDwg.Text = "toolStripButton3";
            this.btnUpdateDwg.ToolTipText = "Обновление загруженной подложки из DXF/DWG файла";
            // 
            // btnCalculate
            // 
            this.btnCalculate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCalculate.Image = ((System.Drawing.Image)(resources.GetObject("btnCalculate.Image")));
            this.btnCalculate.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(23, 22);
            this.btnCalculate.Text = "toolStripButton4";
            this.btnCalculate.ToolTipText = "Расчет количества материалов";
            // 
            // btnSettings
            // 
            this.btnSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(23, 22);
            this.btnSettings.Text = "toolStripButton5";
            this.btnSettings.ToolTipText = "Настройки модуля \"Планы расположения\"";
            // 
            // CableLayoutToolbar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.toolStrip1);
            this.Name = "CableLayoutToolbar";
            this.Size = new System.Drawing.Size(133, 104);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnMain;
        private System.Windows.Forms.ToolStripButton btnLoadDwg;
        private System.Windows.Forms.ToolStripButton btnUpdateDwg;
        private System.Windows.Forms.ToolStripButton btnCalculate;
        private System.Windows.Forms.ToolStripButton btnSettings;
    }
}
