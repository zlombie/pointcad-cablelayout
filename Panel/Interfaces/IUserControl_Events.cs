﻿using System.Runtime.InteropServices;


namespace InstrumentationLayout.Interfaces
{
	[InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
	[Guid("42542713-08f9-4a32-ae01-f18766f0d600")]
	public interface IUserControl_Events
	{
	}
}