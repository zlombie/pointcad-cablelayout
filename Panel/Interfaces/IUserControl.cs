﻿using System.Runtime.InteropServices;


namespace InstrumentationLayout.Interfaces
{
	[ComVisible(true)]
	[Guid("41409b91-f3f4-42dc-8d7e-871ff32e22f3")]
	public interface IUserControl
	{
		/// <summary>
		/// Метод инициализации средств лицензирования FlexNet
		/// </summary>
		void InitializeFlexNet();

		/// <summary>
		/// Отчет о результатах попытки получить лицензию
		/// </summary>
		/// <returns></returns>
		bool TakeLicenseResultInfo();

		/// <summary>
		/// Метод освобождения ресурсов FlexNet
		/// </summary>
		void DisposeFlexNet();
	}
}