﻿using System;
using System.Windows.Forms;
using E3PlugIn;
using FlexNet;
using System.Diagnostics;
using System.Threading;
using System.Linq;

namespace InstrumentationLayout
{
    public class PlugIn : IE3PlugIn
    {
        /// <summary>
        /// Название панели.
        /// </summary>
        private const string PanelName = "[E3.Instrumentation] План расположения";

        public void Init(IE3InitializationContext context)
        {
            if (!context.CheckFeature(E3Features.FEATURE_DBE) &&
                (context.CheckFeature(E3Features.FEATURE_CABLE) ||
                 context.CheckFeature(E3Features.FEATURE_SCHEMA)))
            {
                InitializeFlexNet();
                if (!FlexNetCheckResult)
                {
                    new Thread(PutFlexNetErrorString).Start();
                    return;
                }
                context.RegisterMainWindowInitEventHandler(EventHandler);
            }
        }

        private void PutFlexNetErrorString()
        {
            int currentId = Process.GetCurrentProcess().Id;

            Thread.Sleep(2000);

            // Индекс попытки
            int index = 0;

            // Максимум 20 попыток через каждую секунду
            while (index < 20)
            {
                ++index;
                if (Process.GetProcesses().FirstOrDefault(ctx => ctx.Id == currentId) == null) return;
                Thread.Sleep(1000);
               
                //using (EcubeApplication ecube = EcubeApplication.ConnectToEcubeApplicationByProcessId(currentId))
                //{
                //    if (ecube == null) continue;

                //    ecube.PutWarning(0, $"Не удалось загрузить панель \"{PanelName}\". Причина: {FlexNetErrorString}");
                //    return;
                //}
            }
        }

        private static void EventHandler(object sender, MainWindowInitEventArgs args)
        {
            try
            {
                CableLayoutToolbar toolBar = new CableLayoutToolbar();
                args.AddToolBar(PanelName, toolBar, toolBar.GetToolStripWidth());
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        ~PlugIn()
        {
            DisposeFlexNet();
        }


        #region FlexNet

        /// <summary>
        /// Имя продукта
        /// </summary>
        private const string FlexNetProductName = "E3-Layout";

        /// <summary>
        /// версия продукта
        /// </summary>
        private const string FlexNetProductVersion = "4.0";


        /// <summary>
        /// Флаг, в котором хранится результат попытки получения лицензии FlexNet
        /// </summary>
        private bool FlexNetCheckResult { get; set; }

        /// <summary>
        /// Ошибка
        /// </summary>
        private string FlexNetErrorString { get; set; }

        /// <summary>
        /// Флаг того, что FlexNet уже был инициализирован
        /// </summary>
        private bool FlexNetWasInitialized { get; set; }


        /// <summary>
        /// Служебные методы для поддержки лицензирования FlexNet.
        /// </summary>
        private void InitializeFlexNet()
        {
            if (!FlexNetWasInitialized)
            {
                // Инициализируем структуры FlexNet
                Helper.Initialize();

                // Пытаемся получить лицензию на продукт по имени FlexNetProductName
                FlexNetCheckResult = Helper.CheckOut(FlexNetProductName, FlexNetProductVersion);
                FlexNetErrorString = Helper.GetErrorString();
                FlexNetWasInitialized = true;
            }
        }

        private void DisposeFlexNet()
        {
            try
            {
                if (FlexNetWasInitialized)
                {
                    FlexNetWasInitialized = false;

                    // освобождаем лицензию - независимо от того,
                    // была она реально нам выделена или нет.
                    Helper.CheckIn(FlexNetProductName);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion
    }
}