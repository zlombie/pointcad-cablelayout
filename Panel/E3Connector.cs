﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;


namespace InstrumentationLayout
{
	/// <summary>
	/// Класс подключения к E3.series
	/// </summary>
	internal sealed class E3Connector : IDisposable
	{
		private E3Connector()
		{
			try
			{
				dynamic ecube = null;

				Hashtable hashtable = GetHashtables();
				if (hashtable.Count == 0) return;

				// Панель - часть процесса E3.series
				int currentId = Process.GetCurrentProcess().Id;
				if (currentId != 0 && hashtable.ContainsKey(currentId))
				{
					ecube = hashtable[currentId];
				}
				else
				{
					// Понять, откуда был запущен модуль: как отдельное приложение или из панели в E3.series
					int parentId = WindowsApi.GetParentProcessId();
					if (parentId != 0 && hashtable.ContainsKey(parentId))
					{
						ecube = hashtable[currentId];
					}
				}

				if (ecube == null)
				{
					E3Application = null;
					return;
				}

				E3Application = ecube;
			}
			catch (Exception)
			{
			}
		}


		public dynamic E3Application { get; }


		private static E3Connector _instance;

		public static E3Connector Instance
		{
			get { return _instance ?? (_instance = new E3Connector()); }
		}


		#region Методы подключения к E3.series

		/// <summary>
		/// Специальный класс для определения окна E3.series в режиме редактора БД.
		/// Examples with Running Object Table (ROT)
		/// Согласно примеру в COM справке от E3.series 2016
		/// </summary>
		[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("00000001-0000-0000-C000-000000000046")]
		private interface IClassFactory
		{
			[PreserveSig]
			int CreateInstance(IntPtr pUnkOuter, ref Guid riid, out IntPtr ppvObject);
		}

		[SuppressMessage("ReSharper", "InconsistentNaming")]
		[SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local")]
		[SuppressMessage("ReSharper", "UnusedMember.Local")]
		[SuppressMessage("ReSharper", "MemberCanBePrivate.Local")]
		[SuppressMessage("ReSharper", "UnusedMember.Global")]
		private static class NativeMethods
		{
			/// <summary>
			/// Returns a pointer to the IRunningObjectTable
			/// interface on the local running object table (ROT).
			/// </summary>
			/// <param name="reserved">This parameter is reserved and must be 0.</param>
			/// <param name="pprot">The address of an IRunningObjectTable* pointer variable
			/// that receives the interface pointer to the local ROT. When the function is
			/// successful, the caller is responsible for calling Release on the interface
			/// pointer. If an error occurs, *pprot is undefined.</param>
			/// <see cref="http://www.pinvoke.net/default.aspx/ole32.GetRunningObjectTable"/>
			/// <returns>This function can return the standard return values E_UNEXPECTED and S_OK.</returns>
			[DllImport("ole32.dll")]
			public static extern int GetRunningObjectTable(uint reserved, out IRunningObjectTable pprot);

			[DllImport("ole32.dll", PreserveSig = false)]
			public static extern void CreateBindCtx(uint reserved, out IBindCtx ppbc);

			[StructLayout(LayoutKind.Sequential, Pack = 1)]
			public struct PROCESS_BASIC_INFORMATION
			{
				public IntPtr ExitStatus;
				public IntPtr PebBaseAddress;
				public IntPtr AffinityMask;
				public IntPtr BasePriority;
				public UIntPtr UniqueProcessId;
				public IntPtr InheritedFromUniqueProcessId;

				public static int Size
				{
					get { return Marshal.SizeOf(typeof(PROCESS_BASIC_INFORMATION)); }
				}
			}

			[Flags]
			public enum ProcessAccessFlags : uint
			{
				All = 0x001F0FFF,
				Terminate = 0x00000001,
				CreateThread = 0x00000002,
				VirtualMemoryOperation = 0x00000008,
				VirtualMemoryRead = 0x00000010,
				VirtualMemoryWrite = 0x00000020,
				DuplicateHandle = 0x00000040,
				CreateProcess = 0x000000080,
				SetQuota = 0x00000100,
				SetInformation = 0x00000200,
				QueryInformation = 0x00000400,
				QueryLimitedInformation = 0x00001000,
				Synchronize = 0x00100000
			}


			[DllImport("kernel32.dll", SetLastError = true)]
			public static extern IntPtr OpenProcess(ProcessAccessFlags processAccess,
													bool bInheritHandle,
													int processId);

			/// <summary>
			/// Получает сведения об указанном процессе.
			/// </summary>
			/// <param name="hProcess"></param>
			/// <param name="pic"></param>
			/// <param name="pbi"></param>
			/// <param name="cb"></param>
			/// <param name="pSize"></param>
			/// <returns></returns>
			[DllImport("NTDLL.DLL", SetLastError = true)]
			public static extern int NtQueryInformationProcess(IntPtr hProcess,
															   PROCESSINFOCLASS pic,
															   ref PROCESS_BASIC_INFORMATION pbi,
															   int cb,
															   out int pSize);

			public enum PROCESSINFOCLASS
			{
				ProcessBasicInformation = 0, // 0, q: PROCESS_BASIC_INFORMATION, PROCESS_EXTENDED_BASIC_INFORMATION
				ProcessQuotaLimits, // qs: QUOTA_LIMITS, QUOTA_LIMITS_EX
				ProcessIoCounters, // q: IO_COUNTERS
				ProcessVmCounters, // q: VM_COUNTERS, VM_COUNTERS_EX
				ProcessTimes, // q: KERNEL_USER_TIMES
				ProcessBasePriority, // s: KPRIORITY
				ProcessRaisePriority, // s: ULONG
				ProcessDebugPort, // q: HANDLE
				ProcessExceptionPort, // s: HANDLE
				ProcessAccessToken, // s: PROCESS_ACCESS_TOKEN
				ProcessLdtInformation, // 10
				ProcessLdtSize,
				ProcessDefaultHardErrorMode, // qs: ULONG
				ProcessIoPortHandlers, // (kernel-mode only)
				ProcessPooledUsageAndLimits, // q: POOLED_USAGE_AND_LIMITS
				ProcessWorkingSetWatch, // q: PROCESS_WS_WATCH_INFORMATION[]; s: void
				ProcessUserModeIOPL,
				ProcessEnableAlignmentFaultFixup, // s: BOOLEAN
				ProcessPriorityClass, // qs: PROCESS_PRIORITY_CLASS
				ProcessWx86Information,
				ProcessHandleCount, // 20, q: ULONG, PROCESS_HANDLE_INFORMATION
				ProcessAffinityMask, // s: KAFFINITY
				ProcessPriorityBoost, // qs: ULONG
				ProcessDeviceMap, // qs: PROCESS_DEVICEMAP_INFORMATION, PROCESS_DEVICEMAP_INFORMATION_EX
				ProcessSessionInformation, // q: PROCESS_SESSION_INFORMATION
				ProcessForegroundInformation, // s: PROCESS_FOREGROUND_BACKGROUND
				ProcessWow64Information, // q: ULONG_PTR
				ProcessImageFileName, // q: UNICODE_STRING
				ProcessLUIDDeviceMapsEnabled, // q: ULONG
				ProcessBreakOnTermination, // qs: ULONG
				ProcessDebugObjectHandle, // 30, q: HANDLE
				ProcessDebugFlags, // qs: ULONG
				ProcessHandleTracing, // q: PROCESS_HANDLE_TRACING_QUERY; s: size 0 disables, otherwise enables
				ProcessIoPriority, // qs: ULONG
				ProcessExecuteFlags, // qs: ULONG
				ProcessResourceManagement,
				ProcessCookie, // q: ULONG
				ProcessImageInformation, // q: SECTION_IMAGE_INFORMATION
				ProcessCycleTime, // q: PROCESS_CYCLE_TIME_INFORMATION
				ProcessPagePriority, // q: ULONG
				ProcessInstrumentationCallback, // 40
				ProcessThreadStackAllocation, // s: PROCESS_STACK_ALLOCATION_INFORMATION, PROCESS_STACK_ALLOCATION_INFORMATION_EX
				ProcessWorkingSetWatchEx, // q: PROCESS_WS_WATCH_INFORMATION_EX[]
				ProcessImageFileNameWin32, // q: UNICODE_STRING
				ProcessImageFileMapping, // q: HANDLE (input)
				ProcessAffinityUpdateMode, // qs: PROCESS_AFFINITY_UPDATE_MODE
				ProcessMemoryAllocationMode, // qs: PROCESS_MEMORY_ALLOCATION_MODE
				ProcessGroupInformation, // q: USHORT[]
				ProcessTokenVirtualizationEnabled, // s: ULONG
				ProcessConsoleHostProcess, // q: ULONG_PTR
				ProcessWindowInformation, // 50, q: PROCESS_WINDOW_INFORMATION
				ProcessHandleInformation, // q: PROCESS_HANDLE_SNAPSHOT_INFORMATION // since WIN8
				ProcessMitigationPolicy, // s: PROCESS_MITIGATION_POLICY_INFORMATION
				ProcessDynamicFunctionTableInformation,
				ProcessHandleCheckingMode,
				ProcessKeepAliveCount, // q: PROCESS_KEEPALIVE_COUNT_INFORMATION
				ProcessRevokeFileHandles, // s: PROCESS_REVOKE_FILE_HANDLES_INFORMATION
				MaxProcessInfoClass
			}


			[DllImport("kernel32.dll", SetLastError = true)]
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			[SuppressUnmanagedCodeSecurity]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool CloseHandle(IntPtr hObject);
		}

		[SuppressMessage("ReSharper", "UnusedVariable")]
		[SuppressMessage("ReSharper", "RedundantCast")]
		[SuppressMessage("ReSharper", "RedundantAssignment")]
		private static class WindowsApi
		{
			/// <summary>
			/// Возвращает идентификатор процесса, откуда был запущен данный модуль.
			/// <see cref="http://www.pinvoke.net/default.aspx/ntdll.ntqueryinformationprocess"/>
			/// </summary>
			/// <param name="applicationId">Идентификатор процесса.</param>
			/// <returns>Идентификатор процесса.</returns>
			public static int GetParentProcessId(int applicationId = 0)
			{
				if (applicationId == 0) applicationId = Process.GetCurrentProcess().Id;

				NativeMethods.PROCESS_BASIC_INFORMATION pbi = new NativeMethods.PROCESS_BASIC_INFORMATION();

				//Get a handle to our own process
				IntPtr hProc = NativeMethods.OpenProcess((NativeMethods.ProcessAccessFlags) 0x001F0FFF,
														 false,
														 applicationId);

				try
				{
					int sizeInfoReturned;
					int queryStatus = NativeMethods.NtQueryInformationProcess(hProc,
																			  (NativeMethods.PROCESSINFOCLASS) 0,
																			  ref pbi,
																			  NativeMethods.PROCESS_BASIC_INFORMATION.Size,
																			  out sizeInfoReturned);
				}
				finally
				{
					if (!hProc.Equals(IntPtr.Zero))
					{
						//Close handle and free allocated memory
						NativeMethods.CloseHandle(hProc);
						hProc = IntPtr.Zero;
					}
				}

				return (int) pbi.InheritedFromUniqueProcessId;
			}
		}

		[Flags]
		private enum E3Type
		{
			/// <summary>
			/// Процесс E3.series запущенный в обычном режиме.
			/// </summary>
			E3 = 1,

			/// <summary>
			/// Процесс E3.series запущенный в режиме редакторе БД.
			/// </summary>
			DBE = 2
		}

		/// <summary>
		/// С этого имени начинается "внутреннее имя процесса" E3.series в обычном режиме запуска.
		/// </summary>
		private const string E3ApplicationName = "!E3Application";

		/// <summary>
		/// С этого имени начинается "внутреннее имя процесса" E3.series в режиме редактора БД.
		/// </summary>
		private const string E3DbeApplicationName = "!E3DbeApplication";


		private static Hashtable GetHashtables(E3Type type = E3Type.E3)
		{
			Hashtable hashtable = new Hashtable();

			IBindCtx bindContext;
			IRunningObjectTable rot;
			NativeMethods.GetRunningObjectTable(0, out rot);
			if (rot == null) return hashtable;

			NativeMethods.CreateBindCtx(0, out bindContext);
			if (bindContext == null) return hashtable;

			IEnumMoniker monikerEnumerator;
			rot.EnumRunning(out monikerEnumerator);
			if (monikerEnumerator == null) return hashtable;

			monikerEnumerator.Reset();
			IntPtr pNumFetched = new IntPtr();
			IMoniker[] monikers = new IMoniker[1];

			while (monikerEnumerator.Next(1, monikers, pNumFetched) == 0)
			{
				object obj;
				rot.GetObject(monikers[0], out obj);
				if (obj == null) continue;

				string displayName;
				monikers[0].GetDisplayName(bindContext, null, out displayName);
				if (string.IsNullOrEmpty(displayName)) continue;

				IClassFactory classFactory = obj as IClassFactory;

				dynamic dyn = null;

				if (type.HasFlag(E3Type.DBE) && classFactory != null &&
					displayName.StartsWith(E3DbeApplicationName, StringComparison.CurrentCultureIgnoreCase))
					// "!" + "E3DBEAPPLICATIONFACTORY" + ":"
				{
					// Для E3.series 2015 и выше

					IntPtr appObject;
					Guid guidIUnknown = new Guid("00000000-0000-0000-C000-000000000046");
					int result = classFactory.CreateInstance(IntPtr.Zero, ref guidIUnknown, out appObject);

					if (appObject != null)
					{
						dyn = Marshal.GetObjectForIUnknown(appObject);
					}
				}
				// Для нижних  E3.series 2015 версий найти объект не известно как.

				else if (type.HasFlag(E3Type.E3) &&
						 displayName.StartsWith(E3ApplicationName, StringComparison.CurrentCultureIgnoreCase))
					// "!" + "E3APPLICATION" + ":"
				{
					dyn = obj;
				}

				if (dyn != null)
				{
					int pid;

					// Извлечение из displayName идентификатор процесса
					int index = displayName.LastIndexOf(':');
					if (index != -1 && index < displayName.Length && int.TryParse(displayName.Substring(index + 1), out pid))
					{
						hashtable.Add(pid, dyn);
					}
				}
			}

			return hashtable;
		}

		#endregion


		#region Dispose: CA1063: следует правильно реализовывать IDisposable

		/*
				/// <summary>
				/// Используется для выяснения того, вызывался ли уже метод Dispose()
				/// </summary>
				private bool _disposed;
		*/
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// NOTE: Leave out the finalizer altogether if this class doesn't   
		// own unmanaged resources itself, but leave the other methods  
		// exactly as they are.   
		~E3Connector()
		{
			// Finalizer calls Dispose(false)  
			Dispose(false);
		}

		private void Dispose(bool disposing)
		{
			/*
			if (!_disposed)
			{
				if (disposing)
				{
					// Освобождаем управляемые ресурсы
					if (IsLoadedE3Application) DisposeObject(E3Application);
				}

				// освобождаем неуправляемые объекты
				_disposed = true;
			}
			*/

			// Освобождаем управляемые ресурсы
			DisposeObject(E3Application);
			// Использовать это?
			_instance = null;
		}

		#endregion

		/// <summary>
		/// Освобождение объекта
		/// </summary>
		/// <param name="obj"></param>
		private static void DisposeObject(object obj)
		{
			// Освобождаем управляемые ресурсы
			if (obj != null && Marshal.IsComObject(obj)) Marshal.ReleaseComObject(obj);
			obj = null;
		}
	}
}