﻿using System.Reflection;
using System.Runtime.InteropServices;


// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("Pointcad.Instrumentation.Layout.Plugin x64")]
[assembly: AssemblyDescription("[E3.Instrumentation.Plugin] Планы расположения x64")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Point JSC")]
[assembly: AssemblyProduct("Pointcad.Instrumentation.Layout")]
[assembly: AssemblyCopyright("Copyright © Point JSC 2020. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False для параметра ComVisible делает типы в этой сборке невидимыми
// для компонентов COM. Если необходимо обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("f2e6e54a-5093-4ef3-a97d-bc80c44b5470")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номер сборки и номер редакции по умолчанию.
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("4.0.0.0")]

// 42005 соответствует e3_plugin_x32_e3_2016_17.30
[assembly: E3PlugIn.E3PlugInEntryType(typeof(InstrumentationLayout.PlugIn), 42005)]