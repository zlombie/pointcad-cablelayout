﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;


namespace Pointcad.Dragon.Converters
{
	public class BooleanInverter : MarkupExtension, IValueConverter
	{
		#region Fields

		private static readonly BooleanInverter Converter = new BooleanInverter();

		#endregion

		#region Properties

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return !(bool) value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return !(bool) value;
		}

		#endregion

		#region Methods

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Converter;
		}

		#endregion
	}
}