﻿using System;
using System.Globalization;
using System.Windows.Data;


namespace Pointcad.Dragon.Converters
{
	public class MultiConverter : IMultiValueConverter
	{
		#region Properties

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			return values.Clone();
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}
}