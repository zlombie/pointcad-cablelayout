﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;


namespace Pointcad.Dragon.Converters
{
	public class EnumToBooleanConverter : MarkupExtension, IValueConverter
	{
		#region Fields

		private static readonly EnumToBooleanConverter Converter = new EnumToBooleanConverter();

		#endregion

		#region Properties

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value != null && value.Equals(parameter);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value.Equals(true) ? parameter : Binding.DoNothing;
		}

		#endregion

		#region Methods

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Converter;
		}

		#endregion
	}
}