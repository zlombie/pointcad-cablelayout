﻿using System.Linq;
using System.Windows.Controls;


namespace Pointcad.Dragon.UserControls
{
	public class CharBox : TextBox
	{
		#region Constructors

		public CharBox()
		{
			TextChanged += OnTextChanged;
		}

		#endregion

		#region Methods

		private void OnTextChanged(object sender, TextChangedEventArgs e)
		{
			string initialText = Text;
			if (string.IsNullOrEmpty(initialText) || initialText.Length == 1)
			{
				return;
			}
			e.Handled = true;
			Text = initialText[e.Changes.Last().Offset].ToString();
			CaretIndex = Text.Length;
		}

		#endregion
	}
}