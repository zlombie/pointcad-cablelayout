﻿namespace Pointcad.Dragon.UserControls
{
	public enum FileSystemDialogMode
	{
		OpenFile,
		SaveFile,
		FolderBrowser
	}
}