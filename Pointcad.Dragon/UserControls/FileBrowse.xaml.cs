﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;


namespace Pointcad.Dragon.UserControls
{
	public partial class FileBrowse
	{
		#region Constructors

		public FileBrowse()
		{
			InitializeComponent();
			TextControl.TextChanged += TextControlOnTextChanged;
		}

		#endregion

		#region Fields

		public static readonly RoutedEvent FileNameChangedEvent = EventManager.RegisterRoutedEvent(nameof(FileNameChanged),
																								   RoutingStrategy.Bubble,
																								   typeof (RoutedEventHandler),
																								   typeof (FileBrowse));

		public static readonly DependencyProperty FileNameProperty = DependencyProperty.Register(nameof(FileName),
																								 typeof (string),
																								 typeof (FileBrowse),
																								 new FrameworkPropertyMetadata
																								 {
																									 BindsTwoWayByDefault = true,
																									 DefaultValue = null
																								 });

		public static readonly DependencyProperty FileSystemDialogModeProperty =
			DependencyProperty.Register(nameof(FileSystemDialogMode),
										typeof (FileSystemDialogMode),
										typeof (FileBrowse));

		public static readonly DependencyProperty FilterProperty = DependencyProperty.Register(nameof(Filter),
																							   typeof (string),
																							   typeof (FileBrowse));

		public static readonly DependencyProperty FolderBrowserDescriptionProperty =
			DependencyProperty.Register(nameof(FolderBrowserDescription),
										typeof (string),
										typeof (FileBrowse));

		public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(nameof(IsReadOnly),
																								   typeof (bool),
																								   typeof (FileBrowse));

		#endregion

		#region Properties

		public string FileName
		{
			get { return (string) GetValue(FileNameProperty); }
			set { SetValue(FileNameProperty, value); }
		}

		public FileSystemDialogMode FileSystemDialogMode
		{
			get { return (FileSystemDialogMode) GetValue(FileSystemDialogModeProperty); }
			set { SetValue(FileSystemDialogModeProperty, value); }
		}

		public string Filter
		{
			get { return (string) GetValue(FilterProperty); }
			set { SetValue(FilterProperty, value); }
		}

		public string FolderBrowserDescription
		{
			get { return (string) GetValue(FolderBrowserDescriptionProperty); }
			set { SetValue(FolderBrowserDescriptionProperty, value); }
		}

		public bool IsReadOnly
		{
			get { return (bool) GetValue(IsReadOnlyProperty); }
			set { SetValue(IsReadOnlyProperty, value); }
		}

		#endregion

		#region Methods

		private void BrowseButton_Click(object sender, RoutedEventArgs e)
		{
			if (FileSystemDialogMode == FileSystemDialogMode.FolderBrowser)
			{
				var folderDialog = new FolderBrowserDialog
								   {
									   ShowNewFolderButton = true,
									   Description = FolderBrowserDescription
								   };

				if (!string.IsNullOrEmpty(FileName))
				{
					folderDialog.SelectedPath = FileName;
				}

				DialogResult result = folderDialog.ShowDialog();
				if (result == DialogResult.OK)
				{
					FileName = folderDialog.SelectedPath;
				}
			}
			else
			{
				FileDialog dialog;

				switch (FileSystemDialogMode)
				{
					case FileSystemDialogMode.OpenFile:
						dialog = new OpenFileDialog();
						break;

					case FileSystemDialogMode.SaveFile:
						dialog = new SaveFileDialog();
						break;

					default:
						return;
				}

				dialog.Filter = Filter;

				if (!string.IsNullOrEmpty(FileName))
				{
					string directoryName = Path.GetDirectoryName(FileName);
					if (Path.IsPathRooted(directoryName))
					{
						dialog.InitialDirectory = directoryName;
					}
				}

				DialogResult result = dialog.ShowDialog();
				if (result == DialogResult.OK)
				{
					FileName = dialog.FileName;
				}
			}
		}

		private void TextControlOnTextChanged(object sender, TextChangedEventArgs e)
		{
			e.Handled = true;
			var args = new RoutedEventArgs(FileNameChangedEvent);
			RaiseEvent(args);
		}

		#endregion

		public event RoutedEventHandler FileNameChanged
		{
			add { AddHandler(FileNameChangedEvent, value); }
			remove { RemoveHandler(FileNameChangedEvent, value); }
		}
	}
}