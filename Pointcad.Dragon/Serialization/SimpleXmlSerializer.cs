﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace Pointcad.Dragon.Serialization
{
	public static class SimpleXmlSerializer
	{
		#region Properties

		public class TemporaryFile : IDisposable
		{
			#region Constructors

			public TemporaryFile(string fileName)
			{
				if (!string.IsNullOrEmpty(fileName))
				{
					_fileInfo = new FileInfo(fileName);
				}
			}

			#endregion

			#region Fields

			private readonly FileInfo _fileInfo;

			#endregion

			#region Properties

			public string FullName => _fileInfo?.FullName;

			public void Dispose()
			{
				if (_fileInfo != null && _fileInfo.Exists)
				{
					_fileInfo.Delete();
				}
			}

			#endregion

			#region Methods

			public void CopyTo(string destinationFileName)
			{
				if (_fileInfo == null || !_fileInfo.Exists || string.IsNullOrEmpty(destinationFileName))
				{
					return;
				}

				try
				{
					File.Copy(_fileInfo.FullName, destinationFileName, true);
				}
				catch (Exception)
				{
					// 
				}
			}

			#endregion
		}

		#endregion

		#region Methods

		public static T Load<T>(string fileName) where T : new()
		{
			if (string.IsNullOrWhiteSpace(fileName) || !Path.IsPathRooted(fileName))
			{
				return default(T);
			}

			var file = new FileInfo(fileName);
			if (file.Exists)
			{
				using (XmlReader xmlReader = XmlReader.Create(file.FullName))
				{
					var serializer = new XmlSerializer(typeof (T));
					return (T) serializer.Deserialize(xmlReader);
				}
			}

			return default(T);
		}

		public static bool Save(string fileName, object obj)
		{
			if (string.IsNullOrWhiteSpace(fileName) || !Path.IsPathRooted(fileName))
			{
				return false;
			}

			var file = new FileInfo(fileName);
			DirectoryInfo directory = file.Directory;

			if (directory == null)
			{
				return false;
			}

			if (!directory.Exists)
			{
				try
				{
					directory.Create();
				}
				catch (Exception)
				{
					return false;
				}
			}

			string backupFileName;
			if (file.Exists)
			{
				backupFileName = Path.GetTempFileName() + file.Extension;
				try
				{
					File.Copy(file.FullName, backupFileName);
				}
				catch (Exception)
				{
					backupFileName = null;
				}
			}
			else
			{
				backupFileName = null;
			}

			using (var backup = new TemporaryFile(backupFileName))
			{
				try
				{
					using (TextWriter writer = new StreamWriter(file.FullName, false, Encoding.Unicode))
					{
						var serializer = new XmlSerializer(obj.GetType());
						serializer.Serialize(writer, obj);
					}
				}
				catch (Exception)
				{
					backup.CopyTo(file.FullName);
					throw;
				}

				return true;
			}
		}

		#endregion
	}
}