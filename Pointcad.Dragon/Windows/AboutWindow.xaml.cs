﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;


namespace Pointcad.Dragon.Windows
{
	public partial class AboutWindow
	{
		#region Constructors

		public AboutWindow(Window owner = null)
		{
			InitializeComponent();
			Owner = owner;
		}

		public AboutWindow(string product, string version, string build, Window owner = null)
			: this(owner)
		{
			DataContext = new AboutAdapter(product, version, build);
		}

		#endregion
	}

	internal class AboutAdapter : ViewModelBase
	{
		#region Constructors

		public AboutAdapter()
		{
			Assembly assembly = Assembly.GetEntryAssembly();
			AssemblyName assemblyName = assembly.GetName();
			DateTime buildDateTime = RetrieveLinkerTimestamp(assembly);

			VersionLine =	$"Версия {assemblyName.Version} ({buildDateTime.ToString("dd MMMM yyyy г. HH:mm")})";

            ApplicationName = "Планы расположения";

            Year = buildDateTime.Year;
		}

		public AboutAdapter(string product, string version, string build)
			: this()
		{

            Product = product;
            Version = "Версия " + version;
            Build = build;
			CopyrightLine = "AO ПОИНТ, 2020. Все права защищены";
		}

		#endregion

		#region Fields

		#endregion

		#region Properties

		public bool Is64BitProcess => Environment.Is64BitProcess;

        public string Product
        {
            get;
            private set;
        }

        public string Version
        {
            get;
            private set;
        }

        public string Build
        {
            get;
            private set;
        }

        public string ApplicationName
		{
			get;
			private set;
		}

		public string CopyrightLine
		{
			get;
			private set;
		}

		public string DeveloperHeader
		{
			get;
			private set;
		}

		public string DeveloperListingLine
		{
			get;
			private set;
		}

		public string ForumUrl
		{
			get;
		}

		public string VersionLine
		{
			get;
			private set;
		}

		public static int Year
		{
			get;
			private set;
		}

		#endregion

		#region Commands

		private ICommand _closeWindowCommand;
		private ICommand _navigateToForumCommand;
		private ICommand _navigateToOfficialWebsiteCommand;

		public ICommand CloseWindowCommand =>
			_closeWindowCommand ?? (_closeWindowCommand = new RelayCommand<Window>(CloseWindow));

		public ICommand NavigateToForumCommand =>
			_navigateToForumCommand ?? (_navigateToForumCommand = new RelayCommand(NavigateToForum));

		public ICommand NavigateToOfficialWebsiteCommand =>
			_navigateToOfficialWebsiteCommand ??
			(_navigateToOfficialWebsiteCommand = new RelayCommand(NavigateToOfficialWebsite));

		#endregion

		#region Methods

		private static void CloseWindow(Window window)
		{
			window?.Close();
		}

		private static void NavigateToOfficialWebsite()
		{
			Process.Start("http://www.e3series.ru");
		}

		private static DateTime RetrieveLinkerTimestamp(Assembly assembly)
		{
			string filePath = assembly.Location;
			const int peHeaderOffset = 60;
			const int linkerTimestampOffset = 8;

			var buffer = new byte[2048];
			Stream stream = null;

			try
			{
				stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				stream.Read(buffer, 0, 2048);
			}
			finally
			{
				stream?.Close();
			}

			int i = BitConverter.ToInt32(buffer, peHeaderOffset);
			int secondsSince1970 = BitConverter.ToInt32(buffer, i + linkerTimestampOffset);
			var dt = new DateTime(1970, 1, 1, 0, 0, 0);
			dt = dt.AddSeconds(secondsSince1970);
			dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
			return dt;
		}

		private void NavigateToForum()
		{
            Process.Start("mailto:e3@pointcad.ru");
		}

		#endregion
	}
}