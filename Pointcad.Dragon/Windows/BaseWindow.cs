﻿using System;
using System.Diagnostics;
using System.Windows.Media;
using MahApps.Metro.Controls;


namespace Pointcad.Dragon.Windows
{
	public class BaseWindow : MetroWindow
	{
		#region Constructors

		public BaseWindow()
		{
			ShowIconOnTitleBar = false;
#pragma warning disable CS0618 // 'MetroWindow.TitleCaps" является устаревшим: 'This property will be deleted in the next release. You should use the new TitleCharacterCasing dependency property.'
			TitleCaps = false;
#pragma warning restore CS0618 // 'MetroWindow.TitleCaps" является устаревшим: 'This property will be deleted in the next release. You should use the new TitleCharacterCasing dependency property.'
			UseLayoutRounding = true;

			try
			{
				GlowBrush = FindResource("AccentColorBrush") as Brush;

				//var drawing = FindResource("PointLogoForTaskbar") as Drawing;
				//if (drawing != null)
				//{
				//	Icon = new DrawingImage(drawing);
				//}
			}
			catch (Exception)
			{
				//
			}
		}

		#endregion

		#region Properties

		public bool IsInDesignMode
		{
			get
			{
				Process process = Process.GetCurrentProcess();
				bool result = process.ProcessName == "devenv";
				process.Dispose();
				return result;
			}
		}

		#endregion
	}
}