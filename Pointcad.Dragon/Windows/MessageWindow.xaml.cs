﻿using System.Threading;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;


namespace Pointcad.Dragon.Windows
{
	public partial class MessageWindow
	{
		#region Constructors

		private MessageWindow(string text,
							  string header,
							  string title,
							  MessageWindowButtons button,
							  MessageWindowImages icon,
							  Window owner)
		{
			InitializeComponent();

			Title = title ?? string.Empty;

			if (owner == null)
			{
				WindowStartupLocation = WindowStartupLocation.CenterScreen;
			}
			else
			{
				WindowStartupLocation = WindowStartupLocation.CenterOwner;
				Owner = owner;
			}

			DataContext = new MessageWindowViewModel(text, header, button, icon);
		}

		#endregion

		#region Properties

		private MessageWindowViewModel ViewModel => DataContext as MessageWindowViewModel;

		#endregion

		#region Methods

		public static MessageDialogResults ShowDialog(string text,
													  string header = null,
													  string title = null,
													  MessageWindowButtons button = MessageWindowButtons.Ok,
													  MessageWindowImages icon = MessageWindowImages.Information,
													  Window owner = null)
		{
			var window = new MessageWindow(text, header, title, button, icon, owner);
			return window.ShowDialog();
		}

		public static MessageDialogResults ShowDialogInSeparateThread(string text,
																	  string header = null,
																	  string title = null,
																	  MessageWindowButtons button = MessageWindowButtons.Ok,
																	  MessageWindowImages icon = MessageWindowImages.Information)
		{
			var result = MessageDialogResults.Cancel;

			var thread = new Thread(() => result = ShowDialog(text, header, title, button, icon));
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();
			thread.Join();

			return result;
		}

		public new MessageDialogResults ShowDialog()
		{
			base.ShowDialog();
			return ViewModel?.DialogResult ?? MessageDialogResults.Cancel;
		}

		#endregion
	}

	internal class MessageWindowViewModel : ViewModelBase
	{
		#region Constructors

		public MessageWindowViewModel()
		{
		}

		public MessageWindowViewModel(string text, string header, MessageWindowButtons button, MessageWindowImages icon)
		{
			Text = text;
			Header = header;

			switch (button)
			{
				case MessageWindowButtons.AbortIgnoreIgnoreAll:
					AbortButtonIsVisible = true;
					IgnoreButtonIsVisible = true;
					IgnoreAllButtonIsVisible = true;
					break;

				case MessageWindowButtons.Ok:
					OkButtonIsVisible = true;
					break;

				case MessageWindowButtons.OkCancel:
					OkButtonIsVisible = true;
					CancelButtonIsVisible = true;
					break;

				case MessageWindowButtons.RetryCancel:
					IgnoreButtonIsVisible = true;
					CancelButtonIsVisible = true;
					break;

				case MessageWindowButtons.YesNo:
					YesButtonIsVisible = true;
					NoButtonIsVisible = true;
					break;

				case MessageWindowButtons.YesNoCancel:
					YesButtonIsVisible = true;
					NoButtonIsVisible = true;
					CancelButtonIsVisible = true;
					break;
			}

			switch (icon)
			{
				case MessageWindowImages.Information:
					InformationIconIsVisible = true;
					break;

				case MessageWindowImages.Question:
					QuestionIconIsVisible = true;
					break;

				case MessageWindowImages.Warning:
					WarningIconIsVisible = true;
					break;
			}
		}

		#endregion

		#region Fields

		private bool _abortButtonIsVisible;
		private bool _cancelButtonIsVisible;
		private string _header;
		private bool _ignoreButtonIsVisible;
		private bool _informationIconIsVisible;
		private bool _noButtonIsVisible;
		private bool _okButtonIsVisible;
		private bool _questionIconIsVisible;
		private bool _retryButtonIsVisible;
		private string _text;
		private bool _warningIconIsVisible;
		private bool _yesButtonIsVisible;

		#endregion

		#region Properties

		public bool AbortButtonIsVisible
		{
			get { return _abortButtonIsVisible; }
			set { Set(ref _abortButtonIsVisible, value); }
		}

		public bool CancelButtonIsVisible
		{
			get { return _cancelButtonIsVisible; }
			set { Set(ref _cancelButtonIsVisible, value); }
		}

		public MessageDialogResults DialogResult
		{
			get;
			private set;
		}

		public string Header
		{
			get { return _header; }
			set { Set(ref _header, value); }
		}

		public bool IgnoreAllButtonIsVisible
		{
			get { return _ignoreButtonIsVisible; }
			set { Set(ref _ignoreButtonIsVisible, value); }
		}

		public bool InformationIconIsVisible
		{
			get { return _informationIconIsVisible; }
			set { Set(ref _informationIconIsVisible, value); }
		}

		public bool NoButtonIsVisible
		{
			get { return _noButtonIsVisible; }
			set { Set(ref _noButtonIsVisible, value); }
		}

		public bool OkButtonIsVisible
		{
			get { return _okButtonIsVisible; }
			set { Set(ref _okButtonIsVisible, value); }
		}

		public bool QuestionIconIsVisible
		{
			get { return _questionIconIsVisible; }
			set { Set(ref _questionIconIsVisible, value); }
		}

		public bool IgnoreButtonIsVisible
		{
			get { return _retryButtonIsVisible; }
			set { Set(ref _retryButtonIsVisible, value); }
		}

		public string Text
		{
			get { return _text; }
			set { Set(ref _text, value); }
		}

		public bool WarningIconIsVisible
		{
			get { return _warningIconIsVisible; }
			set { Set(ref _warningIconIsVisible, value); }
		}

		public bool YesButtonIsVisible
		{
			get { return _yesButtonIsVisible; }
			set { Set(ref _yesButtonIsVisible, value); }
		}

		#endregion

		#region Commands

		private ICommand _closeWindowCommand;

		public ICommand CloseWindowCommand
			=> _closeWindowCommand ?? (_closeWindowCommand = new RelayCommand<object>(CloseWindow));

		#endregion

		#region Methods

		private void CloseWindow(object obj)
		{
			var values = obj as object[];
			if (values == null || values.Length < 2)
			{
				return;
			}

			if (values[1] is MessageDialogResults)
			{
				DialogResult = (MessageDialogResults) values[1];
			}

			var window = values[0] as Window;
			window?.Close();
		}

		#endregion
	}

	/// <summary>
	/// Задает константы, определяющие кнопки, которые нужно отображать в окне MessageWindow.
	/// </summary>
	public enum MessageWindowButtons
	{
		/// <summary>
		/// Окно сообщения содержит кнопки "Прервать", "Повторить" и "Пропустить".
		/// </summary>
		AbortIgnoreIgnoreAll,

		/// <summary>
		/// Окно сообщения содержит кнопку "ОК".
		/// </summary>
		Ok,

		/// <summary>
		/// Окно сообщения содержит кнопки "OK" и "Отмена".
		/// </summary>
		OkCancel,

		/// <summary>
		/// Окно сообщения содержит кнопки "Повторить" и "Отмена".
		/// </summary>
		RetryCancel,

		/// <summary>
		/// Окно сообщения содержит кнопки "Да" и "Нет".
		/// </summary>
		YesNo,

		/// <summary>
		/// Окно сообщения содержит кнопки "Да", "Нет" и "Отмена".
		/// </summary>
		YesNoCancel
	}

	public enum MessageWindowImages
	{
		/// <summary>
		/// Без иконки.
		/// </summary>
		None,

		/// <summary>
		/// Знак вопроса.
		/// </summary>
		Question,

		/// <summary>
		/// Информация.
		/// </summary>
		Information,

		/// <summary>
		/// Предупреждение.
		/// </summary>
		Warning
	}

	public enum MessageDialogResults
	{
		/// <summary>
		/// Отмена.
		/// </summary>
		Cancel,

		/// <summary>
		/// Прервать.
		/// </summary>
		Abort,

		/// <summary>
		/// Пропустить.
		/// </summary>
		Ignore,
        /// <summary>
        /// Пропустить.
        /// </summary>
        IgnoreAll,
        /// <summary>
        /// Повторить.
        /// </summary>
        Retry,

		/// <summary>
		/// Да.
		/// </summary>
		Yes,

		/// <summary>
		/// Нет.
		/// </summary>
		No,

		/// <summary>
		/// OK.
		/// </summary>
		Ok
	}
}