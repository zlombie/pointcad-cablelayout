﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Pointcad.Dragon.Windows;
using System.Windows.Media.Imaging;
using System.IO;

namespace Layout.Views
{

    /// <summary>
    /// Логика взаимодействия для Preview3D.xaml
    /// </summary>
    public partial class Preview3D
    {
        List<EItemOnLayoutAdvanced> SheetSegments = new List<EItemOnLayoutAdvanced>();
        public Model3D Model { get; set; }
        public string TempFile;
        BitmapImage _image = new BitmapImage();
        Uri _u;

        public Preview3D(List<EItemOnLayoutAdvanced> sheetSegments, string tempfile)
        {
            InitializeComponent();
            try
            {
                Model = null;
                SheetSegments.Clear();
                SheetSegments = sheetSegments;
                TempFile = tempfile;
                var modelGroup = new Model3DGroup();
                var meshBuilder = new MeshBuilder(false, false);
                List<Point3D> list = new List<Point3D>();
                foreach (var s in sheetSegments)
                {
                    List<Point3D> tubelist = new List<Point3D>();
                    foreach (var l in s.Lines)
                    {
                        Point3D p1 = new Point3D(l.P1.X, l.P1.Y, s.Height);
                        Point3D p2 = new Point3D(l.P2.X, l.P2.Y, s.Height);
                        tubelist.Add(p1);
                        tubelist.Add(p2);
                    }
                    list.AddRange(tubelist);
                    meshBuilder.AddTube(tubelist, 1, 12, false);
                }
                foreach (var p in list)
                {
                    var fnd = list.FindAll(f => f.X == p.X && f.Y == p.Y && f.Z != p.Z);
                    double max = 0;
                    int index = 0;
                    for (int i = 0; i < fnd.Count; i++)
                    {
                        if (Math.Abs(fnd[i].Z - p.Z) > max)
                        {
                            max = Math.Abs(fnd[i].Z - p.Z);
                            index = i;
                        }
                    }
                    if (max > 0)
                    {
                        var p1 = p;
                        var p2 = fnd[index];
                        if (p1.Z > p2.Z)
                        {
                            p1.Z += 0.5;
                            p2.Z -= 0.5;
                        }
                        else
                        {
                            p1.Z -= 0.5;
                            p2.Z += 0.5;
                        }
                        meshBuilder.AddCylinder(p1, p2, 1, 12);
                    }
                }
                // Create a mesh from the builder (and freeze it)
                var mesh = meshBuilder.ToMesh(true);
                // Create some materials
                var greenMaterial = MaterialHelper.CreateMaterial(Colors.Green);
                var insideMaterial = MaterialHelper.CreateMaterial(Colors.DarkGreen);

                modelGroup.Children.Add(new GeometryModel3D { Geometry = mesh, Material = greenMaterial, BackMaterial = insideMaterial });
                meshBuilder = new MeshBuilder(false, true);
                _u = new Uri(tempfile, UriKind.Relative);
                if (File.Exists(tempfile))
                {
                    var stream = File.OpenRead(tempfile);
                    _image.BeginInit();
                    _image.CacheOption = BitmapCacheOption.OnLoad;
                    _image.StreamSource = stream;
                    _image.EndInit();
                    stream.Close();
                    IList<Point3D> pnts = new List<Point3D>();
                    pnts.Add(new Point3D(0, 0, 0));
                    pnts.Add(new Point3D(_image.Width * 0.26583, 0, 0));
                    pnts.Add(new Point3D(_image.Width * 0.26583, _image.Height * 0.26583, 0));
                    pnts.Add(new Point3D(0, _image.Height * 0.26583, 0));
                    meshBuilder.AddPolygon(pnts);
                    mesh = meshBuilder.ToMesh(true);
                    var mat = MaterialHelper.CreateImageMaterial(_image, 0.5);
                    modelGroup.Children.Add(new GeometryModel3D { Geometry = mesh, Material = mat, BackMaterial = mat });
                }
                Model = modelGroup;
                helix3D.DataContext = this;
            }
            catch (Exception ex)
            {
                MessageWindow.ShowDialog(ex.StackTrace, "", "Ошибка", MessageWindowButtons.Ok, MessageWindowImages.Warning);
            }
        }

        private void BaseWindow_Closed(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.Delete(TempFile);
            }
            catch (Exception ex)
            {
                e3c.app.PutError(0, ex.Message);
            }
            
        }
    }
}

