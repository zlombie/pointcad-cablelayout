﻿using Layout.Configuration;
using Layout.Main;
using Layout.Properties;
using Layout.Streams;
using DragonExample.Database;
using Pointcad.Dragon.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Layout.Service;

namespace Layout.Views
{

    public partial class ConstructionWindow
    {
        eProfile _profile;
        public ObservableCollection<EMaterialComponent> Materials { get; set; }
        public EMaterialComponent CurrentMaterial { get; set; }


        private EMaterialComponent _selectedMaterial { get; set; }


        #region Constructors

        public ConstructionWindow(eProfile profile, ObservableCollection<EMaterialComponent> materials)
        {
            InitializeComponent();
            this.Topmost = true;
            try
            {
                _profile = profile;
                foreach (var d in DatabaseSingleton.Instance.Components)
                {
                    if (d.Name!="<Нет данных>")
                    {
//                        cboComponents.Items.Add(d.Name);
                    }
                }
                Materials = materials;
  //              dgMaterials.ItemsSource = Materials;

    //            CurrentMaterial = new EMaterialComponent(cboComponents.Items[0].ToString(), 1, 1, "метр", "До целого", "Математически");
                this.DataContext = CurrentMaterial;


            }
            catch (Exception ex)
            {

            }
        }


        #endregion
        bool changeSelected = true;

        private void btnAddMaterial_Click(object sender, RoutedEventArgs e)
        {
            changeSelected = false;
            var tempMaterial = new EMaterialComponent(CurrentMaterial.Component, CurrentMaterial.RatioNumerator, CurrentMaterial.RatioDenominator, CurrentMaterial.Units, CurrentMaterial.RoundNum, CurrentMaterial.RoundType);
            Materials.Add(tempMaterial);
        }

        private void btnDeleteMaterial_Click(object sender, RoutedEventArgs e)
        {
            if (Materials.Count>0 && _selectedMaterial!=null)
            {
                Materials.Remove(_selectedMaterial);
            }
        }

        private void dgMaterials_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (changeSelected && _selectedMaterial != null)
            {
                _selectedMaterial.Component=CurrentMaterial.Component;
                _selectedMaterial.RatioNumerator = CurrentMaterial.RatioNumerator;
                _selectedMaterial.RatioDenominator = CurrentMaterial.RatioDenominator;
                _selectedMaterial.Units = CurrentMaterial.Units;
                _selectedMaterial.RoundNum = CurrentMaterial.RoundNum;
                _selectedMaterial.RoundType = CurrentMaterial.RoundType;
            }

            //_selectedMaterial = (EMaterialComponent)dgMaterials.SelectedItem;
            if (_selectedMaterial != null)
            {
                CurrentMaterial.Component=_selectedMaterial.Component;
                CurrentMaterial.RatioNumerator = _selectedMaterial.RatioNumerator;
                CurrentMaterial.RatioDenominator = _selectedMaterial.RatioDenominator;
                CurrentMaterial.Units = _selectedMaterial.Units;
                CurrentMaterial.RoundNum = _selectedMaterial.RoundNum;
                CurrentMaterial.RoundType = _selectedMaterial.RoundType;
                changeSelected = true;
            }
        }

        private void BaseWindow_Closing(object sender, CancelEventArgs e)
        {
            if (changeSelected && _selectedMaterial != null)
            {
                _selectedMaterial.Component=CurrentMaterial.Component;
                _selectedMaterial.RatioNumerator = CurrentMaterial.RatioNumerator;
                _selectedMaterial.RatioDenominator = CurrentMaterial.RatioDenominator;
                _selectedMaterial.Units = CurrentMaterial.Units;
                _selectedMaterial.RoundNum = CurrentMaterial.RoundNum;
                _selectedMaterial.RoundType = CurrentMaterial.RoundType;
            }
        }
    }
}