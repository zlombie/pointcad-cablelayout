﻿using Layout.Configuration;
using Layout.Streams;
using Pointcad.Dragon.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Layout.Service;
using Layout.Properties;
using Layout.Main;

namespace Layout.Views
{
    public partial class MainWindow : INotifyPropertyChanged
    {

        #region Constructors

        public MainWindow()
		{
            InitializeComponent();
			Settings settings = Settings.Default;
            Left = settings.MainWindowLeft;
            Top = settings.MainWindowTop;
            oldw = settings.MainWindowWidth;
            oldh = settings.MainWindowHeight;
            exit = true;

            _cabLay = new ePathConstructor(SetProgress);
            _cabStream = new EStreamBuilder(SetProgress, SetStatusBarText);
            _section = new AllSectionBuilder(SetProgress, SetStatusBarText);

            SetLayingStyles();
            SetScalesList();
            cbHeight.Items.Add(eConstants.NullString);
            Utilites.CurrentSheet.GetAllHeights().ForEach(f => cbHeight.Items.Add(f));
            dgTrenchCables.ItemsSource = new List<EStreamCable> { new EStreamCable { Name = "Выберите участок!", Class = eConstants.NullString } };
            var cv = (CollectionView)CollectionViewSource.GetDefaultView(dgTrenchCables.ItemsSource);
            var pgd = new PropertyGroupDescription("Class");
            cv.GroupDescriptions.Add(pgd);
            this.Loaded += (s, e) => { tabControl.SelectionChanged += tabControl_SelectionChanged; };
            txtStatus.Text = "Масштаб активного листа:";
            this.Topmost = true;
            
        }

        #endregion

        #region Прогресс

        private WaitingForm wFrm = new WaitingForm();

        private void SetStatusBarText(string text)
        {
            if (txtStatus.Dispatcher.CheckAccess())
            {
                txtStatus.Text = text;
            }
            else
            {
                txtStatus.Dispatcher.Invoke(new StatusTextHandler(SetStatusBarText), text);
                txtStatus.Text = text;
            }
            
        }

        private void SetProgress(int cnt, int max, string text = "")
        {
            var d = wFrm.pBar.Dispatcher;
            if (!d.CheckAccess())
            {
                d.Invoke(new ProgressHandler(SetProgress), cnt, max, text);
            }
            else
            {
                wFrm.Title = text;
                wFrm.pBar.Maximum = max + 1;
                if (cnt < max) wFrm.pBar.Value = cnt;
            }
        } 

        #endregion

        #region Кнопки в заголовке окна

        private void btnShowSettings_Click(object sender, RoutedEventArgs e)
        {
            var profileWindow = new ProfileWindow() { Owner = this };
            profileWindow.ShowDialog();
            SetLayingStyles();
            SetScalesList();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = e3c.app.GetId();
                var e3proc = Process.GetProcessById(id);
                string productName = "E3.series";
                string version = "";
                try
                {
                    productName = e3proc.MainModule.FileVersionInfo.ProductName;
                    version = e3proc.MainModule.FileVersionInfo.ProductVersion.Replace(",", ".");
                }
                catch { }
                string build = e3c.app.GetBuild();
                var aboutWindow = new AboutWindow(productName, version, build, this);
                aboutWindow.ShowDialog();
            }
            catch { }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory + "CableLayoutHelp.pdf";
            try
            {
                Process.Start(filename);
            }
            catch (Exception ex)
            {
                MessageWindow.ShowDialog(ex.Message + ": " + filename, "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
        }

        #endregion

        #region Mouse Hooks

        MouseHook _hook = new MouseHook();
        private bool _hookOff = false;

        #endregion

        #region Methods

        private void BaseWindow_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.S)
            {
                btnShowSettings_Click(this, null);
            }
        }

        private void BaseWindow_Activated(object sender, EventArgs e)
        {
            _hook.Uninstall();
            Thread.Sleep(10);
            try
            {
                if (this.WindowState != WindowState.Minimized)
                {
                    if (tabControl.SelectedIndex == 0)
                    {
                        ReadFirstTab();
                    }
                    _hookOff = false;
                    wFrm.Owner = this;
                    if (tabControl.SelectedIndex == 2)
                    {
                        oldw = this.Width;
                        oldh = this.Height;
                    }
                }
            }
            catch { }
        }

        private void BaseWindow_Deactivated(object sender, EventArgs e)
        {
            if (this.WindowState != WindowState.Minimized)
            {
                _hook = new MouseHook();
                if (tabControl.SelectedIndex == 0 && !_hookOff)
                {
                    _hook.LeftButtonUp += hook_GetSegments;
                    _hook.Install();
                }
                if (tabControl.SelectedIndex == 1 && !_hookOff)
                {
                    _hook.LeftButtonUp += hook_GetStream;
                    _hook.Install();
                }
                if (tabControl.SelectedIndex == 2)
                {
                    oldw = this.Width;
                    oldh = this.Height;
                    _hookOff = true;
                }
                if (tabControl.SelectedIndex == 3 && !_hookOff)
                {
                    _hook.LeftButtonUp += hook_GetSection;
                    _hook.Install();
                }
            }
        }

        private double oldw = 0;
        private double oldh = 0;
        private bool exit = true;

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Settings settings = Settings.Default;
            _hookOff = true;
            if (WindowState == WindowState.Maximized)
            {
                settings.MainWindowLeft = RestoreBounds.Left;
                settings.MainWindowTop = RestoreBounds.Top;
                settings.MainWindowWidth = RestoreBounds.Width;
                settings.MainWindowHeight = RestoreBounds.Height;
                settings.MainWindowMaximized = true;
            }
            else
            {
                settings.MainWindowLeft = Left;
                settings.MainWindowTop = Top;
                settings.MainWindowWidth = oldw;
                settings.MainWindowHeight = oldh;
                settings.MainWindowMaximized = false;
            }
            settings.Save();
            try
            {
                if (_cabLay.ColorsChanged == true)
                    _cabLay.HighLightSegments(false);
            }
            finally
            {
                if (exit)
                    Environment.Exit(0);
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is TabControl tc && e.Source is TabControl)
            {
                barScale.Visibility = tabControl.SelectedIndex == 0 ? Visibility.Visible : Visibility.Collapsed;
                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        if (oldw < this.Width - 5)
                        {
                            oldw = this.Width;
                            oldh = this.Height;
                        }
                        this.MinWidth = 430;
                        this.SizeToContent = SizeToContent.WidthAndHeight;
                        this.ResizeMode = ResizeMode.CanMinimize;
                        ReadFirstTab();
                        txtStatus.Text = "Масштаб активного листа:";
                        cbScale.Text = Utilites.CurrentSheet.ScaleString;
                        break;
                    case 1:
                        if (oldw < this.Width - 5)
                        {
                            oldw = this.Width;
                            oldh = this.Height;
                        }
                        _cabStream.ReadStreams();
                        dgStreams.ItemsSource = _cabStream.Cables;
                        txtStatus.Text = string.Format("Кабелей в потоке: {0}", _cabStream.Cables.Count);
                        this.SizeToContent = SizeToContent.WidthAndHeight;
                        this.MinWidth = 430;
                        this.Width = 430;
                        this.MaxWidth = 430;
                        this.MinHeight = this.Height;
                        this.MaxHeight = 2000;
                        this.ResizeMode = ResizeMode.CanResize;
                        break;
                    case 2:
                        this.SizeToContent = SizeToContent.Manual;
                        this.MaxWidth = 2000;
                        this.MaxHeight = 2000;

                        this.Width = oldw;
                        this.Height = oldh;
                        oldw = this.Width;
                        oldh = this.Height;
                        this.ResizeMode = ResizeMode.CanResize;
                        txtStatus.Text = "Для расчета длин кабелей нажмите кнопку \"Загрузить кабели\"";
                        break;
                    case 3:
                        SetShelfTypes();
                        ReadSection();
                        this.SizeToContent = SizeToContent.Manual;
                        this.MaxWidth = 1500;
                        this.MaxHeight = 2000;

                        this.MinWidth = 800;
                        this.Width = 800;
                        this.Height = 400;
                        this.ResizeMode = ResizeMode.CanResize;
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Вкладка 1: раскладка

        #region Способы прокладки

        private ePathConstructor _cabLay;

        public void SetLayingStyles()
        {
            var layingLst = new List<LayingType>();
            if (eSettings.Profile.LayingTypes.Count>0)
               layingLst.AddRange(eSettings.Profile.LayingTypes);
            layingLst.Insert(0, new LayingType { Name = eConstants.NullString });
            cbLayingType.ItemsSource = layingLst;
            cbLayingType.SelectedIndex = 0;
            cbLayingType.DisplayMemberPath = "Name";

            ColorHeightGood = (Color)ColorConverter.ConvertFromString(string.IsNullOrEmpty(eSettings.Profile.СolorGoodNumber) ? "Black" : eSettings.Profile.СolorGoodNumber);
            ColorHeightBad = (Color)ColorConverter.ConvertFromString(string.IsNullOrEmpty(eSettings.Profile.СolorBadNumber) ? "Black" : eSettings.Profile.СolorBadNumber);
            ColorLayingGood = (Color)ColorConverter.ConvertFromString(string.IsNullOrEmpty(eSettings.Profile.СolorLayingGoodNumber) ? "Black" : eSettings.Profile.СolorLayingGoodNumber);
            ColorLayingBad = (Color)ColorConverter.ConvertFromString(string.IsNullOrEmpty(eSettings.Profile.СolorLayingBadNumber) ? "Black" : eSettings.Profile.СolorLayingBadNumber);

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ColorHeightGood"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ColorHeightBad"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ColorLayingGood"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ColorLayingBad"));
        }

        public void SetScalesList()
        {
            var scaleList = new List<string>();
            scaleList.Add(eConstants.NullString);
            if (eSettings.Profile.Scales.Count>0)
            {
                scaleList.AddRange(eSettings.Profile.Scales);
            }
            if (!string.IsNullOrEmpty(Utilites.CurrentSheet.ScaleString) &&!scaleList.Contains(Utilites.CurrentSheet.ScaleString) && Utilites.CurrentSheet.Scale>0)
            {
                scaleList.Add(Utilites.CurrentSheet.ScaleString);

            }
            cbScale.ItemsSource = scaleList;
            cbScale.Text = Utilites.CurrentSheet.ScaleString;
        }

        //Хук на выделенные сегменты
        private void hook_GetSegments(MouseHook.MSLLHOOKSTRUCT mouseStruct)
        {
            try
            {
                System.Drawing.Point p = new System.Drawing.Point { X = mouseStruct.pt.x, Y = mouseStruct.pt.y };
                IntPtr hwnd = WinApi.WindowFromPoint(p);
                string windowClassName = WinApi.EnumClassName(hwnd);
                if (windowClassName.StartsWith("AfxFrameOrView"))
                {
                    ReadFirstTab();
                }
            }
            catch (Exception ex)
            {
                //MessageWindow.ShowDialog($"{ex.Message}\n{ex.StackTrace}", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
            finally { e3c.DisposeE3Job(); }
        }

        private void ReadFirstTab()
        {
            var task = Task.Run(() =>
            {
                _cabLay.ReadSelectedSegments();
                _cabLay.ReadSelectedDevices();
                _cabLay.ReadSelectedSymbols();
            });
            task.Wait();
            cbHeight.IsEnabled = true;
            if (_cabLay.SelectedSegments.Count == 1)
            {
                cbHeight.Text = _cabLay.SelectedSegments[0].HeightString;
                try
                {
                    if (_cabLay.SelectedSegments[0].Laying != null)
                        cbLayingType.SelectedItem = _cabLay.SelectedSegments[0].Laying;
                    else
                        cbLayingType.SelectedIndex = 0;
                }
                catch { cbLayingType.SelectedIndex = 0; }
            }
            else
            {
                if (_cabLay.SelectedDevices.Count == 1)
                {
                    cbHeight.Text = _cabLay.SelectedDevices[0].HeightString;
                    cbLayingType.SelectedIndex = 0;
                }
                if (_cabLay.SelectedSymbols.Count == 1)
                {
                    if (_cabLay.SelectedSymbols[0].Laying != null)
                        cbLayingType.SelectedItem = _cabLay.SelectedSymbols[0].Laying;
                    else
                        cbLayingType.SelectedIndex = 0;
                    if (_cabLay.SelectedDevices.Count == 0)
                    {
                        cbHeight.Text = eConstants.NullString;
                        cbHeight.IsEnabled = false;
                    }
                }
                else
                {
                    cbLayingType.SelectedIndex = 0;
                    cbHeight.Text = eConstants.NullString;
                }
            }
        }

        private void btnBreakSegment_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            _cabLay.BreakSegment((bool)chkSaveLaying.IsChecked, eSettings.Profile.ChangeTypeSymbol, true);
            _hookOff = false;
            BaseWindow_Deactivated(this, null);

        }

        private void cbLayingType_DropDownClosed(object sender, System.EventArgs e)
        {
             _cabLay.SetLine((LayingType)cbLayingType.SelectedItem);
        }

        #endregion

        #region Высотные отметки

        //Переход вверх
        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            //_cabLay.HighLight(false);
            _cabLay.BreakSegment((bool)chkSaveLaying.IsChecked, eSettings.Profile.UpSymbol, false);
            //_cabLay.ShowHeightColors(chkShowColor.IsChecked);
            _hookOff = false;
        }

        //Переход вниз
        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            //_cabLay.HighLight(false);
            _cabLay.BreakSegment((bool)chkSaveLaying.IsChecked, eSettings.Profile.DownSymbol, false);
            //_cabLay.ShowHeightColors(chkShowColor.IsChecked);
            _hookOff = false;
        }

        //Применить высотную отметку
        private void btnApplyHeight_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            _cabLay.PlaceTemplate();
            _hookOff = false;
        }

        string oldval = "";
        private void cbHeight_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                SetHeight();
            if (e.Key == System.Windows.Input.Key.S)
            {
                oldval = cbHeight.Text;
            }
        }

        private void cbHeight_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.S)
            {
                cbHeight.Text = oldval;
            }
        }

        private void cbHeight_DropDownClosed(object sender, EventArgs e)
        {
            SetHeight();
        }

        private void SetHeight()
        {
            string txt = cbHeight.Text;
            if (txt == eConstants.NullString || txt == "...")
                txt = "";
            else if (!string.IsNullOrEmpty(txt) && txt[0] != '-' && txt[0] != '+')
            {
                txt = "+" + txt;
                cbHeight.Text = txt;
            }
            bool res = _cabLay.SetHeight(txt);
            if (res == false)
            {
                MessageWindow.ShowDialog("Введите значение в формате \"+1.000\"", "Неверный формат высотной отметки", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
            else
            {
                if (!string.IsNullOrEmpty(txt) && !cbHeight.Items.Contains(txt) && txt != "+0.000")
                {
                    cbHeight.Items.Add(txt);
                }
            }
            //_cabLay.ShowHeightColors(chkShowColor.IsChecked);
        }

        #endregion

        #region Инструменты

        private void btnCheckViews_Click(object sender, RoutedEventArgs e)
        {
            MessageWindow.ShowDialog(_cabLay.CheckViews(), "", "Проверка изображений", icon: MessageWindowImages.Information);
        }

        #endregion

        #region Предпросмотр 3D
        private void btn3D_Click(object sender, RoutedEventArgs e)
        {
            Utilites.CurrentSheet.Preview3D();
            if (File.Exists(Utilites.TempFile))
            {
                Preview3D w = new Preview3D(Utilites.CurrentSheet.HorizontalSegments, Utilites.TempFile);
                w.ShowDialog();
            }
        }

        #endregion

        #region Подсветка
        public Color ColorHeightBad { get; set;}
        public Color ColorHeightGood { get; set;}
        public Color ColorLayingBad { get; set;}
        public Color ColorLayingGood { get; set; }

        private void mnuResetHighlight_Click(object sender, RoutedEventArgs e)
        {
            _cabLay.HighLightSegments(true);
        }

        private void mnuHighlightHeight_Click(object sender, RoutedEventArgs e)
        {
            _cabLay.HighLightSegments(false, eSettings.Profile.NetSegmentHeightAttribute, eSettings.Profile.СolorGoodNumber, eSettings.Profile.СolorBadNumber);
        }

        private void mnuHighlightLaying_Click(object sender, RoutedEventArgs e)
        {
            _cabLay.HighLightSegments(false, eSettings.Profile.LayingTypeAttribute, eSettings.Profile.СolorLayingGoodNumber, eSettings.Profile.СolorLayingBadNumber);
        }

        private void cbScale_DropDownClosed(object sender, EventArgs e)
        {
            Utilites.CurrentSheet.ScaleString = cbScale.Text;
            Utilites.CurrentSheet.ToE3();
        }
        #endregion

        #endregion

        #region Вкладка 2: потоки

        private EStreamBuilder _cabStream;

        #region Чтение

        //Хук на потоки
        private void hook_GetStream(MouseHook.MSLLHOOKSTRUCT mouseStruct)
        {
            try
            {
                System.Drawing.Point p = new System.Drawing.Point { X = mouseStruct.pt.x, Y = mouseStruct.pt.y };
                IntPtr hwnd = WinApi.WindowFromPoint(p);
                string windowClassName = WinApi.EnumClassName(hwnd);
                if (windowClassName.StartsWith("AfxFrameOrView"))
                {
                    var task = Task.Run(() =>
                    {
                        _cabStream.ReadStreams();
                    });
                task.Wait();
                dgStreams.ItemsSource = _cabStream.Cables;
                    txtStatus.Text = string.Format("Кабелей в потоке: {0}", _cabStream.Cables.Count);
                }
        }
            //catch (Exception ex)
            //{
            //    MessageWindow.ShowDialog($"{ex.Message}\n{ex.InnerException.StackTrace}", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            //}
            finally { e3c.DisposeE3Job(); }
        }

        #endregion

        #region Удаление
        private void btnDeleteStream_Click(object sender, RoutedEventArgs e)
        {
            _cabStream.DeleteStream();
        }
        #endregion

        #region Построение
        private void btnBuildStream_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            this.IsEnabled = false;
            _cabStream.DrawStream((bool)chkStreamWithNote.IsChecked);
            _hookOff = false;
            this.IsEnabled = true;
            _hook.LeftButtonUp += hook_GetStream;
            _hook.Install();
        } 
        #endregion

        #region Обновление

        private void btnUpdateStream_Click(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
            string res = _cabStream.UpdateStreams(true);
            if (!string.IsNullOrEmpty(res))
                MessageWindow.ShowDialog(res, "Внимание", icon: MessageWindowImages.Warning);
            this.Topmost = true;
        }

        private void bwUpdateStreams(object sender, DoWorkEventArgs e)
        {
            string res = _cabStream.UpdateStreams(false);
            if (!string.IsNullOrEmpty(res))
                MessageWindow.ShowDialog(res, "Внимание", icon: MessageWindowImages.Warning);
        }

        private void bwUpdateStreamsComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            wFrm.Hide();
            this.Topmost = true;
            txtStatus.Text = string.Format("Обновлено потоков: {0}", _cabStream.RefreshCount);
            _hookOff = false;
        }

        private void btnUpdateAllStreams_Click(object sender, RoutedEventArgs e)
        {
            _hookOff = true;
            this.Topmost = false;
            var bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bwUpdateStreams);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwUpdateStreamsComplete);
            bw.RunWorkerAsync();
            wFrm.ShowDialog();
        }


        #endregion

        #endregion

        #region Вкладка 3: кабели

        #region Загрузка кабелей
        string _message = "";
        void bwRead(object sender, DoWorkEventArgs e)
        {
            _message = _cabLay.ReadAllCables();
        }

        void bwReadComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            wFrm.Hide();
            AddColumns();
            txtStatus.Text = string.Format("Загружено кабелей из проекта: {0}\nКоэффициент запаса кабелей: {1}%\nВремя последней загрузки: {2}", _cabLay.Cables.Count, _cabLay.StockPercent, DateTime.Now.ToShortTimeString());
            if (!string.IsNullOrEmpty(_message))
                MessageWindow.ShowDialog(_message, "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            this.Topmost = true;
        }



        private void btnLoadCables_Click(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
            var bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bwRead);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwReadComplete);
            bw.RunWorkerAsync();
            wFrm.ShowDialog();
        }

        #endregion

        #region Передача кабелей в Е3
        void bwWrite(object sender, DoWorkEventArgs e)
        {
            _message = _cabLay.WriteAllCables();
        }

        void bwWriteComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            wFrm.Hide();

            AddColumns();
            if (!string.IsNullOrEmpty(_message))
                MessageWindow.ShowDialog(_message, "", "Внимание!", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);

            this.Topmost = true;
        }

        private void btnWriteCables_Click(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
            var bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bwWrite);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwWriteComplete);
            bw.RunWorkerAsync();
            wFrm.ShowDialog();
        }

        #endregion

        #region Обработка DataGrid с показом всплывающей панели

        private void dgCables_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var sel = dgCables.SelectedIndex;
            if (sel > -1)
            {
                _cabLay.JumpToID(sel);
            }
        }

        private void dgCables_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var sel = dgCables.SelectedIndex;
            if (sel > -1)
            {
                flCables.IsOpen = true;
                flCables.Header = _cabLay.Cables[sel].Name;
                dgCablePath.ItemsSource = _cabLay.Cables[sel].NetSegments;
            }
        }

        private void dgCables_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //w.Hide();
        }

        private void dgCables_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void AddColumns()
        {
            dgCables.ItemsSource = null;
            for (int i = dgCables.Columns.Count - 1; i > 4; i--)
            {
                dgCables.Columns.RemoveAt(i);
            }
            for (int i = 0; i < eSettings.Profile.LayingTypes.Count; i++)
            {
                var col = new DataGridTextColumn();
                col.Header = eSettings.Profile.LayingTypes[i].Name;
                col.Binding = new Binding(string.Format("LengthsWithType[{0}].Length", i));
                col.IsReadOnly = true;
                col.Width = new DataGridLength(0.3, DataGridLengthUnitType.Star);
                dgCables.Columns.Add(col);
            }
            dgCables.ItemsSource = _cabLay.Cables;
        }

        #endregion

        #endregion

        #region Вкладка 4: разрезы

        private AllSectionBuilder _section;

        private void hook_GetSection(MouseHook.MSLLHOOKSTRUCT mouseStruct)
        {
            System.Drawing.Point p = new System.Drawing.Point { X = mouseStruct.pt.x, Y = mouseStruct.pt.y };
            IntPtr hwnd = WinApi.WindowFromPoint(p);
            string windowClassName = WinApi.EnumClassName(hwnd);
            if (windowClassName.StartsWith("AfxFrameOrView"))
            {
                ReadSection();
            }
        }

        private void ReadSection()
        {
            try
            {
                var task = Task.Run(() =>
                {
                    _section.ReadSection();
                });
                task.Wait();
                dgLeftSide.ItemsSource = null;
                dgRightSide.ItemsSource = null;
                txtStatus.Text = string.Empty;

                if (_section.TrenchSection != null)
                {
                    if (_section.TrenchSection.CurrentTrench != null)
                    {
                        dgTrenchCables.ItemsSource = _section.TrenchSection.Cables;

                        var cv = (CollectionView)CollectionViewSource.GetDefaultView(dgTrenchCables.ItemsSource);
                        var pgd = new PropertyGroupDescription("Class");
                        cv.GroupDescriptions.Add(pgd);

                        cbTrenchTypes.ItemsSource = _section.TrenchSection.AvaibleTrenches;
                        cbTrenchTypes.SelectedItem = _section.TrenchSection.CurrentTrench;
                        tabTrench.IsSelected = true;
                        string mes = "";
                        if (!string.IsNullOrEmpty(Utilites.SelectedItem.TrenchSectionNumber))
                        {
                            mes = $"Разрез: {Utilites.SelectedItem.TrenchSectionNumber}-{Utilites.SelectedItem.TrenchSectionNumber}. ";
                        }
                        mes += _section.Message;
                        txtStatus.Text = mes;
                    }
                    else
                    {
                        tabNothing.IsSelected = true;
                        txtStatus.Text = $"Ошибка: {_section.Message}";
                    }
                }
                else if (_section.MetalconstructionSection != null)
                {
                    if (_section.MetalconstructionSection.Construction != null)
                    {
                        tabDucts.IsSelected = true;
                        if (_section.MetalconstructionSection.Construction.LeftStand != null)
                        {
                            dgLeftSide.ItemsSource = _section.MetalconstructionSection.Construction.LeftStand.Shelfs;
                        }
                        if (_section.MetalconstructionSection.Construction.RightStand != null)
                        {
                            dgRightSide.ItemsSource = _section.MetalconstructionSection.Construction.RightStand.Shelfs;
                        }
                        spnShelfCount.Minimum = _section.MetalconstructionSection.Construction.ShelfCount;
                        spnShelfCount.Value = _section.MetalconstructionSection.Construction.ShelfCount;
                        spnSides.Value = _section.MetalconstructionSection.Construction.Sides;
                        cbShelfTypes.SelectedItem = _section.MetalconstructionSection.Construction.ShelfType;
                        string mes = "";
                        if (!string.IsNullOrEmpty(Utilites.SelectedItem.DuctSectionNumber))
                        {
                            mes += $"Разрез: {Utilites.SelectedItem.DuctSectionNumber}-{Utilites.SelectedItem.DuctSectionNumber}. ";
                        }
                        if (_section.MetalconstructionSection.Construction.NewLayout)
                        {
                            mes = "Параметры металоконструкции не заданы!";
                        }
                        else
                        {
                            mes += _section.MetalconstructionSection.Construction.ConstructionParameters;
                        }
                        txtStatus.Text = mes;
                    }
                    else
                    {
                        txtStatus.Text = $"Ошибка: {_section.Message}";
                        tabNothing.IsSelected = true;
                    }
                }
                else
                {
                    tabNothing.IsSelected = true;
                    txtStatus.Text = "Для построения разреза выберите участок трассы с определенным в профиле способом прокладки!";
                }
            }
#if DEBUG
            catch (Exception ex)
            {
                MessageWindow.ShowDialog($"{ex.Message}\n{ex.StackTrace}", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
#endif
            finally { e3c.DisposeE3Job(); }
        }

        private void btnBuildSection_Click(object sender, RoutedEventArgs e)
        {
            _hook.Uninstall();
            _hookOff = true;
            this.IsEnabled = false;
            WindowState oldState = this.WindowState;
            this.WindowState = WindowState.Minimized;
            _section.BuildSection(true);
            this.IsEnabled = true;

            ReadSection();
            this.WindowState = oldState;
            _hookOff = false;
            _hook.LeftButtonUp += hook_GetSection;
            _hook.Install();
        }

        private void btnDeleteSection_Click(object sender, RoutedEventArgs e)
        {
            _section.DeleteSection();
        }

        private void btnApplySection_Click(object sender, RoutedEventArgs e)
        {
            if (_section.MetalconstructionSection != null)
            {
                UpdateConstruction();
            }
            _section.BuildSection(false);
            ReadSection();
        }

        #region Траншеи

        public event PropertyChangedEventHandler PropertyChanged;

        private bool isEditingTrench;

        public TrenchType CurrentTrench
        {
            get
            {
                return _section.TrenchSection.CurrentTrench;
            }
            set
            {
                if (object.ReferenceEquals(_section.TrenchSection.CurrentTrench, value) != true)
                {
                    _section.TrenchSection.CurrentTrench = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedTrench"));
                    IsEditingTrench = false;
                }
            }
        }

        public bool IsEditingTrench
        {
            get
            {
                return isEditingTrench;
            }
            set
            {
                if (isEditingTrench.Equals(value) != true)
                {
                    isEditingTrench = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEditingTrench"));
                }
            }
        }

        private void cbTrenchTypes_DropDownClosed(object sender, EventArgs e)
        {
            cbTrenchTypes.SelectedItem = _section.TrenchSection.CurrentTrench;
        }

        #endregion

        #region Металлоконструкции

        public void SetShelfTypes()
        {
            cbShelfTypes.ItemsSource = eSettings.Profile.ShelfTypes;
            cbShelfTypes.SelectedIndex = 0;
            cbShelfTypes.DisplayMemberPath = "Width";

            cbMetalConstruction.ItemsSource = eSettings.Profile.MetalconstructionTypes;
            cbMetalConstruction.SelectedIndex = 0;
            cbMetalConstruction.DisplayMemberPath = "Name";

            spnFullnes.Value = eSettings.Profile.FullPercent;
        }

        private void btnUpdateConstruction_Click(object sender, RoutedEventArgs e)
        {
            UpdateConstruction();
        }

        private void UpdateConstruction()
        {
            eSettings.Profile.FullPercent = (int)spnFullnes.Value;
            dgLeftSide.ItemsSource = null;
            dgRightSide.ItemsSource = null;
            string s = $"{cbMetalConstruction.Text}#{spnSides.Value}#{cbShelfTypes.Text}#{spnShelfCount.Value}";
            _section.MetalconstructionSection.Construction = new EMetalconstructionWithStands(s);
            _section.MetalconstructionSection.ReadSection(s);

            if (_section.MetalconstructionSection.Construction.LeftStand != null)
            {
                dgLeftSide.ItemsSource = _section.MetalconstructionSection.Construction.LeftStand.Shelfs;
            }
            if (_section.MetalconstructionSection.Construction.RightStand != null)
            {
                dgRightSide.ItemsSource = _section.MetalconstructionSection.Construction.RightStand.Shelfs;
            }
        }


        #endregion

        #endregion


    }
}