﻿using Layout.Configuration;
using Layout.Properties;
using Pointcad.Dragon.Windows;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace Layout.Views
{
    public partial class CalculateWindow
    {
        EMetalCounter _metalCounter;

        #region Constructors

        public CalculateWindow()
		{
            InitializeComponent();
			Settings settings = Settings.Default;
            Left = settings.MainWindowLeft;
            Top = settings.MainWindowTop;
            oldw = settings.MainWindowWidth;
            oldh = settings.MainWindowHeight;
            this.Topmost = true;
            _metalCounter = new EMetalCounter();
            dgConstructions.ItemsSource = _metalCounter.MaterialsList;
            //Environment.Exit(0);
        }

        #endregion

        #region Прогресс

        private WaitingForm wFrm = new WaitingForm();


        private void SetProgress(int cnt, int max, string text = "")
        {
            var d = wFrm.pBar.Dispatcher;
            if (!d.CheckAccess())
            {
                d.Invoke(new ProgressHandler(SetProgress), cnt, max, text);
            }
            else
            {
                wFrm.Title = text;
                wFrm.pBar.Maximum = max + 1;
                if (cnt < max) wFrm.pBar.Value = cnt;
            }
        } 

        #endregion

        #region Кнопки в заголовке окна

        private void btnShowSettings_Click(object sender, RoutedEventArgs e)
        {
            var profileWindow = new ProfileWindow() { Owner = this };
            profileWindow.ShowDialog();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = e3c.app.GetId();
                var e3proc = Process.GetProcessById(id);
                string productName = e3proc.MainModule.FileVersionInfo.ProductName;
                string version = e3proc.MainModule.FileVersionInfo.ProductVersion.Replace(",", ".");
                string build = e3c.app.GetBuild();
                var aboutWindow = new AboutWindow(productName, version, build, this);
                aboutWindow.ShowDialog();
            }
            catch { }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory + "CableLayoutHelp.pdf";
            try
            {
                Process.Start(filename);
            }
            catch (Exception ex)
            {
                MessageWindow.ShowDialog(ex.Message + ": " + filename, "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
        }

        #endregion

        #region Methods

        private void BaseWindow_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.S)
            {
                btnShowSettings_Click(this, null);
            }
        }

        private double oldw = 0;
        private double oldh = 0;

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Settings settings = Settings.Default;
            if (WindowState == WindowState.Maximized)
            {
                settings.MainWindowLeft = RestoreBounds.Left;
                settings.MainWindowTop = RestoreBounds.Top;
                settings.MainWindowWidth = RestoreBounds.Width;
                settings.MainWindowHeight = RestoreBounds.Height;
                settings.MainWindowMaximized = true;
            }
            else
            {
                settings.MainWindowLeft = Left;
                settings.MainWindowTop = Top;
                settings.MainWindowWidth = oldw;
                settings.MainWindowHeight = oldh;
                settings.MainWindowMaximized = false;
            }
            settings.Save();
        }

        #endregion

        private void BaseWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            _metalCounter.DeleteOldMaterials();
        }

        private void btnWrite_Click(object sender, RoutedEventArgs e)
        {
            _metalCounter.Write();
            //MessageWindow.ShowDialog("Расчет металлоконструкций завершен!", "", "E3.Instrumentation Планы расположения", icon: Pointcad.Dragon.Windows.MessageWindowImages.Information);
            //this.Close();
        }

        private void dgConstructions_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                dgConstructions.Items.Refresh();
            }
            catch { }
        }
    }
}