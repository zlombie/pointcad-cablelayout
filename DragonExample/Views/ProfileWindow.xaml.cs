﻿using Layout.Configuration;
using DragonExample.Database;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using Pointcad.Dragon.Windows;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Linq;
using Layout.Service;
using System.IO;

namespace Layout.Views
{
	public partial class ProfileWindow
	{
        #region Constructors

		internal ProfileWindow()
		{
			InitializeComponent();
            CurrentProfile = ObjectCopier.Clone(eSettings.Profile);
            InitializeBinding();
            CurrentProfile.IsSaved = true;
            cboPlaceMode.ItemsSource = eConstants.PlaceModes;
        }

        private void InitializeBinding()
        {
            DataContext = CurrentProfile;
            dgLayingStyles.ItemsSource = CurrentProfile.LayingTypes;
            dataGridClassification.ItemsSource = CurrentProfile.CableTypes;
            dataGridTrench.ItemsSource = CurrentProfile.TrenchTypes;
            dataGridBetween.ItemsSource = CurrentProfile.BetweenTypes;
            grdMetalconstructions.ItemsSource = CurrentProfile.MetalconstructionTypes;
            //grdDucts.ItemsSource = CurrentProfile.DuctTypes;
            //grdShelfs.ItemsSource = CurrentProfile.ShelfTypes;
            //grdStands.ItemsSource = CurrentProfile.StandTypes;
            cboSettingsFile.ItemsSource = eSettings.RegistryLastPaths;
            cboSettingsFile.Text = CurrentProfile.FileName;
            cbScales.Text = CurrentProfile.ScalesSum;
        }

        #endregion

        #region Properties

        internal eProfile CurrentProfile { get; set; }
        internal bool WasSaved { get; set; }

        #endregion


        #region Drag and Drop Rows

        /// <summary>
        /// DraggedItem Dependency Property
        /// </summary>
        public static readonly DependencyProperty DraggedItemProperty =
            DependencyProperty.Register("DraggedItem", typeof(CableType), typeof(ProfileWindow));

        /// <summary>
        /// Gets or sets the DraggedItem property.  This dependency property 
        /// indicates ....
        /// </summary>
        public CableType DraggedItem
        {
            get { return (CableType)GetValue(DraggedItemProperty); }
            set { SetValue(DraggedItemProperty, value); }
        }

        public bool IsEditing { get; set; }

        private void OnBeginEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            IsEditing = true;
            //in case we are in the middle of a drag/drop operation, cancel it...
            if (IsDragging) ResetDragDrop();
        }

        private void OnEndEdit(object sender, DataGridCellEditEndingEventArgs e)
        {
            IsEditing = false;
        }



        /// <summary>
        /// Keeps in mind whether
        /// </summary>
        public bool IsDragging { get; set; }

        /// <summary>
        /// Initiates a drag action if the grid is not in edit mode.
        /// </summary>
        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsEditing) return;

            var row = UIHelpers.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(dataGridClassification));
            if (row == null || row.IsEditing || row.IsNewItem) return;

            //set flag that indicates we're capturing mouse movements
            IsDragging = true;
            DraggedItem = (CableType)row.Item;
        }


        /// <summary>
        /// Completes a drag/drop operation.
        /// </summary>
        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!IsDragging || IsEditing)
            {
                return;
            }
            //reset
            ResetDragDrop();
            //get the target item
            CableType targetItem = (CableType)dataGridClassification.SelectedItem;

            if (targetItem == null || !ReferenceEquals(DraggedItem, targetItem))
            {
                //remove the source from the list
                CurrentProfile.CableTypes.Remove(DraggedItem);

                //get target index
                var targetIndex = CurrentProfile.CableTypes.IndexOf(targetItem);

                //move source at the target's location
                CurrentProfile.CableTypes.Insert(targetIndex, DraggedItem);

                //select the dropped item
                dataGridClassification.SelectedItem = DraggedItem;
                dataGridClassification.ItemsSource = null;
                dataGridClassification.ItemsSource = CurrentProfile.CableTypes;
            }
        }


        /// <summary>
        /// Closes the popup and resets the
        /// grid to read-enabled mode.
        /// </summary>
        private void ResetDragDrop()
        {
            IsDragging = false;
            popup1.IsOpen = false;
            dataGridClassification.IsReadOnly = false;
        }


        /// <summary>
        /// Updates the popup's position in case of a drag/drop operation.
        /// </summary>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!IsDragging || e.LeftButton != MouseButtonState.Pressed) return;

            //display the popup if it hasn't been opened yet
            if (!popup1.IsOpen)
            {
                //switch to read-only mode
                dataGridClassification.IsReadOnly = true;

                //make sure the popup is visible
                popup1.IsOpen = true;
            }


            Size popupSize = new Size(popup1.ActualWidth, popup1.ActualHeight);
            popup1.PlacementRectangle = new Rect(e.GetPosition(this), popupSize);

            //make sure the row under the grid is being selected
            Point position = e.GetPosition(dataGridClassification);
            var row = UIHelpers.TryFindFromPoint<DataGridRow>(dataGridClassification, position);
            if (row != null) dataGridClassification.SelectedItem = row.Item;
        }

        #endregion

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.Filter = "Файлы профиля (*.Layout-Profile)|*.Layout-Profile";
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = false;
            if (myDialog.ShowDialog() == true)
            {
                var tempProfile = eSettings.Load(myDialog.FileName);
                if (tempProfile != null)
                {
                    CurrentProfile = tempProfile;
                    InitializeBinding();
                }
                else
                {
                    MessageWindow.ShowDialog($"Невозможно открыть файл профиля {myDialog.FileName}", "", "[E3.Instrumentation] Планы расположения", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning, owner: this);
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            eSettings.Profile = CurrentProfile;
            eSettings.Profile.Save();
        }

        private void btnSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog myDialog = new SaveFileDialog();
            myDialog.Filter = "Файлы профиля (*.Layout-Profile)|*.Layout-Profile";
            myDialog.FileName = Path.GetFileNameWithoutExtension(CurrentProfile.FileName);
            if (myDialog.ShowDialog() == true)
            {
                CurrentProfile.FileName = myDialog.FileName;
                CurrentProfile.Save();
                var tempProfile = eSettings.Load(CurrentProfile.FileName);
                if (tempProfile != null)
                {
                    CurrentProfile = tempProfile;
                    InitializeBinding();
                }
            }
        }

        private void Me_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            eSettings.WriteProfileToRegistry();
            if (!CurrentProfile.IsSaved || CurrentProfile.FileName!=eSettings.Profile.FileName)
            {
                var res = MessageWindow.ShowDialog("Вы хотите сохранить изменения в активном профиле настроек?", "", "[E3.Instrumentation] Планы расположения", MessageWindowButtons.YesNo, MessageWindowImages.Question, this);
                if (res == MessageDialogResults.Yes)
                {
                    eSettings.Profile = CurrentProfile;
                    eSettings.Profile.Save();
                }
            }
        }

        private void cbScales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var regex = new Regex(@"1:\d+");
                if (regex.IsMatch(cbScales.Text))
                {
                    if (!CurrentProfile.Scales.Contains(cbScales.Text))
                    {
                        CurrentProfile.Scales.Add(cbScales.Text);
                    }
                }
                else
                {
                    MessageWindow.ShowDialog("Введите значение в формате \"1:100\"", "Неверный формат масштаба", "Ошибка", icon: MessageWindowImages.Warning);
                }
                cbScales.Text = CurrentProfile.ScalesSum;
                var comboTextBoxChild = cbScales.FindChild<TextBox>("PART_EditableTextBox");
                comboTextBoxChild.SelectAll();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            if (null == fe)
            {
                return;
            }
            var tmp = fe.DataContext;
            if (tmp!=null)
            {
                CurrentProfile.Scales.Remove((string)tmp);
                cbScales.Text = CurrentProfile.ScalesSum;
                //cbScales.IsDropDownOpen = false;
            }
        }

        private void cbScales_DropDownClosed(object sender, System.EventArgs e)
        {
            cbScales.Text = CurrentProfile.ScalesSum;
        }

        private void CbScales_TextInput(object sender, TextCompositionEventArgs e)
        {
            var t = e.Text;
        }

        private void ShowMaterialsEditor(object sender, RoutedEventArgs e)
        {
            eItemWithMaterialComponents currentLaying = null;
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    if (row.Item is eItemWithMaterialComponents)
                    {
                        currentLaying = row.Item as eItemWithMaterialComponents;
                    }
                    break;
                }
            }
            if (currentLaying!=null)
            {
                MaterialsWindow tempForm = new MaterialsWindow(CurrentProfile, currentLaying.Materials);
                tempForm.Owner = this;
                tempForm.ShowDialog();
                currentLaying.RaisePropertyChanged("MaterialsCount");
            }
        }


        private void ShowShelfsEditor(object sender, RoutedEventArgs e)
        {
            eItemWithMaterialComponents currentLaying = null;
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    if (row.Item is eItemWithMaterialComponents)
                    {
                        currentLaying = row.Item as eItemWithMaterialComponents;
                    }
                    break;
                }
            }
            if (currentLaying != null)
            {
                MaterialsWindow tempForm = new MaterialsWindow(CurrentProfile, currentLaying.Materials);
                tempForm.Owner = this;
                tempForm.ShowDialog();
                currentLaying.RaisePropertyChanged("MaterialsCount");
            }
        }

        private void ShowDuctsEditor(object sender, RoutedEventArgs e)
        {
            eItemWithMaterialComponents currentLaying = null;
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    if (row.Item is eItemWithMaterialComponents)
                    {
                        currentLaying = row.Item as eItemWithMaterialComponents;
                    }
                    break;
                }
            }
            if (currentLaying != null)
            {
                MaterialsWindow tempForm = new MaterialsWindow(CurrentProfile, currentLaying.Materials);
                tempForm.Owner = this;
                tempForm.ShowDialog();
                currentLaying.RaisePropertyChanged("MaterialsCount");
            }
        }

        private void cboSettingsFile_DropDownClosed(object sender, EventArgs e)
        {
            var tempProfile = eSettings.Load(cboSettingsFile.Text);
            if (tempProfile != null)
            {
                CurrentProfile = tempProfile;
                InitializeBinding();
            }
            else
            {
                MessageWindow.ShowDialog($"Невозможно открыть файл профиля {cboSettingsFile.Text}", "", "[E3.Instrumentation] Планы расположения", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning, owner: this);
                eSettings.RegistryLastPaths.Remove(cboSettingsFile.Text);
                if (eSettings.RegistryLastPaths.Count>0)
                    cboSettingsFile.SelectedIndex = 0;
            }
        }
    }
}