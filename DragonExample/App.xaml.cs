﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using DragonExample.Database;
using Pointcad.Log;
using System.Threading;
using Layout;
using Layout.Configuration;
using System.IO;
using Layout.Views;
using Pointcad.Dragon.Windows;
using System.Linq;
using FlexNet;
using System.Diagnostics;
using Layout.Service;

namespace DragonExample
{
	public partial class App
	{

        /// <summary>
        /// Имя продукта
        /// </summary>
        private const string FlexNetProductName = "E3-Layout";

        /// <summary>
        /// версия продукта
        /// </summary>
        private const string FlexNetProductVersion = "4.0";


        #region Methods


        private static void ConnectToE3SeriesAndReadDatabase()
        {
            try
            {
                DatabaseReader.ReadData(e3c.app);
            }
            catch (Exception exception)
            {

                LogHelper.Logger.Warn(exception);
            }
        }

        private Mutex _instanceMutex = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            var args = e.Args.ToList();
            e3c.InitE3Objects();
            var readDB = Task.Run(() => { ConnectToE3SeriesAndReadDatabase(); });
            base.OnStartup(e);
#if !DEBUG
            // Проверка подключения к нужному идентификатору
            var pidArg = args.FirstOrDefault(ctx => ctx.StartsWith(@"pid=", StringComparison.OrdinalIgnoreCase));
            if (pidArg == null || pidArg.Length < 4)
            {
                // Запуск не с панели
                Shutdown(0);
                return;
            }
            if (!int.TryParse(pidArg.Substring(4), out int ecubeId) || ecubeId != E3Connection.PID)
            {
                MessageBox.Show($"{pidArg} {E3Connection.PID}");
                // Запуск не с панели
                Shutdown(0);
                return;
            }
#endif
            Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
            _instanceMutex = new Mutex(true, @"Global\Layout", out bool createdNew);
            if (!createdNew)
            {
                var currentProcess = Process.GetCurrentProcess();
                try
                {
                    foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName, currentProcess.MachineName))
                    {
                        if (process.Id != currentProcess.Id)
                        {
                            IntPtr hWnd = process.MainWindowHandle;
                            if (hWnd != IntPtr.Zero) WinApi.SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogHelper.Logger.Warn(exception);

                    // Вывод сообщения, если не удалось другое приложение перевести на передний план
                    MessageWindow.ShowDialog($"Приложение \"{AppDomain.CurrentDomain.FriendlyName}\" уже запущено!{Environment.NewLine}Запущенное приложение закрыто!",
                                    "Предупреждение", "",
                                    MessageWindowButtons.Ok,
                                    MessageWindowImages.Warning);
                }
                _instanceMutex = null;
                Shutdown(0);
                return;
            }


#if !DEBUG
            Helper.Initialize();
            bool retF2 = Helper.CheckOut(FlexNetProductName, FlexNetProductVersion);
            if (!retF2)
            {
                MessageWindow.ShowDialog(Helper.GetErrorString(), "", "Ошибка лицензии", MessageWindowButtons.Ok, MessageWindowImages.Warning);
                Shutdown(0);
                return;
            }
#endif

            eSettings.ReadProfileFromRegistry();
            
            string param = "";
            if (e.Args.Length>1)
            {
                param = e.Args[1].ToLower();
            }
            switch (param)
            {
                case "dwgupdate":
                    {
                        Acad _acad = new Acad();
                        _acad.RefreshDWG();
                        Shutdown(0);
                        break;
                    }
                case "dwgload":
                    {
                        var dialog = new Microsoft.Win32.OpenFileDialog();
                        dialog.Filter = "";
                        dialog.DefaultExt = ".dwg"; // Default file extension
                        dialog.Filter = "Файлы AutoCAD (*.dwg)|*.dwg|Файлы DXF (*.dxf)|*.dxf"; // Filter files by extension
                        dialog.Title = "Загрузка обновляемой подложки";
                        if (dialog.ShowDialog() == true)
                        {
                            Acad _acad = new Acad();
                            var fullPath = dialog.FileName;
                            _acad.LoadDWG(fullPath, 0, "");
                        }
                        Shutdown(0);
                        break;
                    }
                case "settings":
                    {
                        readDB.Wait();
                        ProfileWindow pw = new ProfileWindow();
                        pw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        pw.Topmost = true;
                        pw.Show();
                        break;
                    }
                case "calc":
                    {
                        StartupUri = new Uri("Views/CalculateWindow.xaml", UriKind.Relative);
                        break;
                    }
                default:
                    {
                        StartupUri = new Uri("Views/MainWindow.xaml", UriKind.Relative);
                    }
                    break;
            }         
        }

        protected override void OnExit(ExitEventArgs e)
        {
            if (_instanceMutex != null)
                _instanceMutex.ReleaseMutex();
            base.OnExit(e);
        }
#endregion
    }
}