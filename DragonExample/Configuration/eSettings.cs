﻿using Layout.Service;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Layout.Configuration
{
    static class eSettings
    {

        public static eProfile Profile { get; set; }

        public static string       RegistryCurrentPath { get; private set; }
        public static ObservableCollection<string> RegistryLastPaths { get; private set; }

        private static string subKey = $@"Software\{eConstants.Developer}\{eConstants.Product}\Layout\4";
        public static void ReadProfileFromRegistry()
        {
            RegistryLastPaths = new ObservableCollection<string>();
            string defaultFile = eConstants.DefaultProfilePath;
            //HKLM
            var dfltRegKey = Registry.LocalMachine.OpenSubKey(subKey, false);
            if (dfltRegKey != null)
            {
                var dfltVal = dfltRegKey.GetValue(eConstants.SettingsFileRegKey);
                if (dfltVal != null)
                {
                    if (File.Exists(dfltVal.ToString()))
                        defaultFile = dfltVal.ToString();
                }
            }
            //HKCU
            RegistryKey k = Registry.CurrentUser.OpenSubKey(subKey, true);
            if (k == null)
            {
                k = Registry.CurrentUser.CreateSubKey(subKey);
            }
            //Текущий файл
            var p = k.GetValue(eConstants.SettingsFileRegKey);
            if (p == null && !string.IsNullOrEmpty(defaultFile))
            {
                k.SetValue(eConstants.SettingsFileRegKey, defaultFile);
                p = defaultFile;
            }
            RegistryCurrentPath = p.ToString();
            //Последние файлы
            var valNames = k.GetValueNames();
            for (int i = 5; i >= 1; i--)
            {
                if (valNames.Length > i && valNames[i] == $"File{i}")
                {
                    var tmpVal = k.GetValue(valNames[i]);
                    if (tmpVal != null && !RegistryLastPaths.Contains(tmpVal))
                    {
                        RegistryLastPaths.Add(tmpVal.ToString());
                    }
                }
            }
            if (File.Exists(RegistryCurrentPath))
            {
                Profile = Load(RegistryCurrentPath) ?? new eProfile();
            }
            else
            {
                Profile = new eProfile();
                //return $"Файл профиля {RegCurrentPath} не найден!";
            }
        }

        public static eProfile Load(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    return Deserialize(path);
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static eProfile Deserialize(string path)
        {
            var stream = new FileStream(path, FileMode.Open);
            try
            {
                XmlSerializer xsSubmit = new XmlSerializer(typeof(eProfile));
                var tmpP = (eProfile)xsSubmit.Deserialize(stream);
                tmpP.FileName = path;
                if (!RegistryLastPaths.Contains(path))
                {
                    RegistryLastPaths.Insert(0, path);
                }
                return tmpP;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                stream.Close();
            }
        }

        public static void WriteProfileToRegistry()
        {
            try
            {
                //HKCU
                RegistryKey k = Registry.CurrentUser.OpenSubKey(subKey, true);
                if (k == null)
                {
                    k = Registry.CurrentUser.CreateSubKey(subKey);
                }
                //Текущий файл
                k.SetValue(eConstants.SettingsFileRegKey, Profile.FileName);

                for (int i = 1; i <= Math.Min(5, RegistryLastPaths.Count); i++)
                {
                    //Текущий файл
                    k.SetValue($"File{i}", RegistryLastPaths[i - 1]);
                }
            }
            catch
            {

            }
        }



    }
}
