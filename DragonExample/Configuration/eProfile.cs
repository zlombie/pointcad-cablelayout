﻿using Layout.Service;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Xml.Serialization;

namespace Layout.Configuration
{
    [Serializable]
    [Magic]
    public sealed class eProfile : INotifyPropertyChanged
    {

        #region INotifyPropertyChanged

        public bool Save()
        {
            bool res = false;
            //if (DuctTypes.Count > 0)
            //{
            //    DuctTypes = eProfileReader.Profile.DuctTypes.OrderBy(o => o.S).ToList();
            //}
            //if (ShelfTypes.Count > 0)
            //{
            //    ShelfTypes = eProfileReader.Profile.ShelfTypes.OrderBy(o => o.Width).ToList();
            //}

            if (Path.IsPathRooted(FileName))
            {
                string directoryPath = Path.GetDirectoryName(FileName);
                if (!string.IsNullOrEmpty(directoryPath))
                {
                    if (!Directory.Exists(directoryPath))
                    {
                        try
                        {
                            Directory.CreateDirectory(directoryPath);
                        }
                        catch (Exception)
                        {
                            res = false;
                        }
                    }
                    try
                    {
                        res = Serialize(this);
                        if (res)
                            IsSaved = true;
                    }
                    catch (Exception)
                    {
                        res = false;
                    }
                }
            }
            else
            {
                res = false;
            }
            return res;
        }

        private bool Serialize (eProfile profile)
        {
            Stream stream = new FileStream(profile.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            try
            {
                XmlSerializer xsSubmit = new XmlSerializer(typeof(eProfile));
                xsSubmit.Serialize(stream, profile);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                stream.Close();
            }
        }

        private void RaisePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            IsSaved = false;
        }
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Служебные поля

        public string FileName;
        public bool IsSaved = true;

        #endregion

        private string _metalconstructionParametersAttribute;
        private string _ductSectionNumberAttribute;
        private string _ductSectionSymbolVert;
        private string _ductSectionSymbolHor;
        private string _ductParametersAttribute;
        private string _trenchSectionSymbolVert;
        private string _trenchSectionSymbolHor;
        private string _trenchSectionAttribute;
        private string _trenchParametersAttribute;
        private string _trenchCableDepthAttribute;
        private string _trenchMaterialAttribute;
        private string _dWGFileAttribute;
        private string _layingTypeAttribute;
        private string _layingSymAttribute;
        private string _netSegmentHeightAttribute;
        private string _deviceHeightAttribute;
        private string _symbolHeightAttribute;
        private string _colorAttribute;
        private string _layingMaterialAttribute;
        private string _changeTypeSymbol;
        private string _changeTypeInteractiveSymbol;
        private string _downSymbol;
        private string _upSymbol;
        private string _nodeSymbol;
        private string _heightTemplateSymbol;
        private string _graphAttribute;
        private string _signalAttribute;
        private string _streamAttribute;
        private string _gidAttribute;
        private string _lengthAttribute;
        private string _stockAttribute;
        private string _scaleAttribute;
        private string _additionalLengthAttribute;
        private string _noteSymbol;
        private string _streamNoteTemplate;
        private string _treeName;


        #region Типы схем
        public int SchemeNumber { get; set; }
        public int DevNumber { get; set; }

        #endregion

        #region Раскладка кабелей: основные
        public string СolorGoodNumber { get; set; }
        public string СolorBadNumber { get; set; }
        public string СolorLayingGoodNumber { get; set; }
        public string СolorLayingBadNumber { get; set; }
        public double LineWidth { get; set; }

        private ObservableCollection<string> _scales;
        public ObservableCollection<string> Scales
        {
            get
            {
                {
                    if (_scales == null)
                        _scales = new ObservableCollection<string>();
                    return _scales;
                }
            }
            set
            {
                if (_scales != value)
                {
                    _scales = value;
                    RaisePropertyChanged("ScalesSum");
                }
            }
        }

        [XmlIgnore]
        public string ScalesSum { get { return string.Join(",", Scales); } }
        #endregion

        #region Раскладка кабелей: символы
        public string ChangeTypeSymbol { get => _changeTypeSymbol ?? string.Empty; set => _changeTypeSymbol = value; }
        public string ChangeTypeInteractiveSymbol { get => _changeTypeInteractiveSymbol ?? string.Empty; set => _changeTypeInteractiveSymbol = value; }
        public string DownSymbol { get => _downSymbol ?? string.Empty; set => _downSymbol = value; }
        public string UpSymbol { get => _upSymbol ?? string.Empty; set => _upSymbol = value; }
        public string NodeSymbol { get => _nodeSymbol ?? string.Empty; set => _nodeSymbol = value; }
        public string HeightTemplateSymbol { get => _heightTemplateSymbol ?? string.Empty; set => _heightTemplateSymbol = value; }

        #endregion

        #region Раскладка кабелей: атрибуты
        public string LayingTypeAttribute { get => _layingTypeAttribute ?? string.Empty; set => _layingTypeAttribute = value; }
        public string LayingSymAttribute { get => _layingSymAttribute ?? string.Empty; set => _layingSymAttribute = value; }
        public string NetSegmentHeightAttribute { get => _netSegmentHeightAttribute ?? string.Empty; set => _netSegmentHeightAttribute = value; }
        public string DeviceHeightAttribute { get => _deviceHeightAttribute ?? string.Empty; set => _deviceHeightAttribute = value; }
        public string SymbolHeightAttribute { get => _symbolHeightAttribute ?? string.Empty; set => _symbolHeightAttribute = value; }
        public string ColorAttribute { get => _colorAttribute ?? string.Empty; set => _colorAttribute = value; }
        public string LayingMaterialAttribute { get => _layingMaterialAttribute ?? string.Empty; set => _layingMaterialAttribute = value; }

        private ObservableCollection<LayingType> _layingTypes;
        public ObservableCollection<LayingType> LayingTypes
        {
            get
            {
                {
                    if (_layingTypes == null)
                        _layingTypes = new ObservableCollection<LayingType>();
                    return _layingTypes;
                }
            }
            set
            {
                if (_layingTypes != value)
                {
                    _layingTypes = value;
                }
            }
        }

        #endregion

        #region Расчет длин кабелей: атрибуты
        public string LengthAttribute { get => _lengthAttribute ?? string.Empty; set => _lengthAttribute = value; }
        public string StockAttribute { get => _stockAttribute ?? string.Empty; set => _stockAttribute = value; }
        public string ScaleAttribute { get => _scaleAttribute ?? string.Empty; set => _scaleAttribute = value; }
        public string AdditionalLengthAttribute { get => _additionalLengthAttribute ?? string.Empty; set => _additionalLengthAttribute = value; }
        public bool UseAdditionalLength { get; set; }
        #endregion

        #region Потоки: атрибуты
        public string GraphAttribute { get => _graphAttribute ?? string.Empty; set => _graphAttribute = value; }
        public string SignalAttribute { get => _signalAttribute ?? string.Empty; set => _signalAttribute = value; }
        public string StreamAttribute { get => _streamAttribute ?? string.Empty; set => _streamAttribute = value; }
        public string GidAttribute { get => _gidAttribute ?? string.Empty; set => _gidAttribute = value; }
        #endregion

        #region Потоки: графика
        public string NoteSymbol { get => _noteSymbol ?? string.Empty; set => _noteSymbol = value; }
        public string StreamNoteTemplate { get => _streamNoteTemplate ?? string.Empty; set => _streamNoteTemplate = value; }
        public int StreamTypeWidth { get; set; }
        public int StreamCableWidth { get; set; }
        public int StreamCableHeight { get; set; }
        public string StreamOldColor { get; set; }
        public string StreamNewColor { get; set; }
        public bool StreamAddLine { get; set; }
        public bool Fill { get; set; }
        public bool ReadTubes { get; set; }
        public string TreeName { get => _treeName ?? string.Empty; set => _treeName = value; }
        #endregion

        #region Разрезы траншей

        private ObservableCollection<CableType> _cableTypes;
        public ObservableCollection<CableType> CableTypes
        {
            get
            {
                {
                    if (_cableTypes == null)
                        _cableTypes = new ObservableCollection<CableType>();
                    return _cableTypes;
                }
            }
            set
            {
                if (_cableTypes != value)
                {
                    _cableTypes = new ObservableCollection<CableType>(value.Where(w=>!string.IsNullOrEmpty(w.Name)));
                }
            }
        }

        private ObservableCollection<TrenchType> _trenchTypes;
        public ObservableCollection<TrenchType> TrenchTypes
        {
            get
            {
                {
                    if (_trenchTypes == null)
                        _trenchTypes = new ObservableCollection<TrenchType>();
                    return _trenchTypes;
                }
            }
            set
            {
                if (_trenchTypes != value)
                {
                    _trenchTypes = value;
                }
            }
        }

        private ObservableCollection<BetweenType> _betweenTypes;
        public ObservableCollection<BetweenType> BetweenTypes
        {
            get
            {
                {
                    if (_betweenTypes == null)
                        _betweenTypes = new ObservableCollection<BetweenType>();
                    return _betweenTypes;
                }
            }
            set
            {
                if (_betweenTypes != value)
                {
                    _betweenTypes = value;
                }
            }
        }


        public string TrenchSectionSymbolHor { get => _trenchSectionSymbolHor ?? string.Empty; set => _trenchSectionSymbolHor = value; }
        public string TrenchSectionSymbolVert { get => _trenchSectionSymbolVert ?? string.Empty; set => _trenchSectionSymbolVert = value; }
        public string TrenchSectionAttribute { get => _trenchSectionAttribute ?? string.Empty; set => _trenchSectionAttribute = value; }
        public string TrenchParametersAttribute { get => _trenchParametersAttribute ?? string.Empty; set => _trenchParametersAttribute = value; }
        public string TrenchCableDepthAttribute { get => _trenchCableDepthAttribute ?? string.Empty; set => _trenchCableDepthAttribute = value; }
        public string TrenchMaterialAttribute { get => _trenchMaterialAttribute ?? string.Empty; set => _trenchMaterialAttribute = value; }

        public int TrenchSectionTextNumber { get; set; }
        public int TrenchStreamY { get; set; }
        public double TrenchScale { get; set; }
        public bool TrenchAlphabeticNumerate { get; set; }

        #endregion

        #region Разрезы металлоконструкций

        private ObservableCollection<ETypeMetalconstruction> _metalconstructionTypes;
        public ObservableCollection<ETypeMetalconstruction> MetalconstructionTypes
        {
            get
            {
                {
                    if (_metalconstructionTypes == null)
                        _metalconstructionTypes = new ObservableCollection<ETypeMetalconstruction>();
                    return _metalconstructionTypes;
                }
            }
            set
            {
                if (_metalconstructionTypes != value)
                {
                    _metalconstructionTypes = value;
                }
            }
        }

        private ObservableCollection<ETypeStand> _standTypes;
        public ObservableCollection<ETypeStand> StandTypes
        {
            get
            {
                {
                    if (_standTypes == null)
                        _standTypes = new ObservableCollection<ETypeStand>();
                    return _standTypes;
                }
            }
            set
            {
                if (_standTypes != value)
                {
                    _standTypes = value;
                }
            }
        }

        private ObservableCollection<ETypeShelf> _shelfTypes;
        public ObservableCollection<ETypeShelf> ShelfTypes
        {
            get
            {
                {
                    if (_shelfTypes == null)
                        _shelfTypes = new ObservableCollection<ETypeShelf>();
                    return _shelfTypes;
                }
            }
            set
            {
                if (_shelfTypes != value)
                {
                    _shelfTypes = value;
                }
            }
        }

        private ObservableCollection<ETypeDuct> _ductTypes;
        public ObservableCollection<ETypeDuct> DuctTypes
        {
            get
            {
                {
                    if (_ductTypes == null)
                        _ductTypes = new ObservableCollection<ETypeDuct>();
                    return _ductTypes;
                }
            }
            set
            {
                if (_ductTypes != value)
                {
                    _ductTypes = value;
                }
            }
        }




        public string DuctSectionSymbolHor { get => _ductSectionSymbolHor ?? string.Empty; set => _ductSectionSymbolHor = value; }
        public string DuctSectionSymbolVert { get => _ductSectionSymbolVert ?? string.Empty; set => _ductSectionSymbolVert = value; }
        public string DuctSectionNumberAttribute { get => _ductSectionNumberAttribute ?? string.Empty; set => _ductSectionNumberAttribute = value; }
        public string MetalconstructionParametersAttribute { get => _metalconstructionParametersAttribute ?? string.Empty; set => _metalconstructionParametersAttribute = value; }
        public string DuctParametersAttribute { get => _ductParametersAttribute ?? string.Empty; set => _ductParametersAttribute = value; }
        public int DuctSectionNumberTextNumber { get; set; }
        public int FullPercent { get; set; }

        public bool MetalAlphabeticNumerate { get; set; }
        #endregion

        #region Расчет материалов

        public string MetalCalcDeviceQuantityAttribute { get; set; }

        private string _metalCalcPalceMode;
        private string _calcMetalSaveAttribute;

        public string MetalCalcPlaceMode { get { return string.IsNullOrEmpty(_metalCalcPalceMode) ? "Интерактивно" : _metalCalcPalceMode; } set { _metalCalcPalceMode = value; } }
        public int NonInteractivelyX { get; set; }
        public int NonInteractivelyY { get; set; }
        public int NonInteractivelyDeltaX { get; set; }
        public int NonInteractivelyDeltaY { get; set; }
        public int MetalCalcSymbolLevel { get; set; }
        public double CalcMinLength { get; set; }
        public string CalcMetalSaveAttribute { get => _calcMetalSaveAttribute ?? string.Empty; set => _calcMetalSaveAttribute = value; }
        #endregion

        #region Подложка
        public string DWGFileAttribute { get => _dWGFileAttribute ?? string.Empty; set => _dWGFileAttribute = value; }
        public bool AddDWGToProject { get; set; }
        public bool DwgCreateConnectLine { get; set; }
        public int DwgConnectLinesLevelNumber { get; set; }
        #endregion




    }
}