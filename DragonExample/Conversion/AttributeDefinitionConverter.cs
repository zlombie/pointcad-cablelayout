﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

using DragonExample.Database;


namespace DragonExample.Conversion
{
    internal abstract class AttributeDefinitionConverter : MarkupExtension, IValueConverter
	{
		#region Properties

		protected virtual List<AttributeDefinition> AttributeDefinitions => new List<AttributeDefinition>();

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string name = (string) value;
			AttributeDefinition definition = AttributeDefinitions.Find(x => string.Equals(x.InternalName, name)) ??
											 AttributeDefinitions.Find(x => string.Equals(x.Description, name));
			return definition;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var definition = value as AttributeDefinition;
			if (definition == null)
			{
				return null;
			}
            string name = definition.InternalName;
            if (string.IsNullOrEmpty(name))
            {
                name = definition.Description;
            }
            return name;
		}

		#endregion
	}
}