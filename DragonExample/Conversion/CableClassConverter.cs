﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;


namespace DragonExample.Conversion
{
    internal abstract class CableClassConverter : MarkupExtension, IValueConverter
    {
        #region Properties

        protected virtual List<CableType> CableTypes => new List<CableType>();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string name = (string)value;
            var definition = CableTypes.Find(x => string.Equals(x.Name, name));
            return definition;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var definition = value as CableType;
            if (definition == null)
            {
                return null;
            }
            string name = definition.Name;
            return name;
        }

        #endregion
    }
}