﻿using System;
using System.Collections.Generic;
using System.Linq;
using DragonExample.Database;
using System.Windows.Markup;
using System.Windows.Data;
using System.Globalization;

namespace DragonExample.Conversion
{
    internal class ComponentConverter : MarkupExtension, IValueConverter
    {
        #region Fields

        private static readonly ComponentConverter Converter = new ComponentConverter();

        #endregion

        #region Properties

        protected List<ComponentDefinition> Components =>
            DatabaseSingleton.Instance.Components.ToList();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string name = (string)value;
            ComponentDefinition definition = Components.Find(x => string.Equals(x.Name, name));
            return definition;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var definition = value as ComponentDefinition;
            if (definition == null)
            {
                return null;
            }
            return definition.Name;
        }

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }
}
