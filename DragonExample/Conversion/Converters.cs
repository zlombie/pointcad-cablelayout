﻿using System;
using System.Collections.Generic;
using System.Linq;
using DragonExample.Database;
using System.Collections.ObjectModel;

namespace DragonExample.Conversion
{
    internal class DeviceAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly DeviceAttributeDefinitionConverter Converter = new DeviceAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.DeviceAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class ComponentAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly ComponentAttributeDefinitionConverter Converter = new ComponentAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.ComponentAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class NetSegmentAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly NetSegmentAttributeDefinitionConverter Converter = new NetSegmentAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.NetSegmentAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion

    }

    internal class CableAttributeDefinitionConverter : AttributeDefinitionConverter
        {
            #region Fields

            private static readonly CableAttributeDefinitionConverter Converter = new CableAttributeDefinitionConverter();

            #endregion

            #region Properties

            protected override List<AttributeDefinition> AttributeDefinitions =>
                DatabaseSingleton.Instance.CableAttributeDefinitions;

            #endregion

            #region Methods

            public override object ProvideValue(IServiceProvider serviceProvider)
            {
                return Converter;
            }

            #endregion
        }

    internal class SymbolAttributeDefinitionConverter : AttributeDefinitionConverter
        {
            #region Fields

            private static readonly SymbolAttributeDefinitionConverter Converter = new SymbolAttributeDefinitionConverter();

            #endregion

            #region Properties

            protected override List<AttributeDefinition> AttributeDefinitions =>
                DatabaseSingleton.Instance.SymbolAttributeDefinitions;

            #endregion

            #region Methods

            public override object ProvideValue(IServiceProvider serviceProvider)
            {
                return Converter;
            }

            #endregion
        }

    internal class SheetFieldAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly SheetFieldAttributeDefinitionConverter Converter = new SheetFieldAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.SheetFieldAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class GroupAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly GroupAttributeDefinitionConverter Converter = new GroupAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.GroupAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class SymGraphTextGroupAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly SymGraphTextGroupAttributeDefinitionConverter Converter = new SymGraphTextGroupAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.SymGraphTextGroupAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class ProjectAttributeDefinitionConverter : AttributeDefinitionConverter
    {
        #region Fields

        private static readonly ProjectAttributeDefinitionConverter Converter = new ProjectAttributeDefinitionConverter();

        #endregion

        #region Properties

        protected override List<AttributeDefinition> AttributeDefinitions =>
            DatabaseSingleton.Instance.ProjectAttributeDefinitions;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class TemplateSymbolDefinitionConverter : SymbolDefinitionConverter
    {
        #region Fields

        private static readonly TemplateSymbolDefinitionConverter Converter = new TemplateSymbolDefinitionConverter();

        #endregion

        #region Properties

        protected override List<SymbolDefinition> SymbolDefinitions =>
            DatabaseSingleton.Instance.TemplateSymbolNames;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    internal class StandartSymbolDefinitionConverter : SymbolDefinitionConverter
    {
        #region Fields

        private static readonly StandartSymbolDefinitionConverter Converter = new StandartSymbolDefinitionConverter();

        #endregion

        #region Properties

        protected override List<SymbolDefinition> SymbolDefinitions =>
            DatabaseSingleton.Instance.StandartSymbolNames;

        #endregion

        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Converter;
        }

        #endregion
    }

    //internal class CableTypeConverter : CableClassConverter
    //{
    //    #region Fields

    //    private static readonly CableTypeConverter Converter = new CableTypeConverter();

    //    #endregion

    //    #region Properties

    //    protected override List<CableType> CableTypes =>
    //        DatabaseSingleton.Instance.CableTypes.ToList();

    //    #endregion

    //    #region Methods

    //    public override object ProvideValue(IServiceProvider serviceProvider)
    //    {
    //        return Converter;
    //    }

    //    #endregion
    //}
}
