﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

using DragonExample.Database;


namespace DragonExample.Conversion
{
    internal abstract class SymbolDefinitionConverter : MarkupExtension, IValueConverter
    {
        #region Properties

        protected virtual List<SymbolDefinition> SymbolDefinitions => new List<SymbolDefinition>();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string name = value as string;
            SymbolDefinition definition = SymbolDefinitions.Find(x => string.Equals(x.Name, name));
            return definition;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var definition = value as SymbolDefinition;
            if (definition == null)
            {
                return null;
            }
            string name = definition.Name;
            return name;
        }

        #endregion
    }
}