﻿using System.Collections.Generic;


namespace DragonExample.Service
{
	public class AlphanumericStringComparer : IComparer<string>
	{
		public int Compare(string x, string y)
		{
			return AlphanumericComparer.Compare(x, y);
		}
	}
}