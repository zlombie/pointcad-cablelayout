﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Service
{
    static class eConstants
    {
        public const string NullString = "<нет данных>";
        public const int BadHeight = 123456789;


        public const string Developer = "Pointcad";
        public const string Product = "Instrumentation";
        public const string Version = "4";
        public const string SettingsFileRegKey = "FileSettings";

        public static string DefaultProfilePath
        {
            get
            {
                string application = Assembly.GetEntryAssembly().GetName().Name;
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                           $"{Developer}",
                           $"{Product}",
                           $"{application}",
                           "Settings.Layout-Profile");
            }
        }


        public static List<string> PlaceModes { get; } = new List<string> { "Интерактивно", "По заданным координатам" };
        public static List<string> RatioUnits { get; } = new List<string> { "метр", "участок трассы" };
        public static List<string> RoundNum { get; } = new List<string> { "Не округлять", "До целого", "1 знак после запятой", "2 знака после запятой", "3 знака после запятой" };
        public static List<string> RoundType { get; } = new List<string> { "Математически", "В большую сторону", "В меньшую сторону" };
    }
}
