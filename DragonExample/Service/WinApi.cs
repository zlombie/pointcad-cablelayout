﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Service
{
    class WinApi
    {
        public enum GetWindowCmd : uint
        {
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("ole32.dll")]
#pragma warning disable CS0618 // 'UCOMIRunningObjectTable" является устаревшим: 'Use System.Runtime.InteropServices.ComTypes.IRunningObjectTable instead. http://go.microsoft.com/fwlink/?linkid=14202'
        public static extern int GetRunningObjectTable(uint reserved, out UCOMIRunningObjectTable pprot);
#pragma warning restore CS0618 // 'UCOMIRunningObjectTable" является устаревшим: 'Use System.Runtime.InteropServices.ComTypes.IRunningObjectTable instead. http://go.microsoft.com/fwlink/?linkid=14202'

        [DllImport("ole32.dll")]
#pragma warning disable CS0618 // 'UCOMIBindCtx" является устаревшим: 'Use System.Runtime.InteropServices.ComTypes.IBindCtx instead. http://go.microsoft.com/fwlink/?linkid=14202'
        public static extern int CreateBindCtx(int reserved, out UCOMIBindCtx ppbc);
#pragma warning restore CS0618 // 'UCOMIBindCtx" является устаревшим: 'Use System.Runtime.InteropServices.ComTypes.IBindCtx instead. http://go.microsoft.com/fwlink/?linkid=14202'

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr WindowFromPoint(System.Drawing.Point p);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        public static string EnumClassName(IntPtr hwnd)
        {
            if ((!hwnd.Equals(IntPtr.Zero)))
            {
                var ClassName = new StringBuilder(255);
                var NumberOfCharacters = GetClassName(hwnd, ClassName, ClassName.Capacity);
                if (NumberOfCharacters > 0)
                    return ClassName.ToString();
                else
                    return "1";
            }
            else
                return "1";
        }
    }
}
