﻿namespace DragonExample.Database
{
    public class ComponentDefinition
    {
        #region Constructors

        public ComponentDefinition(string name, string version)
        {
            Name = name;
            Version = version;
        }

        #endregion

        #region Properties

        public string Name { get; private set; }
        public string Version { get; private set; }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}