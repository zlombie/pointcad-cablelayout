﻿using System;


namespace DragonExample.Database
{
	[Flags]
	public enum AttributeOwner
	{
		None = 0,
		Symbol = 1,
		Cable = 1 << 1,
		Component = 1 << 5,
		Device = 1 << 11,
		Sheet = 1 << 25,
        Field = 1 << 31,
        NetSegment = 1 << 24,
        Project = 1 << 14
    }

    [Flags]
    public enum AttributeOwner2
    {
        None = 0,
        Node = 1,
        Group = 1 << 10,
        Graphic = 1 << 15,
        Text = 1 << 16,
    }
}