﻿using System.Collections.Generic;

namespace DragonExample.Database
{
    public class SymbolDefinition
    {
		#region Constructors

		public SymbolDefinition(string name, int type, List<int> schemes)
		{
			Name = name;
            SymbolType = type;
            SchemeTypes = schemes ?? new List<int>();
        }

		#endregion

		#region Properties

		public string Name {	get; private set; }
        public int SymbolType { get; private set; }
        public List<int> SchemeTypes { get; private set; }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}