﻿using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Collections.ObjectModel;
using Layout;
using System;
using System.ComponentModel;
using Layout.Configuration;

namespace DragonExample.Database
{
    internal class MagicAttribute : Attribute { }

    [Magic]
    public sealed class DatabaseSingleton : INotifyPropertyChanged
    {
		#region Fields

		private static readonly object SyncRoot = new object();
		private static DatabaseSingleton _instance;

        #endregion

        #region Properties

        public static DatabaseSingleton Instance
		{
			get
			{
				if (_instance == null)
				{
					Monitor.Enter(SyncRoot);
					var temp = new DatabaseSingleton();
					Interlocked.Exchange(ref _instance, temp);
					Monitor.Exit(SyncRoot);
					return _instance;
				}
				return _instance;
			}
		}

        public List<SymbolDefinition> AllSymbols { get; set; }
        public List<AttributeDefinition> AllAttributes { get; set; }
        public List<AttributeDefinition> ComponentAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Component)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> DeviceAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Device)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> CableAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Cable)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> SymbolAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Symbol)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> NetSegmentAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.NetSegment)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> GroupAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner2.HasFlag(AttributeOwner2.Group)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> ProjectAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Project)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> SymGraphTextGroupAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Symbol) && f.Owner2.HasFlag(AttributeOwner2.Group) && f.Owner2.HasFlag(AttributeOwner2.Text) && f.Owner2.HasFlag(AttributeOwner2.Graphic)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> NetSegmentMultiplyAttributeDefinitions { get { return NetSegmentAttributeDefinitions.FindAll(f => f.IsSingleInstance == false) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> SheetFieldAttributeDefinitions { get { return AllAttributes.FindAll(f => f.Owner.HasFlag(AttributeOwner.Sheet) || f.Owner.HasFlag(AttributeOwner.Field)) ?? new List<AttributeDefinition>(); } }
        public List<AttributeDefinition> NetsegmentSymbolAttributeDefinitions { get { return SymbolAttributeDefinitions.Intersect(NetSegmentAttributeDefinitions).ToList(); } }   
        public List<AttributeDefinition> NetsegmentSymbolMultiplyAttributedefinitions { get { return SymbolAttributeDefinitions.Intersect(NetSegmentMultiplyAttributeDefinitions).ToList(); } }
        public List<SymbolDefinition> TemplateSymbolNames { get { return AllSymbols.FindAll(f => f.SchemeTypes.Contains(eSettings.Profile.SchemeNumber) && f.SymbolType == 102); } }
        public List<SymbolDefinition> StandartSymbolNames { get { return AllSymbols.FindAll(f => f.SchemeTypes.Contains(eSettings.Profile.SchemeNumber) && f.SymbolType == 2); } }
        public List<ComponentDefinition> Components { get; set; }
        public List<string> DrawingEntries { get; set; }
        public List<string> SectionTypes { get; set; } = new List<string> { "<Нет данных>", "Траншея", "Металлоконструкции" };

        void RaisePropertyChanged(string propName)
        {
            var e = PropertyChanged;
            if (e != null)
                e(this, new PropertyChangedEventArgs(propName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}