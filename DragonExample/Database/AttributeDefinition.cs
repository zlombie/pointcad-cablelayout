﻿namespace DragonExample.Database
{
	public class AttributeDefinition
	{
		#region Constructors

		public AttributeDefinition(string internalName, string description, AttributeOwner owner, AttributeOwner2 owner2, bool? isSingleInstance)
		{
			InternalName = internalName;
			Description = string.IsNullOrEmpty(description) ? internalName : description;
            Owner2 = owner2;
            Owner = owner;
            IsSingleInstance = isSingleInstance ?? false;
		}

		#endregion

		#region Properties

		public string Description {	get; private set; }
		public string InternalName { get; private set; }
		public AttributeOwner Owner	{get; private set;}
        public AttributeOwner2 Owner2 {get; private set; }
        public bool IsSingleInstance { get; private set; }


        public override string ToString()
        {
            return InternalName;
        }
        #endregion
    }
}