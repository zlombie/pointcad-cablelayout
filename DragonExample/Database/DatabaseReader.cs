﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

using DragonExample.Service;

//using e3;

using Pointcad.Log;


#pragma warning disable 618

namespace DragonExample.Database
{
	internal class DatabaseReader
	{
		#region Fields

		private static readonly AlphanumericStringComparer AlphanumericStringComparer = new AlphanumericStringComparer();

		private static readonly Dictionary<int, string> CountryTelephoneCodes = new Dictionary<int, string>
																				{
																					{49, "German"},
																					{44, "English"},
																					{33, "French"},
																					{39, "Italian"},
																					{34, "Spanish"},
																					{31, "Dutch"},
																					{351, "Portuguese"},
																					{86, "Chinese"},
																					{81, "Japanese"},
																					{7, "Russian"},
																					{90, "Turkish"}
																				};

		#endregion

		#region Methods

		private static DbCommand GetCommand(DbConnection connection, string query)
		{
			DbCommand command;
			var sqlConnection = connection as SqlConnection;
			if (sqlConnection != null)
			{
				command = new SqlCommand(query, sqlConnection);
			}
			else
			{
				var oracleConnection = connection as OracleConnection;
				if (oracleConnection != null)
				{
					command = new OracleCommand(query, oracleConnection);
				}
				else
				{
					command = new OleDbCommand(query, (OleDbConnection) connection);
				}
			}
			return command;
		}

		[DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool IsWow64Process([In] IntPtr process, [Out] out bool wow64Process);

		private static void ReadAttributeData(DbConnection connection,
											  string separator,
											  string schemaSeparator,
											  string configurationDatabaseTableSchema,
											  int languageCode)
		{
			string language = CountryTelephoneCodes.ContainsKey(languageCode)
								  ? CountryTelephoneCodes[languageCode]
								  : CountryTelephoneCodes[44];

			string queryColumn = string.Format("SELECT {0}{3}{0}.{0}ColumnName{0}, {0}{3}{0}.{0}{4}{0} " +
											   "FROM {0}{1}{0}{2}{0}{3}{0} " +
											   "WHERE {0}{3}{0}.{0}TableName{0}='ComponentData'",
											   separator,
											   configurationDatabaseTableSchema,
											   schemaSeparator,
											   "ColumnDescription",
											   language);

			var definitions = new List<AttributeDefinition>();

			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
			}

			DbCommand commandColumn = GetCommand(connection, queryColumn);
			DbDataReader reader = null;
			try
			{
				reader = commandColumn.ExecuteReader();
				while (reader.Read())
				{
					string internalName = reader[0] as string;
					string description = reader[1] as string;

					if (!string.IsNullOrEmpty(internalName))
					{
						var definition = new AttributeDefinition(internalName, description, AttributeOwner.Component, AttributeOwner2.None, false);
						definitions.Add(definition);
					}
				}
			}
			catch (Exception exception)
			{
				LogHelper.Logger.Warn(exception);
			}
			finally
			{
				reader?.Close();
			}

			if (languageCode == 1)
			{
				languageCode = 44;
			}

			string descriptionFieldName = "DESCRIPTION_" + languageCode.ToString().PadLeft(2, '0');

			string queryAttribute = string.Format("SELECT {0}{3}{0}.{0}AttributeName{0}, " +
												  "{0}{3}{0}.{0}Owner{0}, " +
                                                  "{0}{3}{0}.{0}Owner2{0}, " +
                                                  "{0}{3}{0}.{0}{4}{0}, " +
                                                  "{0}{3}{0}.{0}SingleInstance{0} " +
                                                  "FROM {0}{1}{0}{2}{0}{3}{0}",
												  separator,
												  configurationDatabaseTableSchema,
												  schemaSeparator,
												  "AttributeDefinition",
												  descriptionFieldName);

			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
			}

			var mask = AttributeOwner.None;
            var mask2 = AttributeOwner2.None;
            foreach (AttributeOwner value in Enum.GetValues(typeof(AttributeOwner)))
			{
				mask |= value;
			}

            foreach (AttributeOwner2 value in Enum.GetValues(typeof(AttributeOwner2)))
            {
                mask2 |= value;
            }

            DbCommand commandAttribute = GetCommand(connection, queryAttribute);
			reader = null;
			try
			{
				reader = commandAttribute.ExecuteReader();
				while (reader.Read())
				{
					string internalName = reader[0] as string;

					if (string.IsNullOrEmpty(internalName))
					{
						continue;
					}

					string description = reader[3] as string;

					// Системные атрибуты с пустым полем Description не обрабатываем.
					if (internalName.StartsWith(".") && string.IsNullOrEmpty(description))
					{
						continue;
					}

					AttributeOwner owner;
                    AttributeOwner2 owner2;

                    object data = reader[1];
					if (data is DBNull)
					{
						owner = AttributeOwner.None;
					}
					else
					{
						owner = (AttributeOwner) (Convert.ToInt64(data) & (int) mask);
					}

                    data = reader[2];
                    if (data is DBNull)
                    {
                        owner2 = AttributeOwner2.None;
                    }
                    else
                    {
                        owner2 = (AttributeOwner2)(Convert.ToInt64(data) & (int)mask2);
                    }

                    bool? r = reader[4] as bool?;
                                        
                    var definition = new AttributeDefinition(internalName, description, owner, owner2, r);
					definitions.Add(definition);
				}
			}
			catch (Exception exception)
			{
				LogHelper.Logger.Warn(exception);
			}
			finally
			{
				reader.Close();
			}

			definitions.Sort((one, two) =>
							 {
								 int result = AlphanumericStringComparer.Compare(one.Description, two.Description);
								 if (result == 0)
								 {
									 result = AlphanumericStringComparer.Compare(one.InternalName, two.InternalName);
								 }
								 return result;
							 });


            DatabaseSingleton singleton = DatabaseSingleton.Instance;
            singleton.AllAttributes = definitions ?? new List<AttributeDefinition>();
        }

		private static void ReadComponentData(DbConnection connection,
											  string separator,
											  string schemaSeparator,
											  string componentDatabaseTableSchema)
		{
			string query = string.Format("SELECT {0}{3}{0}.{0}ENTRY{0}, {0}{3}{0}.{0}VERSION{0} " +
										 "FROM {0}{1}{0}{2}{0}{3}{0} " +
										 "WHERE {0}{3}{0}.{0}ENTRYTYP{0} IS NULL",
										 separator,
										 componentDatabaseTableSchema,
										 schemaSeparator,
										 "ComponentData");

			var components = new List<ComponentDefinition>();

			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
			}

			DbCommand commandColumn = GetCommand(connection, query);
			DbDataReader reader = null;
			try
			{
				reader = commandColumn.ExecuteReader();
				while (reader.Read())
				{
					string entry = reader[0] as string;
                    string version = reader[1] as string;
					if (!string.IsNullOrEmpty(entry))
					{
                        components.Add(new ComponentDefinition(entry, version));
					}
				}
			}
			catch (Exception exception)
			{
				LogHelper.Logger.Warn(exception);
			}
			finally
			{
				reader?.Close();
			}
            components.Add(new ComponentDefinition("<Нет данных>", "0"));
            components = components.OrderBy(o => o.Name).ToList();

			DatabaseSingleton.Instance.Components = components;
		}

		public static void ReadData(dynamic eApp)
		{
			if (eApp == null)
			{
				return;
			}

			string configurationConnectionString = eApp.GetConfigurationDatabase();
			string configurationTableSchema = eApp.GetConfigurationDatabaseTableSchema();

			string symbolConnectionString = eApp.GetSymbolDatabase();
			string symbolTableSchema = eApp.GetSymbolDatabaseTableSchema();

			string componentConnectionString = eApp.GetComponentDatabase();
			string componentTableSchema = eApp.GetComponentDatabaseTableSchema();

			int languageCode = eApp.GetInstallationLanguage();

			bool is64BitProcess;
			if (!Environment.Is64BitOperatingSystem)
			{
				is64BitProcess = false;
			}
			else
			{
				using (Process process = Process.GetProcessById(eApp.GetId()))
				{
					bool result;
					is64BitProcess = IsWow64Process(process.Handle, out result) && !result;
				}
			}

			ReadData(configurationConnectionString,
					 configurationTableSchema,
					 symbolConnectionString,
					 symbolTableSchema,
					 componentConnectionString,
					 componentTableSchema,
					 languageCode,
					 is64BitProcess);
		}

		public static void ReadData(string configurationConnectionString,
									string configurationTableSchema,
									string symbolConnectionString,
									string symbolTableSchema,
									string componentConnectionString,
									string componentTableSchema,
									int languageCode,
									bool is64BitProcess)
		{
			if (languageCode <= 0)
			{
				languageCode = 44;
			}

			string separator = string.Empty;
			string schemaSeparator;

			var oleDbBuilder = new OleDbConnectionStringBuilder(configurationConnectionString);

			string provider = oleDbBuilder.Provider;
			if (string.IsNullOrEmpty(provider))
			{
				return;
			}

			DbConnection configurationConnection;
			DbConnection symbolConnection;
			DbConnection componentConnection;

			string providerLower = provider.ToLower();

			if (providerLower.Contains("sql"))
			{
				string connectionString = configurationConnectionString;

				var providerRegex = new Regex("Provider=[\\.\\w]+;");
				connectionString = providerRegex.Replace(connectionString, string.Empty);

				var connection = new SqlConnection(connectionString);

				configurationConnection = connection;
				symbolConnection = connection;
				componentConnection = connection;
			}
			else if (providerLower.Contains("oracle"))
			{
				separator = "\"";

				string connectionString = configurationConnectionString;

				var providerRegex = new Regex("Provider=[\\.\\w]+;");
				connectionString = providerRegex.Replace(connectionString, string.Empty);

				var connection = new OracleConnection(connectionString);

				configurationConnection = connection;
				symbolConnection = connection;
				componentConnection = connection;
			}
			else
			{
				string oleDbProvider = null;
				if (Environment.Is64BitProcess == is64BitProcess)
				{
					oleDbProvider = provider;
				}
				else
				{
					var systemProviders = new List<string>();
					OleDbDataReader reader = OleDbEnumerator.GetRootEnumerator();
					while (reader.Read())
					{
						object data = reader[0];
						if (data is DBNull)
						{
							continue;
						}
						string p = data.ToString();
						if (!string.IsNullOrEmpty(p))
						{
							systemProviders.Add(p);
						}
					}

					string[] defaultProviders;
					if (Environment.Is64BitProcess)
					{
						defaultProviders = new[]
										   {
											   "Microsoft.ACE.OLEDB.12.0",
											   "Microsoft.Jet.OLEDB.4.0"
										   };
					}
					else
					{
						defaultProviders = new[]
										   {
											   "Microsoft.Jet.OLEDB.4.0",
											   "Microsoft.ACE.OLEDB.12.0"
										   };
					}

					var providers = new List<string>(defaultProviders);
					foreach (string p in systemProviders)
					{
						if (!providers.Contains(p))
						{
							providers.Add(p);
						}
					}
					if (!providers.Contains(provider))
					{
						providers.Add(provider);
					}

					foreach (string p in providers)
					{
						oleDbBuilder.Provider = p;

						var connection = new OleDbConnection(oleDbBuilder.ConnectionString);
						try
						{
							connection.Open();
							oleDbProvider = p;
							break;
						}
						catch (Exception exception)
						{
							LogHelper.Logger.Warn(exception);
						}
						finally
						{
							connection.Close();
						}
					}
				}

				if (string.IsNullOrEmpty(oleDbProvider))
				{
					return;
				}

				var configurationBuilder = new OleDbConnectionStringBuilder(configurationConnectionString)
										   {
											   Provider = oleDbProvider
										   };

				if (Equals(configurationConnectionString, symbolConnectionString) &&
					Equals(configurationConnectionString, componentConnectionString))
				{
					var connection = new OleDbConnection(configurationBuilder.ConnectionString);

					configurationConnection = connection;
					symbolConnection = connection;
					componentConnection = connection;
				}
				else
				{
					var symbolBuilder = new OleDbConnectionStringBuilder(symbolConnectionString)
										{
											Provider = oleDbProvider
										};

					var componentBuilder = new OleDbConnectionStringBuilder(componentConnectionString)
										   {
											   Provider = oleDbProvider
										   };

					configurationConnection = new OleDbConnection(configurationBuilder.ConnectionString);
					symbolConnection = new OleDbConnection(symbolBuilder.ConnectionString);
					componentConnection = new OleDbConnection(componentBuilder.ConnectionString);
				}
			}

			if (!string.IsNullOrEmpty(configurationTableSchema) ||
				!string.IsNullOrEmpty(symbolTableSchema))
			{
				schemaSeparator = ".";
			}
			else
			{
				schemaSeparator = string.Empty;
			}

			ReadSymbolData(symbolConnection,
						   separator,
						   schemaSeparator,
						   symbolTableSchema);

			ReadAttributeData(configurationConnection,
							  separator,
							  schemaSeparator,
							  configurationTableSchema,
							  languageCode);

			ReadComponentData(componentConnection,
							  separator,
							  schemaSeparator,
							  componentTableSchema);

			if (configurationConnection.State == ConnectionState.Open)
			{
				configurationConnection.Close();
			}

			if (symbolConnection.State == ConnectionState.Open)
			{
				symbolConnection.Close();
			}

			if (componentConnection.State == ConnectionState.Open)
			{
				componentConnection.Close();
			}
		}

		private static void ReadSymbolData(DbConnection connection,
										   string separator,
										   string schemaSeparator,
										   string symbolDatabaseTableSchema)
		{
			string query = string.Format("SELECT {0}{3}{0}.{0}ENTRY{0}, {0}{3}{0}.{0}LSHTYP{0}, {0}SymbolAttribute{0}.{0}AttributeValue{0} " +
                                         "FROM {0}{1}{0}{2}{0}{3}{0} INNER JOIN {0}{1}{0}{2}{0}SymbolAttribute{0} ON {0}{3}{0}.{0}ENTRY{0} = {0}SymbolAttribute{0}.{0}ENTRY{0} " +
                                         "WHERE {0}{3}{0}.{0}VSTATUS{0}='C' AND {0}SymbolAttribute{0}.{0}AttributeName{0}='.SCHEMATIC_TYPE'",
										 separator,
										 symbolDatabaseTableSchema,
										 schemaSeparator,
										 "SymbolData");


            //            SELECT SymbolData.ENTRY, SymbolAttribute.AttributeValue
            //FROM            SymbolData INNER JOIN
            //                  SymbolAttribute ON SymbolData.ENTRY = SymbolAttribute.ENTRY
            //WHERE(SymbolAttribute.AttributeName = '.SCHEMATIC_TYPE') AND(SymbolAttribute.AttributeValue = 101)
            //ORDER BY SymbolAttribute.AttributeName

            var symbolNames = new List<SymbolDefinition>();

			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
			}

			DbCommand command = GetCommand(connection, query);
			DbDataReader reader = null;
			try
			{
				reader = command.ExecuteReader();
				while (reader.Read())
				{
					object data = reader[0];
					string entry = data is DBNull ? string.Empty : (string) data;

					if (!string.IsNullOrEmpty(entry))
					{
						int typeId = Convert.ToInt32(reader[1]);
                        string r2 = reader[2].ToString();
                        int.TryParse(r2, out int schemeType);
                        var fnd = symbolNames.Find(f => f.Name == entry);
                        if (fnd==null)
                            symbolNames.Add(new SymbolDefinition(entry, typeId, new List<int> { schemeType }));
                        else
                            fnd.SchemeTypes.Add(schemeType);
					}
				}
			}
			catch (Exception exception)
			{
				LogHelper.Logger.Warn(exception);
			}
			finally
			{
				reader?.Close();
			}

            if (symbolNames.Count > 0)
            {
                DatabaseSingleton.Instance.AllSymbols = symbolNames.OrderBy(o => o.Name).ToList();
            }
        }

		#endregion
	}
}