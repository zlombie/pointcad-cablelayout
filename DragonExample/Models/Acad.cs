﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using Pointcad.Dragon.Windows;
using Layout.Configuration;

namespace Layout
{

    class Acad
    {

        #region Загрузка из автокад

        /// <summary>
        /// Загрузка подложки из DWG
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sheetID"></param>
        /// <param name="externalDoc"></param>
        /// <param name="scale"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void LoadDWG(string filename, int sheetID, string externalDoc, double scale = 1, double x = 0, double y = 0)
        {
            bool newSheetCreated = false;
            if (sheetID == 0 && string.IsNullOrEmpty(externalDoc))
            {
                e3c.InitE3Objects();
                int currentSheetID = e3c.job.GetActiveSheetId();
                if (currentSheetID==0)
                {
                    MessageWindow.ShowDialog("Для импорта подложки должен быть открыт лист E3.series", "" , "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                    e3c.DisposeE3Job();
                    return;
                }
                if (eSettings.Profile.AddDWGToProject)
                {
                    int extID = e3c.ext.InsertFile(0, Path.GetFileName(filename), filename);
                    if (extID > 0)
                    {
                        e3c.app.PutInfo(0, string.Format("Файл подложки {0} импортирован в проект", filename));
                        externalDoc = extID.ToString();
                    }
                    else
                    {
                        MessageWindow.ShowDialog("Не удалось импортировать файл подложки в проект","" ,"Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                        e3c.DisposeE3Job();
                        return;
                    }
                }
                else
                {
                    externalDoc = filename;
                }
                e3c.sheet.SetId(currentSheetID);
                string sheetName = e3c.sheet.GetFormat();
                sheetID = e3c.sheet.Create(0, "temp", sheetName, currentSheetID, 0);
                newSheetCreated = true;
            }
            else
            {
                e3c.sheet.SetId(sheetID);
            }
            e3c.job.SetSettingValue("USE_READ_ONLY_GRAPHIC_LEVEL", false);
            
            string fontName = e3c.job.GetGraphTextFontName();
            List<int> oldIds = new List<int>();
            List<int> oldGroups = new List<int>();
            List<int> newGroups = new List<int>();
            dynamic ids = null;
            int itmCount = e3c.sheet.GetGraphIds(ref ids);
            for (int i = 1; i <= itmCount; i++)
                oldIds.Add(ids[i]);
            itmCount = e3c.sheet.GetDimensionIds(ref ids);
            for (int i = 1; i <= itmCount; i++)
                oldIds.Add(ids[i]);
            itmCount = e3c.sheet.GetGroupIds(ref ids);
            for (int i = 1; i <= itmCount; i++)
                oldGroups.Add(ids[i]);

            if (!string.IsNullOrEmpty(externalDoc))
            {
                if (eSettings.Profile.AddDWGToProject && int.TryParse(externalDoc, out int id))
                {
                    e3c.ext.SetId(id);
                    int c = e3c.ext.CheckOut();
                    externalDoc = e3c.job.GetGidOfId(id);
                    filename = e3c.ext.GetFile();
                }
                else
                {
                    filename = externalDoc;
                }
                if (File.Exists(filename))
                {
                    int res = e3c.sheet.ImportDXF(filename, scale, x, y, 0, fontName);
                    if (res > 0)
                    {
                        e3c.app.PutInfo(0, "Импорт подложки в Е3 произведен успешно!");
                        List<int> allIds = new List<int>();
                        //Разбиваем все вложенные группы
                        itmCount = e3c.sheet.GetGroupIds(ref ids);
                        do
                        {
                            for (int i = 1; i <= itmCount; i++)
                            {
                                if (!oldGroups.Contains(ids[i]))
                                {
                                    e3c.group.SetId(ids[i]);
                                    dynamic tmp = null;
                                    e3c.group.Delete(ref tmp);
                                }
                            }
                            itmCount = e3c.sheet.GetGroupIds(ref ids) - oldGroups.Count;
                        }
                        while (itmCount > 0);

                        itmCount = e3c.sheet.GetGraphIds(ref ids);
                        for (int i = 1; i <= itmCount;  i++)
                            allIds.Add(ids[i]);
                        itmCount = e3c.sheet.GetDimensionIds(ref ids);
                        for (int i = 1; i <= itmCount; i++)
                            allIds.Add(ids[i]);
                        var newIds = allIds.Except(oldIds).ToList();
                        dynamic graIds = new object[newIds.Count + 1];
                        List<int> lines = new List<int>();

                        e3c.app.PutInfo(0, "Группировка графики...");
                        for (int i = 0; i < newIds.Count; i++)
                        {
                            graIds[i + 1] = newIds[i];
                            e3c.graph.SetId(newIds[i]);
                            if (eSettings.Profile.DwgCreateConnectLine && e3c.graph.GetLevel() == eSettings.Profile.DwgConnectLinesLevelNumber)
                                lines.Add(newIds[i]);
                        }

                        filename = Path.GetFileName(filename);
                        int grId = e3c.group.Create(ref graIds, filename);
                        if (grId > 0)
                        {
                            e3c.app.PutInfo(0, "Графика сгруппирована успешно!");
                            e3c.group.SetAttributeValue(eSettings.Profile.DWGFileAttribute, externalDoc);
                            if (newSheetCreated && x == 0)
                            {
                                e3c.app.PutInfo(0, "Удаление вспомогательных построений!");
                                e3c.group.Unplace();
                                e3c.sheet.Delete();
                                dynamic grids = null;
                                int grcnt = 0;
                                e3c.app.PutInfo(0, "Получение неразмещенных групп...");
                                try
                                {
                                    grcnt = e3c.job.GetUnplacedGroupIds(ref grids);
                                }
                                catch
                                {
                                    e3c.InitE3Objects();
                                    grcnt = e3c.job.GetUnplacedGroupIds(ref grids);
                                }
                                e3c.app.PutInfo(0, "Размещение группы...");
                                e3c.group.SetId(grids[grcnt]);
                                int oldGrcnt = e3c.job.GetGroupIds(ref grids);
                                int groupPlace = e3c.group.PlaceInteractively();
                                if (groupPlace>0 && eSettings.Profile.DwgCreateConnectLine)
                                {
                                    foreach (var gr in lines)
                                    {
                                        CreateConnectionFromGraphic(sheetID, gr);
                                    }
                                }
                                grcnt = e3c.job.GetGroupIds(ref grids);
                                if (grcnt > oldGrcnt)
                                {
                
                                }
                                else
                                {
                                    MessageWindow.ShowDialog("Ошибка размещения подложки!\nСмотрите подробности в окне сообщений", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                                }
                            }
                        }
                        else
                        {
                            if (newSheetCreated)
                            {
                                e3c.ext.Delete();
                                e3c.sheet.Delete();
                            }
                            MessageWindow.ShowDialog("Ошибка загрузки подложки!\nСмотрите подробности в окне сообщений", "" ,"Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                        }
                    }
                    else
                    {
                        if (newSheetCreated)
                        {
                            e3c.ext.Delete();
                            e3c.sheet.Delete();
                        }
                        MessageWindow.ShowDialog("Ошибка загрузки файла подложки!\nСмотрите подробности в окне сообщений", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                    }
                }
                e3c.ext.checkin();
            }
            e3c.DisposeAllE3ComObjects();
        }

        /// <summary>
        ///Обновление подложки
        /// </summary>
        public void RefreshDWG()
        {
            e3c.InitE3Objects();
            int currentSheetID = e3c.job.GetActiveSheetId();
            string fontName = e3c.job.GetGraphTextFontName();
            dynamic grIds = null;
            dynamic extIds = null;
            int extCount = e3c.job.GetTreeSelectedExternalDocumentIds(ref extIds);
            int grCount = e3c.job.GetSelectedGraphIds(ref grIds);
            if (grCount==0)
                grCount = e3c.job.GetSelectedTextIds(ref grIds);
            if (grCount > 0)
            {
                int grId = e3c.group.GetGroupId(grIds[1]);
                e3c.group.SetId(grId);
                string fileName = e3c.group.GetAttributeValue(eSettings.Profile.DWGFileAttribute);
                object x = null, y = null, z = null;
                int sheetId = e3c.group.GetLocation(ref x, ref y, ref z, ref z);
                e3c.sheet.SetId(sheetId);
                double scale = 1;
                if (scale == 0)
                    scale = 1;
                int id = e3c.job.GetIdOfGid(fileName);
                bool ok=true;
                if ((id > 0 && e3c.ext.SetId(id) > 0) || File.Exists(fileName))
                {
                    e3c.group.DeleteContents();
                    if (id>0)
                        fileName = id.ToString();
                    LoadDWG(fileName, currentSheetID, fileName, scale, (double)x, (double)y);
                }
                else
                    ok = false;
                if (ok==false)
                {
                    if (extCount != 1)
                        MessageWindow.ShowDialog("Если файл был ранее импортирован в E3.series, то для повторной связки выберите файл в дереве, выделите один из элементов подложки на листе и повторно вызовите команду", "Не удалось найти файл подложки", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                    else
                    {
                        fileName = e3c.job.GetGidOfId(extIds[1]);
                        e3c.group.SetAttributeValue(eSettings.Profile.DWGFileAttribute, fileName);
                    }
                }
            }
            else
            {
                MessageWindow.ShowDialog("Для обновления подложки необходимо сначала выделить один из ее элементов (текст, графика)", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
            e3c.DisposeAllE3ComObjects();
        }

        /// <summary>
        /// Создание линии соединений из графики листа
        /// </summary>
        /// <param name="sheetID"></param>
        /// <param name="graphID"></param>
        public static void CreateConnectionFromGraphic(int sheetID, int graphID)
        {
            e3c.graph.SetId(graphID);
            if (e3c.graph.GetType() == 2)
            {
                object npoints = null;
                dynamic xarr = null, yarr = null;
                int ok = e3c.graph.GetPolygon(ref npoints, ref xarr, ref yarr);
                if (ok > 0)
                {
                    ok = e3c.con.Create(sheetID, (int)npoints, ref xarr, ref yarr);
                }
            }
            else if (e3c.graph.GetType() == 1)
            {
                object x1 = null, x2 = null, y1 = null, y2 = null;
                int ok = e3c.graph.GetLine(ref x1, ref y1, ref x2, ref y2);
                if (ok > 0)
                {
                    dynamic xarr = new object[3];
                    xarr[1] = x1;
                    xarr[2] = x2;
                    dynamic yarr = new object[3];
                    yarr[1] = y1;
                    yarr[2] = y2;
                    ok = e3c.con.Create(sheetID, 2, ref xarr, ref yarr);
                    if (ok < 1)
                    {
                        xarr[1] = x2;
                        xarr[2] = x1;
                        yarr[1] = y2;
                        yarr[2] = y1;
                        e3c.con.Create(sheetID, 2, ref xarr, ref yarr);
                    }
                }
            }
        }

        #endregion

    }
}
