﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Layout
{

    /// <summary>
    /// Общий класс символа Е3
    /// </summary>
    public class ESymbolBase : eBaseItem
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Rotation { get; set; }
        public int SheetID { get; set; }


        public ESymbolBase()
        {
            ID = e3c.sym.GetId();
            GID = e3c.job.GetGidOfId(ID);
            Name = e3c.sym.GetSymbolTypeName();
            GetCoordinates();
        }

        public void GetCoordinates()
        {
            e3c.sym.SetId(ID);
            object symX = null, symY = null, grid = null;
            SheetID = e3c.sym.GetSchemaLocation(ref symX, ref symY, ref grid);


            X = (double)symX;
            Y = (double)symY;
            object minX = null, minY = null, maxX = null, maxY = null;
            double l = e3c.sym.GetDisplayLength();
            double w = e3c.sym.GetDisplayWidth();
            e3c.sym.GetArea(ref minX, ref minY, ref maxX, ref maxY);
            if (l == 0 && w == 0)
            {
                Width = Math.Abs((double)maxX - (double)minX);
                Height = Math.Abs((double)maxY - (double)minY);
                Left = X + (double)minX;
                Right = X + (double)maxX + l;
                Bottom = Y + (double)minY;
                Top = Y + (double)maxY + w;
            }
            else
            {
                Left = X + (double)minX;
                Right = X + l;
                Bottom = Y + (double)minY;
                Top = Y + w;
            }
        }


        public string GetNameNameWithoutCharacteristic()
        {
            if (Name != null)
                return Regex.Replace(Name, "¶.*", "");
            else
                return "";
        }


        public string GetCharacteristicWithoutName()
        {
            return Regex.Match(Name, "¶.*").ToString().TrimStart('¶');
        }
    }

}
