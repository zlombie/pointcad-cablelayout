﻿using Layout;
using System;


[Serializable]
[Magic]
public class LayingType : eItemWithMaterialComponents
{

    public string HorizontalSymbol { get; set; }
    public string VerticalSymbol { get; set; }
    public int Delta { get; set; }
    public bool InteractivePlace { get; set; }
    public string LengthAttribute { get; set; }

    private string _sectionType;
    public string SectionType
    {
        get
        {
                if (string.IsNullOrEmpty(_sectionType))
                    return "<Нет данных>";
                else
                    return _sectionType;
        }
        set
        {
             _sectionType = value;
        }
    }

    public LayingType()
    {
        SectionType = "<Нет данных>";
    }


}

