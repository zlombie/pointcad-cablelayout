﻿namespace Layout.Enums
{
    public enum ItemType
    {
        None,
        Device,
        NetSegment,
        HeightSymbol
    }
}