﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using e3;
using System.Drawing;
using Layout.Configuration;

namespace Layout
{
    public static class EnumEx
    {
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }

    struct E3Line
    {
        public double x1;
        public double y1;
        public double x2;
        public double y2;
        public bool IsVertical;
    }

    struct PointD
    {
        public double X;
        public double Y;

        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }
    }


    public class LengthWithType
    {
        public double Length { get; set; }
        public string Attr { get; set; }
        public string Name { get; set; }
    }

    public class ELine
    {
        public PointF P1 { get; set; }
        public PointF P2 { get; set; }
    }

    public class CableBase : eBaseItem
    {
        public int CoreId { get; set; }
        public string Component { get; set; }
        public string Type { get; set; }
    }

    public class Cable : CableBase
    {
        public int PinID1 { get; set; }
        public int PinID2 { get; set; }
        public double Reserve { get; set; }
        public double Length { get; set; }
        public double FinalLength { get; set; }
        public List<EItemOnLayoutAdvanced> NetSegments = new List<EItemOnLayoutAdvanced>();
        public List<LengthWithType> LengthsWithType { get; set; }

        public Cable(int id)
        {
            LengthsWithType = new List<LengthWithType>();
            e3c.dev.SetId(id);
            ID = id;
            Name = e3c.dev.GetName();
            Type = e3c.dev.GetComponentName();
        }

        public void SetLayingLengths()
        {
            double allLen = FinalLength;
            if (LengthsWithType.Count > 1)
            {
                //Находим минимальное значение
                double min = int.MaxValue;
                int index = 0;
                for (int i = 0; i < LengthsWithType.Count; i++)
                {
                    if (LengthsWithType[i].Length != 0 && LengthsWithType[i].Length < min)
                    {
                        min = LengthsWithType[i].Length;
                        index = i;
                    }
                }
                //Определяем множитель
                double sum = 0;
                for (int i = 0; i < LengthsWithType.Count; i++)
                {
                    LengthsWithType[i].Length = LengthsWithType[i].Length / min;
                    sum += LengthsWithType[i].Length;
                }
                //Множитель
                sum = allLen / sum;
                //Перерасчет
                for (int i = 0; i < LengthsWithType.Count; i++)
                {
                    LengthsWithType[i].Length = Math.Round(LengthsWithType[i].Length * sum);
                    if (double.IsNaN(LengthsWithType[i].Length))
                        LengthsWithType[i].Length = 0;
                }
            }
            else if (LengthsWithType.Count == 1)
            {
                LengthsWithType[0].Length = FinalLength;
            }
        }
    }

    public class EGroup
    {
        public string GID { get; set; }
        public int TxtID { get; set; }
    }
}