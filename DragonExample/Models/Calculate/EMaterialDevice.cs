﻿using Layout.Configuration;
using System;
using System.ComponentModel;


namespace Layout
{
    [Serializable]
    [Magic]
    public class eMaterialDevice : eBaseItem, INotifyPropertyChanged
    {

        protected void RaisePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public eMaterialDevice(EMaterialComponent component, double segmLenth, double k)
        {
            if (component!=null && !string.IsNullOrEmpty(component.Component))
            {
                Component = component;
                Quantity = Component.GetQuantity(segmLenth, k);
                Segments = 1;
                Owner = "Материалы, назначенные участкам трассы";
            }
        }

        public void SetQuantity()
        {
            e3c.dev.SetId(ID);
            e3c.dev.SetAttributeValue(eSettings.Profile.MetalCalcDeviceQuantityAttribute, FinalQuantity.ToString());
        }

        public EMaterialComponent Component { get; set; }
        public double Quantity { get; set; }
        private double _additional;
        public double Additional
        {
            get
            {
                return _additional;
            }
            set
            {
                if (_additional!=value)
                {
                    _additional = value;
                    RaisePropertyChanged("FinalQuantity");
                }
            }
        }
        public double FinalQuantity { get { return Quantity + Additional; } }
        public string Owner { get; set; }
        public int Segments { get; set; }

        public void RoundQuantity()
        {
            if (Component.Units == "участок трассы" || Component.RoundNumInt == 99)
            {
                return;
            }
            switch (Component.RoundType)
            {
                case "Математически":
                    Quantity = Math.Round(Quantity, Component.RoundNumInt);
                    return;
                case "В большую сторону":
                    Quantity=Math.Ceiling(Quantity * Math.Pow(10, Component.RoundNumInt)) / Math.Pow(10, Component.RoundNumInt);
                    return;
                case "В меньшую сторону":
                    Quantity = Math.Truncate(Quantity * Math.Pow(10, Component.RoundNumInt)) / Math.Pow(10, Component.RoundNumInt);
                    return;
                default:
                    return;
            }
        }
    }
}