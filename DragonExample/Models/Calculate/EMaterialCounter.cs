﻿
using Layout.Streams;
using Pointcad.Dragon.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Layout.Service;
using Layout.Area;
using Layout.Configuration;

namespace Layout
{
    public class EMetalCounter
    {

        private const string saveAttrConst = "Instrumentation_Расчет";
        public ObservableCollection<eMaterialDevice> MaterialsList { get; set; }

        public EMetalCounter()
        {
            MaterialsList = new ObservableCollection<eMaterialDevice>();
            e3c.InitE3Objects();
            Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
            MaterialsList = new ObservableCollection<eMaterialDevice>(Utilites.CurrentSheet.GetMaterials());
            
            //var segmsWithoutParameters = layingSegments.FindAll(f => string.IsNullOrEmpty(f.ConstructionParams));
            //if (segmsWithoutParameters.Count > 0)
            //{
            //    segmsWithoutParameters.ForEach(f => e3c.app.PutWarning(0, $"Для участка трассы (ID={f.ID}, длина {f.Length} м.) не заданы параметры металлоконструкций!", f.ID));              
            //    if (MessageWindow.ShowDialog($"Обнаружено {segmsWithoutParameters.Count} участков трасс с незаданными параметрами металлоконструкций! Подробности смотрите в окне сообщений.\n\nПродолжить выполнение операции?", "Внимание!", "Расчет количества металлоконструкций", MessageWindowButtons.YesNo, MessageWindowImages.Warning) == MessageDialogResults.No)
            //        return;
            //}
        }



        public void Write()
        {
            DeleteOldMaterials();
            e3c.InitE3Objects();
            e3c.sheet.SetId(Utilites.CurrentSheet.ID);
            List<ESymbolBase> metalSymbols = new List<ESymbolBase>();
            double currentX = eSettings.Profile.NonInteractivelyX;
            double currentY = eSettings.Profile.NonInteractivelyY;

            foreach (var mcomp in MaterialsList)
            {
                mcomp.ID = e3c.dev.Create(null, null, null, mcomp.Component.Component, "CCCC", 0);
                if (mcomp.ID > 0)
                {
                    mcomp.SetQuantity();
                    dynamic symIds = null;
                    int symCount = e3c.dev.GetSymbolIds(ref symIds);
                    //for (int i = 1; i <= symCount; i++)
                    if (symCount > 0)
                    {
                        e3c.sym.SetId(symIds[1]);
                        if (eSettings.Profile.MetalCalcPlaceMode.Contains("Интерактивно"))
                        {
                            metalSymbols.Add(new ESymbolBase());
                        }
                        else
                        {
                            e3c.sym.Place(Utilites.CurrentSheet.ID, currentX, currentY);
                            currentX += eSettings.Profile.NonInteractivelyDeltaX;
                            currentY += eSettings.Profile.NonInteractivelyDeltaY;
                        }
                        e3c.sym.SetLevel(eSettings.Profile.MetalCalcSymbolLevel);
                        e3c.sym.SetAttributeValue(eSettings.Profile.CalcMetalSaveAttribute, saveAttrConst);
                    }
                }
            }
            double insertX = 0, insertY = 0;

            columns = 1;
            if (eSettings.Profile.MetalCalcPlaceMode.Contains("Интерактивно"))
            {
                double averageW = metalSymbols.Average(o => o.Width);
                double averageH = metalSymbols.Average(o => o.Height) * metalSymbols.Count;

                KeyboardHook gkh = new KeyboardHook();
                for (int j = 49; j <= 57; j++)
                {
                    gkh.HookedKeys.Add((Keys)j);
                }
                gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
                var outer = Task.Factory.StartNew(() =>
                {
                    while (insertX == 0)
                    {
                        EStreamTable.CreatePreviewRectangle(Utilites.CurrentSheet.ID, columns * averageW, Math.Ceiling(averageH / columns), ref insertX, ref insertY);
                    }
                });
                outer.Wait(); // ожидаем выполнения внешней задачи
                gkh.unhook();
                int counter = 1;
                currentX = insertX;
                currentY = insertY;
                foreach (var s in metalSymbols)
                {
                    e3c.sym.SetId(s.ID);
                    e3c.sym.Place(Utilites.CurrentSheet.ID, currentX, currentY);
                    e3c.sym.SetLevel(eSettings.Profile.MetalCalcSymbolLevel);
                    e3c.sym.SetAttributeValue(eSettings.Profile.CalcMetalSaveAttribute, saveAttrConst);
                    counter++;
                    if (counter > columns)
                    {
                        currentX = insertX;
                        currentY -= s.Height;
                    }
                    else
                    {
                        currentX += s.Width;
                    }
                }
            }
            SendKeys.SendWait("{F5}");
            e3c.DisposeE3Job();
        }

        public void DeleteOldMaterials()
        {
            e3c.InitE3Objects();
            e3c.sheet.SetId(Utilites.CurrentSheet.ID);
            //Удаление старых символов
            dynamic symIds = null;
            int symCount = e3c.sheet.GetSymbolIds(ref symIds);
            for (int i = 1; i <= symCount; i++)
            {
                e3c.sym.SetId(symIds[i]);
                if (e3c.sym.GetAttributeValue(eSettings.Profile.CalcMetalSaveAttribute) == saveAttrConst)
                {
                    e3c.dev.SetId(symIds[i]);
                    if (e3c.dev.IsView() > 0)
                    {
                        int originalId = e3c.dev.GetOriginalId();
                        e3c.dev.SetId(originalId);
                    }
                    e3c.dev.DeleteForced();
                }
            }
            e3c.DisposeE3Job();
        }

        int columns = 1;
        void gkh_KeyDown(object sender, KeyEventArgs e)
        {
            SendKeys.SendWait("{ESC}");
            columns = (int)e.KeyCode - 48;
            e.Handled = true;
        }
    }
}
