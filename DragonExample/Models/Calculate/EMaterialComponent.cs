﻿using System;
using System.ComponentModel;

namespace Layout
{
    [Serializable]
    [Magic]
    public class EMaterialComponent : INotifyPropertyChanged
    {

        protected void RaisePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public EMaterialComponent(string component, int ratioNum, int ratioDen, string units, string roundNum, string roundType)
        {
            if (!string.IsNullOrEmpty(component))
            {
                Component = component;
                RatioDenominator = ratioDen;
                RatioNumerator = ratioNum;
                Units = units;
                RoundType = roundType;
                RoundNum = roundNum;
            }
        }

        public EMaterialComponent()
        {
        }

        private string _component;
        
        public string Component
        {
            get
            {
                return _component;
            }
            set
            {
                if (_component != value)
                {
                    _component = value;
                    RaisePropertyChanged("FullName");
                }
            }
        }

        private int _ratioNumerator;
        public int RatioNumerator
        {
            get
            {
                return _ratioNumerator;
            }
            set
            {
                if (_ratioNumerator != value)
                {
                    _ratioNumerator = value;
                    RaisePropertyChanged("FullName");
                }
            }
        }

        private int _ratioDenominator;
        public int RatioDenominator
        {
            get
            {
                return _ratioDenominator;
            }
            set
            {
                if (_ratioDenominator != value)
                {
                    _ratioDenominator = value;
                    RaisePropertyChanged("FullName");
                }
            }
        }

        public int MaxDenominator { get; set; }

        private string _units;
        public string Units
        {
            get
            {
                return _units;
            }
            set
            {
                if (_units != value)
                {
                    _units = value;
                    if (_units == "участок трассы")
                    {
                        MaxDenominator = 1;
                        RatioDenominator = 1;
                    }
                    else
                    {
                        MaxDenominator = 9999;
                    }
                    RaisePropertyChanged("FullName");
                }
            }
        }
        public string FullName {get { return $"{_component} ({RatioNumerator} ед. на {RatioDenominator} {Units})"; } }

        public string RoundNum { get; set; }
        public int RoundNumInt
        {
            get
            {
                switch (RoundNum)
                {
                    case "До целого":
                        return 0;
                    case "1 знак после запятой":
                        return 1;
                    case "2 знака после запятой":
                        return 2;
                    case "3 знака после запятой":
                        return 3;
                    case "Не округлять":
                        return 99;
                    default:
                        return 0;
                }
            }
        }
        public string RoundType { get; set; }


        public double GetQuantity(double length, double k)
        {
            if (Units == "участок трассы")
            {
                return RatioNumerator;
            }
             return length * k * RatioNumerator / RatioDenominator;


        }


    }
}