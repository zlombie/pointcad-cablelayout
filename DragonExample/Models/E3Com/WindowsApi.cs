﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;


namespace Layout
{
	internal static class WindowsApi
	{
		public const int WM_GETTEXT = 0x000D;
		public const int WM_GETTEXTLENGTH = 0x000E;
		public const int WM_SETTEXT = 0x000C;


		[SuppressMessage("ReSharper", "UnusedMember.Global")]
		[SuppressMessage("ReSharper", "InconsistentNaming")]
		public enum GetWindow_Cmd : uint
		{
			GW_HWNDFIRST = 0,
			GW_HWNDLAST = 1,
			GW_HWNDNEXT = 2,
			GW_HWNDPREV = 3,
			GW_OWNER = 4,
			GW_CHILD = 5,
			GW_ENABLEDPOPUP = 6
		}

		#region Methods

		private static bool EnumWindow(IntPtr handle, IntPtr pointer)
		{
			GCHandle gch = GCHandle.FromIntPtr(pointer);
			var list = gch.Target as List<IntPtr>;
			if (list == null)
			{
				throw new InvalidCastException("GCHandle Target не может быть приведен к типу List<IntPtr>");
			}
			list.Add(handle);
			return true;
		}

		public static List<IntPtr> GetChildWindows(IntPtr parent)
		{
			var result = new List<IntPtr>();
			GCHandle listHandle = GCHandle.Alloc(result);
			try
			{
				var childProc = new NativeMethods.EnumWindowsProc(EnumWindow);
				NativeMethods.EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
			}
			finally
			{
				if (listHandle.IsAllocated)
				{
					listHandle.Free();
				}
			}
			return result;
		}

		/// <summary>
		/// Вычисление самого "верхнего" дескриптора окна из списка дескрипторов.
		/// </summary>
		/// <param name="hwnds">Список дескрипторов.</param>
		/// <returns>Дескриптор "верхнего" окна.</returns>
		public static IntPtr GetTopmostHwnd(List<IntPtr> hwnds)
		{
			var topmostHwnd = IntPtr.Zero;

			if (hwnds != null && hwnds.Count > 0)
			{
				var hwnd = hwnds[0];

				while (hwnd != IntPtr.Zero)
				{
					if (hwnds.Contains(hwnd))
					{
						topmostHwnd = hwnd;
					}

					hwnd = NativeMethods.GetWindow(hwnd, GetWindow_Cmd.GW_HWNDPREV);
				}
			}

			return topmostHwnd;
		}

		#endregion
	}

	/// <summary>
	/// NativeMethods — этот класс не подавляет проверку стека на разрешение неуправляемого кода. (к классу не должен быть применен System.Security.SuppressUnmanagedCodeSecurityAttribute.) Этот класс предназначен для методов, которые могут быть использованы где угодно, поскольку выполняется проверка стека.
	/// <remarks>CA1060: переместите P/Invokes в класс NativeMethods</remarks>
	/// </summary>
	internal static class NativeMethods
	{
		[DllImport("ole32.dll", PreserveSig = false)]
		public static extern void CreateBindCtx(uint reserved, out IBindCtx ppbc);


		/// <summary>
		/// Returns a pointer to the IRunningObjectTable
		/// interface on the local running object table (ROT).
		/// </summary>
		/// <param name="reserved">This parameter is reserved and must be 0.</param>
		/// <param name="prot">The address of an IRunningObjectTable* pointer variable
		/// that receives the interface pointer to the local ROT. When the function is
		/// successful, the caller is responsible for calling Release on the interface
		/// pointer. If an error occurs, *pprot is undefined.</param>
		/// <see cref="http://www.pinvoke.net/default.aspx/ole32.GetRunningObjectTable"/>
		/// <returns>This function can return the standard return values E_UNEXPECTED and S_OK.</returns>
		[DllImport("ole32.dll")]
		public static extern int GetRunningObjectTable(uint reserved, out IRunningObjectTable pprot);


		public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, IntPtr lParam);


		[DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

		[DllImport("user32.dll", SetLastError = false)]
		public static extern IntPtr GetDesktopWindow();

		/// <summary>
		/// Возвращает дескриптор приоритетного окна (окна, с которым пользователь в настоящее время работает). 
		/// Система присваивает немного более высокий приоритет потоку, который создает приоритетное окно, 
		/// чем тот, который она дает другим потокам.
		/// </summary>
		[DllImport("user32.dll")]
		public static extern IntPtr GetForegroundWindow();

		[DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
		public static extern IntPtr GetParent(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr GetWindow(IntPtr hWnd, WindowsApi.GetWindow_Cmd uCmd);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int GetWindowTextLength(IntPtr hWnd);


		/// <summary>
		/// Возвращает обратно идентификатор потока, который создал определяемое окно и, необязательно, идентификатор процесса, который создал окно.
		/// </summary>
		/// <param name="hWnd">Дескриптор окна.</param>
		/// <param name="lpdwProcessId">Указатель на переменную, которая принимает идентификатор процесса.</param>
		/// <returns>Идентификатор потока, который создает окно.</returns>
		[DllImport("user32.dll", SetLastError = true)]
		public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

		[DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool IsWow64Process([In] IntPtr process, [Out] out bool wow64Process);


		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

		[DllImport("user32.dll")]
		public static extern IntPtr SendMessage(IntPtr hWnd,
												uint msg,
												IntPtr wParam,
												[MarshalAs(UnmanagedType.LPStr)] string lParam);


		// Вызывает предупреждение CA1901: объявления P/Invoke должны быть переносимыми
		//[DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
		//public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

		// Вызывает предупреждение CA1901: объявления P/Invoke должны быть переносимыми
		//[DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
		//public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, int wParam, StringBuilder lParam);

		// Смотреть: http://www.pinvoke.net/default.aspx/user32.sendmessage
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, StringBuilder lParam);
	}
}