﻿using System;


namespace Layout
{
	/// <summary>
	/// Тип запущенного процесса E3.series.
	/// </summary>
	[Flags]
	public enum E3Type
	{
		/// <summary>
		/// Процесс E3.series запущенный в обычном режиме.
		/// </summary>
		E3 = 1,

		/// <summary>
		/// Процесс E3.series запущенный в режиме редакторе БД.
		/// </summary>
		DBE = 2
	}
}