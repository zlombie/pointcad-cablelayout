﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace Layout
{
	public sealed class ComObjectDispatcher : IDisposable
	{
		/// <summary>
		/// Специальный класс для определения окна E3.series в режиме редактора БД.
		/// Examples with Running Object Table (ROT)
		/// Согласно примеру в COM справке от E3.series 2016
		/// </summary>
		[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("00000001-0000-0000-C000-000000000046")]
		private interface IClassFactory
		{
			[PreserveSig]
			int CreateInstance(IntPtr pUnkOuter, ref Guid riid, out IntPtr ppvObject);
		}

		/// <summary>
		/// С этого имени начинается "внутреннее имя процесса" E3.series в обычном режиме запуска.
		/// </summary>
		private const string E3ApplicationName = "!E3Application";

		/// <summary>
		/// С этого имени начинается "внутреннее имя процесса" E3.series в режиме редактора БД.
		/// </summary>
		private const string E3DbeApplicationName = "!E3DbeApplication";

		/// <summary>
		/// С этого имени начинается "внутреннее имя процесса" E3.series в режиме редактора БД для версий ниже E3.series 2015.
		/// </summary>
		private const string E3DbeApplicationName2 = "DBE!";


		public ComObjectDispatcher(E3Type e3Type = E3Type.E3)
		{
			Processes = GetComProcesses(e3Type);
		}

		private List<ComProcess> _processes;

		private List<ComProcess> Processes
		{
			get { return _processes ?? (_processes = new List<ComProcess>()); }
			set
			{
				if (_processes != null)
				{
					foreach (ComProcess process in _processes)
					{
						if (!process.IsOutside)
						{
							process.Dispose();
						}
					}
					_processes.Clear();
				}
				_processes = value;
			}
		}

		/// <summary>
		/// Обновления списка процессов
		/// </summary>
		/// <param name="e3Type"></param>
		/// <remarks>Согласно примеру в COM справке: Examples with Running Object Table (ROT)</remarks>
		/// <returns></returns>
		private static List<ComProcess> GetComProcesses(E3Type type)
		{
            var processes = new List<ComProcess>();

            IBindCtx bindContext;
            IRunningObjectTable rot;
            NativeMethods.GetRunningObjectTable(0, out rot);
            if (rot == null) return processes;

            NativeMethods.CreateBindCtx(0, out bindContext);
            if (bindContext == null) return processes;

            IEnumMoniker monikerEnumerator;
            rot.EnumRunning(out monikerEnumerator);
            if (monikerEnumerator == null) return processes;

            monikerEnumerator.Reset();
            IntPtr pNumFetched = new IntPtr();
            IMoniker[] monikers = new IMoniker[1];

            while (monikerEnumerator.Next(1, monikers, pNumFetched) == 0)
            {
                object obj;
                rot.GetObject(monikers[0], out obj);
                if (obj == null) continue;

                string displayName;
                monikers[0].GetDisplayName(bindContext, null, out displayName);
                if (string.IsNullOrEmpty(displayName)) continue;

                var classFactory = obj as IClassFactory;

                dynamic dyn = null;

                if (type.HasFlag(E3Type.DBE) && classFactory != null &&
                    displayName.StartsWith(E3DbeApplicationName, StringComparison.CurrentCultureIgnoreCase))
                // "!" + "E3DBEAPPLICATIONFACTORY" + ":"
                {
                    // Для E3.series 2015 и выше

                    IntPtr appObject;
                    var guidIUnknown = new Guid("00000000-0000-0000-C000-000000000046");
                    int result = classFactory.CreateInstance(IntPtr.Zero, ref guidIUnknown, out appObject);

                    if (appObject != null)
                    {
                        dyn = Marshal.GetObjectForIUnknown(appObject);
                    }
                }
                // Для нижних  E3.series 2015 версий найти объект не известно как.

                else if (type.HasFlag(E3Type.E3) &&
                         displayName.StartsWith(E3ApplicationName, StringComparison.CurrentCultureIgnoreCase))
                // "!" + "E3APPLICATION" + ":"
                {
                    dyn = obj;
                }

                if (dyn != null)
                {
                    int pid;

                    // Извлечение из displayName идентификатор процесса
                    int index = displayName.LastIndexOf(':');
                    if (index != -1 && index < displayName.Length && int.TryParse(displayName.Substring(index + 1), out pid))
                    {
                        processes.Add(new ComProcess(pid, dyn));
                    }
                }
            }

            return processes;
        }

		/// <summary>
		/// Старая версия загрузки данных
		/// </summary>
		/// <param name="startApplicationName"></param>
		private void Update(string startApplicationName)
		{
            var processes = new List<ComProcess>();

            IBindCtx ctx;
            IRunningObjectTable table;
            IEnumMoniker monikerEnumerator;
            var monikers = new IMoniker[1];

            NativeMethods.CreateBindCtx(0, out ctx);
            ctx.GetRunningObjectTable(out table);
            table.EnumRunning(out monikerEnumerator);
            monikerEnumerator.Reset();


            while (monikerEnumerator.Next(1, monikers, IntPtr.Zero) == 0)
            {
                string objectName;
                monikers[0].GetDisplayName(ctx, monikers[0], out objectName);

                if (objectName.StartsWith(startApplicationName, StringComparison.CurrentCultureIgnoreCase))
                {
                    int index = objectName.LastIndexOf(':');
                    if (index != -1 && index < objectName.Length)
                    {
                        int pid;
                        if (int.TryParse(objectName.Substring(index + 1), out pid))
                        {
                            object objectVal;
                            table.GetObject(monikers[0], out objectVal);

                            processes.Add(new ComProcess(pid, objectVal));
                        }
                    }
                }
            }

            Processes = processes;
        }


		/// <summary>
		/// Возвращает COM-объект, связанный с процессом имеющим указанный идентификатор.
		/// </summary>
		/// <param name="processId">Идентификатор процесса.</param>
		/// <returns>COM-объект.</returns>
		public object GetComObjectByProcessId(int processId)
		{
			ComProcess process = Processes.FirstOrDefault(p => p.Id == processId);

			if (process != null)
			{
				process.IsOutside = true;
				return process.Obj;
			}

			return null;
		}

		public int GetComProcessCount()
		{
			return Processes.Count;
		}

		public List<ComProcess> GetComProcesses()
		{
			var processes = new List<ComProcess>();

			foreach (ComProcess process in Processes)
			{
				process.IsOutside = true;
				processes.Add(process);
			}

			return processes;
		}

		internal List<IntPtr> GetAllHwnds()
		{
			List<IntPtr> hwnds = new List<IntPtr>();

			foreach (ComProcess comProcess in Processes)
			{
				try
				{
					Process process = Process.GetProcessById(comProcess.Id);
					hwnds.Add(process.MainWindowHandle);
				}
				catch (Exception)
				{
					//continue;
				}
			}

			return hwnds;
		}

		#region Dispose

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);

			//GC.Collect();
			//GC.WaitForPendingFinalizers();
		}

		// NOTE: Leave out the finalizer altogether if this class doesn't   
		// own unmanaged resources itself, but leave the other methods  
		// exactly as they are.   
		~ComObjectDispatcher()
		{
			// Finalizer calls Dispose(false)  
			Dispose(false);
		}

		private void CleanUp()
		{
			Processes = null;
		}


		// The bulk of the clean-up code is implemented in Dispose(bool)  
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				CleanUp();
			}
		}

		#endregion
	}

	public sealed class ComProcess : IDisposable
	{
		public int Id
		{
			get;
			private set;
		}

		public object Obj
		{
			get;
			private set;
		}

		internal bool IsOutside
		{
			get;
			set;
		}

		public ComProcess(int id, object obj)
		{
			Id = id;
			Obj = obj;
		}

		#region Dispose: CA1063: следует правильно реализовывать IDisposable

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);

			//GC.Collect();
			//GC.WaitForPendingFinalizers();
		}

		// NOTE: Leave out the finalizer altogether if this class doesn't   
		// own unmanaged resources itself, but leave the other methods  
		// exactly as they are.   
		~ComProcess()
		{
			// Finalizer calls Dispose(false)  
			Dispose(false);
		}


		// The bulk of the clean-up code is implemented in Dispose(bool)  
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (Obj != null && Marshal.IsComObject(Obj)) Marshal.ReleaseComObject(Obj);
				Obj = null;
			}
		}

		#endregion
	}
}