﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Pointcad.Dragon.Windows;
using FlexNet;
#if DEBUG
    using e3;
#endif

namespace Layout
{
    public static class e3c
    {
        #region Переменные E3
#if DEBUG
        public static e3Application app = null;
        public static e3Job job = null;
        public static e3Sheet sheet = null;
        public static e3Text txt = null;
        public static e3Symbol sym = null;
        public static e3Symbol sym1 = null;
        public static e3Device dev = null;
        public static e3Pin core = null;
        public static e3NetSegment segm = null;
        public static e3Field fld = null;
        public static e3ConnectLine line = null;
        public static e3Graph graph = null;
        public static e3Group group = null;
        public static e3ExternalDocument ext = null;
        public static e3Dimension dim = null;
        public static e3Tree tree = null;
        public static e3Attribute att = null;
        public static e3Connection con = null;
        public static e3Component comp = null;
#else
        public static dynamic app = null;
        public static dynamic job = null;
        public static dynamic sheet = null;
        public static dynamic txt = null;
        public static dynamic sym = null;
        public static dynamic sym1 = null;
        public static dynamic dev = null;
        public static dynamic core = null;
        public static dynamic segm = null;
        public static dynamic fld = null;
        public static dynamic line = null;
        public static dynamic graph = null;
        public static dynamic group = null;
        public static dynamic ext = null;
        public static dynamic dim = null;
        public static dynamic tree = null;
        public static dynamic att = null;
        public static dynamic con = null;
        public static dynamic comp = null;
#endif


        static e3c()
        {


#if DEBUG
            app = E3Connection.ConnectToE3() as e3Application;
#else
            app = E3Connection.ConnectToE3();
#endif
            if (app == null)
            {
                MessageWindow.ShowDialog("Запуск приложения невозможен (невозможно подключиться к E3.series)", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                Environment.Exit(0);
            }
        }

        // Процедура инициализации объектов для работы с E3.Series
        public static void InitE3Objects(bool tryConnect=true)
        {
            //try
            //{
                if (app != null)
                    job = app.CreateJobObject();
                else if (!tryConnect)
                    Environment.Exit(0);
                else
                {
#if DEBUG
                    app = E3Connection.ConnectToE3() as e3Application;
#else
                    app = E3Connection.ConnectToE3();
#endif
                    job = app.CreateJobObject();
                }
                txt = job.CreateTextObject();
                sym = job.CreateSymbolObject();
                sym1 = job.CreateSymbolObject();
                sheet = job.CreateSheetObject();
                dev = job.CreateDeviceObject();
                core = job.CreatePinObject();
                comp = job.CreateComponentObject();
                segm = job.CreateNetSegmentObject();
                fld = job.CreateFieldObject();
                graph = job.CreateGraphObject();
                line = job.CreateConnectLineObject();
                group = job.CreateGroupObject();
                dim = job.CreateDimensionObject();
                ext = job.CreateExternalDocumentObject();
                tree = job.CreateTreeObject();
                att = job.CreateAttributeObject();
                con = job.CreateConnectionObject();
                
                if (job.GetId() == 0)
                {
                   if (tryConnect) MessageWindow.ShowDialog("Запуск приложения невозможен (Нет открытых проектов)", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                   System.Environment.Exit(0);
                }

            //}
            //catch (Exception ex)
            //{
            //    //System.Environment.Exit(0);
            //}
        }

        //Очистка
        public static void DisposeE3ComObjectsFinally(params object[] comObjects)
        {
            if (app != null)
            {
                foreach (var obj in comObjects)
                {
                    try
                    {
                        E3Connection.DisposeFinally(obj);
                    }
                    catch { }
                }
            }
        }

        public static void DisposeAllE3ComObjects()
        {
            DisposeE3ComObjectsFinally(line, fld, segm, ext, att, dim, con, comp, core, group, dev, sym, sym1, tree, txt, sheet, graph);
            {
                try { DisposeE3ComObjectsFinally(job); } catch { }
                DisposeE3ComObjectsFinally(app);
            }
            FlexNet.Helper.CheckIn("E3-Layout");
            FlexNet.Helper.Dispose();
        }

        public static void DisposeE3Job()
        {
            E3Connection.Dispose(job);
        }

#endregion
    }
}
