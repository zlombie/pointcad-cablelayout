﻿using Layout.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Layout
{


    #region E3Dispatcher
    class E3Dispatcher : IDisposable
    {
        #region Private Methods

        [STAThread]
        private void GetRunningE3Object()
        {
            _runningE3Objects = new List<RunningE3Object>();

            UCOMIRunningObjectTable runningObjectTable;
            UCOMIEnumMoniker monikerEnumerator;
            var monikers = new UCOMIMoniker[1];

            WinApi.GetRunningObjectTable(0, out runningObjectTable);
            runningObjectTable.EnumRunning(out monikerEnumerator);
            monikerEnumerator.Reset();

            while (monikerEnumerator.Next(1, monikers, out int numFetched) == 0)
            {
                UCOMIBindCtx ctx;
                WinApi.CreateBindCtx(0, out ctx);

                string runningObjectName;
                monikers[0].GetDisplayName(ctx, null, out runningObjectName);

                if (!runningObjectName.StartsWith("!E3Application", StringComparison.OrdinalIgnoreCase))
                    continue;

                var index = runningObjectName.IndexOf(':');
                if (index == -1) continue;

                object runningObjectVal;
                runningObjectTable.GetObject(monikers[0], out runningObjectVal);

                _runningE3Objects.Add(new RunningE3Object(int.Parse(runningObjectName.Substring(index + 1)),
                                                          runningObjectVal));
            }
        }

        #endregion

        #region List of E3 Objects

        private List<RunningE3Object> _runningE3Objects = new List<RunningE3Object>();

        public class RunningE3Object
        {
            public int ID;
            public object Object;

            public RunningE3Object(int id, object obj)
            {
                ID = id;
                Object = obj;
            }
        }

        #endregion

        #region Constructor

        public E3Dispatcher()
        {
            GetRunningE3Object();
        }

        #endregion

        #region Public Methods

        public object GetE3ByProcessId(int pid)
        {
            return
                (from runningE3Object in _runningE3Objects where runningE3Object.ID == pid select runningE3Object.Object)
                    .FirstOrDefault();
        }

        public void Dispose()
        {
            _runningE3Objects.Clear();
            _runningE3Objects = null;
            GC.Collect();
        }

        #endregion
    }
    #endregion

    #region E3Connection
    /// <summary>
    /// Подключение к E3.series 
    /// </summary>
    public static class E3Connection
    {
        /// <summary>
        /// Выход из приложения с ошибкой
        /// </summary>
        /// <param name="ErrorMessage"></param>
        static void CloseWithError(string ErrorMessage, bool Exit = true)
        {
            //MessageBox.Show(ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            if (Exit == true) { System.Environment.Exit(0); }
        }

        /// <summary>
        /// Создает экземпляр EcubeApplication для самого "верхнего" из окон процессов E3.series.
        /// </summary>
        /// <param name="hWnd">Дескриптору главного окна процесса.</param>
        /// <exception cref="ComObjectForProcessNotRegisteredException">
        /// Возникает если COM-объект для процесса с указанным идентификатором не зарегистрирован в системе.</exception>
        /// <returns></returns>
        public static object ConnectToE3()
        {
            using (var dispatcher = new ComObjectDispatcher())
            {
                int count = dispatcher.GetComProcessCount();
                if (count <= 0)
                {
                    CloseWithError("Нет запущенных окон E3.Series\n\nВыход из программы");
                }

                object obj;
                if (count == 1)
                {
                    var tmp = dispatcher.GetComProcesses()[0];
                    PID = tmp.Id;
                    obj = tmp.Obj;
                    return obj;
                }

                // Список Handle процессов
                List<IntPtr> hwnds = dispatcher.GetAllHwnds();

                // Вычисление самого "верхнего" окна процессов
                IntPtr hWnd = WindowsApi.GetTopmostHwnd(hwnds);
                if (hWnd == IntPtr.Zero)
                {
                    return null;
                }

                NativeMethods.GetWindowThreadProcessId(hWnd, out int id);

                if (id <= 0)
                {
                    return null;
                }

                Process process;

                try
                {
                    process = Process.GetProcessById(id);
                }
                catch (Exception)
                {
                    return null;
                }

                PID = process.Id;
                obj = dispatcher.GetComObjectByProcessId(process.Id);

                return obj ?? null;
            }
        }

        public static int PID { get; private set; }

        /// <summary>
        /// Процедура правильного удаления COM объекта
        /// </summary>
        public static void DisposeFinally(object obj)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (obj != null)
            {
                try
                {
                    Marshal.FinalReleaseComObject(obj);
                }
                finally { obj = null; }
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public static void Dispose(object obj)
        {
            if (obj == null)
                return;
            try
            {
                Marshal.ReleaseComObject(obj);
            }
            finally
            {
                obj = null;
                GC.Collect();
            }
        }
    }
    #endregion

}
