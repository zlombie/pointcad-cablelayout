﻿using Layout.Area;
using Layout.Configuration;
using Layout.Enums;
using Layout.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Layout
{
    public class EItemOnLayoutAdvanced : ePathSegmentItem
    {
        public string SheetName { get; set; }
        public string DirectionType { get; set; }
        public double Length { get; set; }
        public double Diameter { get; set; }
        public int CoreCount { get; set; }
        public List<ELine> Lines { get; set; }
        public string LayingWithDirection { get { return Laying != null ? $"{Laying.Name} {DirectionType}" : $"{eConstants.NullString} {DirectionType}"; } }
        public string HeightForPath
        {
            get
            {
                string res = "-";
                if (DirectionType == "→" && Height != eConstants.BadHeight)
                {
                    res = Height.ToString();
                }
                return res;
            }
        }
        public int SortNumber { get; set; }
        public List<EMaterialComponent> NonLayingMaterials { get; private set; }
        public int Sides { get; private set; }
        public EItemOnLayoutAdvanced(int id) : base(id)
        {
            DirectionType = "→";
            Lines = new List<ELine>();
            if (e3c.segm.SetId(id) > 0)
            {
                Diameter = e3c.segm.GetOuterDiameter();
                Length = e3c.segm.GetSchemaLength();
                CoreCount = e3c.segm.GetCoreCount();
            }
            else if (e3c.sym.SetId(id) > 0)
            {
                Diameter = 0;
                Length = 0;
            }
        }

        public void GetSectionMaterials()
        {
            e3c.segm.SetId(ID);
            NonLayingMaterials = new List<EMaterialComponent>();
            if (Laying != null)
            {
                switch (Laying.SectionType)
                {
                    case "Металлоконструкции":
                        {
                            dynamic attIds = null;
                            int ductAttrsCount = e3c.segm.GetAttributeIds(ref attIds, eSettings.Profile.DuctParametersAttribute);
                            if (ductAttrsCount == 0)
                            {
                                ConstructionParams = string.Empty;
                                return;
                            }
                            ParseConstructionAttribute(ConstructionParams);
                            if (ConstructionParams != string.Empty)
                            {
                                for (int i = 1; i <= ductAttrsCount; i++)
                                {
                                    e3c.att.SetId(attIds[i]);
                                    string ductCode = e3c.att.GetInternalValue();
                                    var splStr = ductCode.Split('$', ':');
                                    if (splStr.Length > 1)
                                    {
                                        var duct = eSettings.Profile.DuctTypes.FirstOrDefault(f => splStr[1] == $"Короб {f.Width}x{f.Height}");
                                        if (duct != null && duct.Materials.Count > 0)
                                        {
                                            NonLayingMaterials.AddRange(duct.Materials);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //e3.app.PutError
                            }
                            break;
                        }
                    case "Траншея":
                        {
                            string trenchName = e3c.segm.GetAttributeValue(eSettings.Profile.TrenchParametersAttribute);
                            if (string.IsNullOrEmpty(trenchName))
                            {
                                //e3.app.PutError
                            }
                            else
                            {
                                var trench = eSettings.Profile.TrenchTypes.FirstOrDefault(f => trenchName == f.Name);
                                if (trench != null && trench.Materials.Count > 0)
                                {
                                    NonLayingMaterials.AddRange(trench.Materials);
                                }
                            }

                        }
                        break;
                    default:
                        break;
                }

            }
        }

        private void ParseConstructionAttribute(string code)
        {
            try
            {
                var spl = code.Split('#');
                if (spl.Length == 4)
                {
                    Sides = int.TryParse(spl[1], out int s) ? s : 1;
                    var construction = eSettings.Profile.MetalconstructionTypes.FirstOrDefault(f => f.Name == spl[0]);
                    if (construction != null && construction.Materials.Count > 0)
                    {
                        NonLayingMaterials.AddRange(construction.Materials);
                    }
                    int.TryParse(spl[2].Replace(" мм", ""), out int w);
                    var shelf = eSettings.Profile.ShelfTypes.FirstOrDefault(f => f.Width == w);
                    if (shelf != null && shelf.Materials.Count > 0)
                    {
                        NonLayingMaterials.AddRange(shelf.Materials);
                        //*Sides
                    }
                    int.TryParse(spl[3], out int c);
                    var stand = eSettings.Profile.StandTypes.FirstOrDefault(f => f.ShelfCount == c);
                    if (stand != null && stand.Materials.Count > 0)
                    {
                        NonLayingMaterials.AddRange(stand.Materials);
                        //*Sides
                    }
                }
                else
                {
                    ConstructionParams = string.Empty;
                    //e3c.app.PutError(0, $"Не заданы параметры разреза для участка цепи ID {ID}", ID);
                }
            }
            catch (Exception ex)
            {
                ConstructionParams = string.Empty;
                //e3c.app.PutError(0, $"Не заданы параметры разреза для участка цепи ID {ID}", ID);
            }
        }
    }
}