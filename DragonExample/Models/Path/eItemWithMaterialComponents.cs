﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace Layout
{
    [Serializable]
    [Magic]
    public class eItemWithMaterialComponents : INotifyPropertyChanged
    {
        public string Name { get; set; }

        private ObservableCollection<EMaterialComponent> _materials;
        public ObservableCollection<EMaterialComponent> Materials
        {
            get
            {
                {
                    if (_materials == null)
                        _materials = new ObservableCollection<EMaterialComponent>();
                    return _materials;
                }
            }
            set
            {
                if (_materials != value)
                {
                    _materials = value;
                }
            }
        }


        public string MaterialsCount { get { return Materials != null ? $"{Materials.Count} ед." : "0 ед."; } }
        public void RaisePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
