﻿using Layout.Area;
using Layout.Configuration;
using Layout.Service;
using Pointcad.Dragon.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;


namespace Layout.Main
{
    class ePathConstructor
    {
        public ePathConstructor(ProgressHandler progress)
        {
            ProgressEvent += progress;
            e3c.InitE3Objects();
            CheckTree();
            SetColors();
            Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
            Utilites.CurrentSheet.GetVerticalSegments();
            e3c.DisposeE3Job();
        }

        private void CheckTree()
        {
            //Проверяем дерево
            if (!string.IsNullOrEmpty(eSettings.Profile.TreeName))
            {
                dynamic treeIds = null;
                int treeCount = e3c.job.GetTreeIds(ref treeIds);
                bool isCreated = false;
                for (int i = 1; i <= treeCount; i++)
                {
                    e3c.tree.SetId(treeIds[i]);
                    if (e3c.tree.GetName() == eSettings.Profile.TreeName)
                    {
                        isCreated = true;
                    }
                }
                if (!isCreated)
                {
                    var res = MessageWindow.ShowDialog("Не найдено структурное дерево изделий, неразмещенных на планах расположения. Создать дерево?", "", "Планы расположения", icon: MessageWindowImages.Question, button: MessageWindowButtons.YesNo);
                    if (res == MessageDialogResults.Yes)
                    {
                        e3c.tree.Create(eSettings.Profile.TreeName);
                        dynamic arr = new object[6, 4];
                        arr[1, 0] = 1;
                        arr[1, 1] = "";
                        arr[1, 2] = "";
                        arr[1, 3] = 0;

                        arr[2, 0] = 2;
                        arr[2, 1] = "";
                        arr[2, 2] = "";
                        arr[2, 3] = 2;

                        arr[3, 0] = 0;
                        arr[3, 1] = "";
                        arr[3, 2] = "";
                        e3c.tree.SetVisibleObjectTypes(ref arr);
                        e3c.tree.SetVisibleInfoTypes(eSettings.Profile.DevNumber.ToString(), eSettings.Profile.SchemeNumber.ToString());
                    }
                }
            }
        }
        private static void SetColors()
        {
            var bl = eSettings.Profile.СolorGoodNumber;
            Color color = new Color();
            object indexGood = 200, indexBad = 201, r, g, b;
            if (eSettings.Profile.СolorGoodNumber != null)
            {
                color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.СolorGoodNumber);
                r = color.R; g = color.G; b = color.B;
                e3c.job.SetRGBValue(ref indexGood, ref r, ref g, ref b);
            }
            if (eSettings.Profile.СolorBadNumber != null)
            {
                color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.СolorBadNumber);
                r = color.R; g = color.G; b = color.B;
                e3c.job.SetRGBValue(ref indexBad, ref r, ref g, ref b);
            }
            indexGood = 203;
            indexBad = 204;
            if (eSettings.Profile.StreamNewColor != null)
            {
                color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.StreamNewColor);
                r = color.R; g = color.G; b = color.B;
                e3c.job.SetRGBValue(ref indexGood, ref r, ref g, ref b);
            }
            if (eSettings.Profile.StreamOldColor != null)
            {
                color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.StreamOldColor);
                r = color.R; g = color.G; b = color.B;
                e3c.job.SetRGBValue(ref indexBad, ref r, ref g, ref b);
            }
        }

        private event ProgressHandler ProgressEvent;
        public List<Cable> Cables = new List<Cable>();
        public List<EItemOnLayoutAdvanced> SelectedSegments = new List<EItemOnLayoutAdvanced>();
        public List<EItemOnLayoutAdvanced> SelectedDevices = new List<EItemOnLayoutAdvanced>();
        public List<EItemOnLayoutAdvanced> SelectedSymbols = new List<EItemOnLayoutAdvanced>();



        public double StockPercent = 6;
        public bool ColorsChanged = false;
        List<E3Line> ConLines = new List<E3Line>();







        #region Раскладка кабелей

        public void SetLine(LayingType linetype)
        {
            ClearLineStyle();
            if (linetype!=null && linetype.Name!=eConstants.NullString)
            {
                e3c.InitE3Objects();
                if (linetype.InteractivePlace)
                {
                    e3c.sym.Load(linetype.HorizontalSymbol, "1");
                    int id = e3c.sym.PlaceInteractively();
                    foreach (var selSegm in SelectedSegments)
                    {
                        e3c.segm.SetId(selSegm.ID);
                        e3c.segm.SetAttributeValue(eSettings.Profile.LayingTypeAttribute, linetype.Name);
                        e3c.segm.SetLineWidth(eSettings.Profile.LineWidth);
                    }
                    e3c.DisposeE3Job();
                    return;
                }
                if (linetype.Delta <= 0)
                {
                    linetype.Delta = 1;
                }
                foreach (var selSegm in SelectedSegments)
                {
                    e3c.segm.SetId(selSegm.ID);
                    e3c.segm.SetAttributeValue(eSettings.Profile.LayingTypeAttribute, linetype.Name);
                    e3c.segm.SetLineWidth(eSettings.Profile.LineWidth);
                    SetLineStyle(linetype, ref ConLines);
                }
                foreach (var selSym in SelectedSymbols)
                {
                    e3c.sym.SetId(selSym.ID);
                    e3c.sym.SetAttributeValue(eSettings.Profile.LayingSymAttribute, linetype.Name);
                }
                e3c.DisposeE3Job();
            }
        }

        private void ClearLineStyle()
        {
            try
            {
                e3c.InitE3Objects();
                foreach (var selSegm in SelectedSegments)
                {
                    e3c.segm.SetId(selSegm.ID);
                    e3c.segm.DeleteAttribute(eSettings.Profile.LayingTypeAttribute);
                    dynamic symIds = null;
                    int symCnt = e3c.segm.GetSymbolIds(ref symIds);
                    for (int j = 1; j <= symCnt; j++)
                    {
                        e3c.sym.SetId(symIds[j]);
                        string symName = e3c.sym.GetSymbolTypeName();
                        if (eSettings.Profile.LayingTypes.FirstOrDefault(f => (f.HorizontalSymbol == symName || f.VerticalSymbol == symName)) != null)
                        {
                            e3c.sym.Delete();
                        }
                    }
                }
                foreach (var selSym in SelectedSymbols)
                {
                    e3c.sym.SetId(selSym.ID);
                    e3c.sym.DeleteAttribute(eSettings.Profile.LayingSymAttribute);
                }
            }
            finally { e3c.DisposeE3Job(); }
        }

        public void BreakSegment(bool saveLaying, string symbol, bool saveHeight)
        {
            e3c.InitE3Objects();
            e3c.sym.Load(eSettings.Profile.ChangeTypeInteractiveSymbol, "1");
            int res = e3c.sym.PlaceInteractively();
            if (res > 0)
            {
                object x = null, y = null, grid = null;
                e3c.sym.GetSchemaLocation(ref x, ref y, ref grid);
                string rot = e3c.sym.GetRotation();
                e3c.sym.Delete();
                e3c.sym.Load(symbol, "1");
                e3c.sym.Place(Utilites.CurrentSheet.ID, (double)x, (double)y, rot);
                dynamic pinIds = null;
                int pinCount = e3c.sym.GetPinIds(ref pinIds);
                for (int i = 1; i <= pinCount; i++)
                {
                    e3c.core.SetId(pinIds[i]);
                    dynamic segmIds = null;
                    int segmCount = e3c.core.GetNetSegmentIds(ref segmIds);
                    string height = "";
                    string laying = "";
                    for (int j = 1; j <= segmCount; j++)
                    {
                        e3c.segm.SetId(segmIds[j]);
                        height = e3c.segm.GetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute);
                        laying = e3c.segm.GetAttributeValue(eSettings.Profile.LayingTypeAttribute);
                        if (!string.IsNullOrEmpty(height) && !string.IsNullOrEmpty(laying))
                            break;
                    }
                    for (int j = 1; j <= segmCount; j++)
                    {
                        e3c.segm.SetId(segmIds[j]);
                        if (saveHeight)
                            e3c.segm.SetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute, height);
                        if (saveLaying)
                        {
                            e3c.segm.SetAttributeValue(eSettings.Profile.LayingTypeAttribute, laying);
                        }
                        else
                        {
                            laying = e3c.segm.GetAttributeValue(eSettings.Profile.LayingTypeAttribute);
                            if (string.IsNullOrEmpty(laying))
                            {
                                dynamic symIds = null;
                                int symCnt = e3c.segm.GetSymbolIds(ref symIds);
                                for (int k = 1; k <= symCnt; k++)
                                {
                                    e3c.sym.SetId(symIds[k]);
                                    string symName = e3c.sym.GetSymbolTypeName();
                                    if (eSettings.Profile.LayingTypes.FirstOrDefault(f => (f.HorizontalSymbol == symName || f.VerticalSymbol == symName)) != null)
                                    {
                                        e3c.sym.Delete();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Utilites.CurrentSheet.GetHorizontalSegments();
            //e3c.DisposeE3Job();
        }

        private void SetLineStyle(LayingType linetype, ref List<E3Line> linesList, double x1 = 0, double y1 = 0, double x2 = 0, double y2 = 0)
        {
            if (!string.IsNullOrEmpty(linetype.Name)) linesList.Clear();
            dynamic xarr = null;
            dynamic yarr = null;
            int linesegmCnt = e3c.segm.GetLineSegments(Utilites.CurrentSheet, ref xarr, ref yarr);
            for (int i = 1; i < linesegmCnt; i++)
            {
                E3Line ln = new E3Line();
                ln.IsVertical = Utilites.GetCoordinates(xarr[i], xarr[i + 1], yarr[i], yarr[i + 1], out ln.x1, out ln.x2, out ln.y1, out ln.y2);
                linesList.Add(ln);
            }

            linesList = linesList.OrderBy(o => o.x1).ThenBy(o => o.y1).ToList();

            if (!string.IsNullOrEmpty(linetype.Name))
            {
                int loadResult = e3c.sym.Load(linetype.VerticalSymbol, "1");
                if (loadResult < 1)
                    return;
                object xmin = null, ymin = null, xmax = null, ymax = null;
                e3c.sym.GetArea(ref xmin, ref ymin, ref xmax, ref ymax);
                double symW = (double)ymax - (double)ymin;
                double symH = (double)xmax - (double)xmin;
                e3c.sym.Delete();
                double k1 = 0;
                double k2 = linetype.Delta;
                int firstLineIndex = 0;
                int lastLineIndex = linesList.Count - 1;
                if (x1 != 0)
                {
                    firstLineIndex = linesList.FindIndex(f => (f.x1 == x1 && y1 > f.y1 && y1 < f.y2) || (f.y1 == y1 && x1 > f.x1 && x1 < f.x2));
                    lastLineIndex = linesList.FindLastIndex(f => f.x2 == x2 || f.y2 == y2);
                }

                if (firstLineIndex == -1) { firstLineIndex = 0; }
                if (lastLineIndex == -1) { lastLineIndex = linesList.Count - 1; }

                for (int k = firstLineIndex;
                    k <= lastLineIndex; k++)
                {
                    var conln = linesList[k];
                    if (conln.IsVertical)
                    {
                        double dy = conln.y1 + symW;
                        double maxy = conln.y2;

                        if (y1 != 0 && k == firstLineIndex) { dy = y1; }
                        if (y2 != 0 && k == lastLineIndex) { maxy = y2; }

                        while (dy + symW * k1 < maxy)
                        {
                            int res = e3c.sym.Load(linetype.VerticalSymbol, "CCCC");
                            int res1 = e3c.sym.Place(Utilites.CurrentSheet.ID, conln.x1, dy);
                            dy += symW * k2;
                        }
                    }

                    if (!conln.IsVertical)
                    {
                        if (conln.y1 == conln.y2)
                        {
                            double dx = conln.x1 + symW;
                            double maxx = conln.x2;
                            if (x1 != 0 && k == firstLineIndex) { dx = x1; }
                            if (x2 != 0 && k == lastLineIndex) { maxx = x2; }
                            while (dx + symW * k1 < maxx)
                            {
                                int res = e3c.sym.Load(linetype.HorizontalSymbol, "CCCC");
                                int res1 = e3c.sym.Place(Utilites.CurrentSheet.ID, dx, conln.y1);
                                dx += symW * k2;
                            }

                        }
                        else
                        {
                            // int l = 1;
                            // int ky = 1;
                            double dx = 0;
                            double dy = 0;

                            //if (y1 != 0 && k == firstLineIndex) { dy = y1; }
                            //if (y2 != 0 && k == lastLineIndex) { maxy = y2; }

                            //Монотонно возрастающая функция
                            if ((conln.x2 > conln.x1 && conln.y2 > conln.y1) || (conln.x1 > conln.x2 && conln.y1 > conln.y2))
                            {
                                GetCathetus(ref dx, ref dy, symW, 1, conln);
                                dx += Math.Min(conln.x1, conln.x2);
                                dy += Math.Min(conln.y1, conln.y2);
                                while (dx + symW * k1 * Math.Sqrt(1 / (1 + Math.Pow(Math.Abs(conln.y2 - conln.y1) / Math.Abs(conln.x2 - conln.x1), 2))) < Math.Max(conln.x2, conln.x1))
                                {
                                    int res = e3c.sym.Load(linetype.HorizontalSymbol, "1");
                                    int res1 = e3c.sym.Place(Utilites.CurrentSheet.ID, dx, dy);
                                    GetCathetus(ref dx, ref dy, symW * k2, 1, conln);
                                }
                            }
                            else
                            {
                                GetCathetus(ref dx, ref dy, symW * 1.5, -1, conln);
                                dx += Math.Min(conln.x1, conln.x2);
                                dy += Math.Max(conln.y1, conln.y2);
                                while (dx + symW * k1 * Math.Sqrt(1 / (1 + Math.Pow(Math.Abs(conln.y2 - conln.y1) / Math.Abs(conln.x2 - conln.x1), 2))) < Math.Max(conln.x2, conln.x1))
                                {
                                    int res = e3c.sym.Load(linetype.HorizontalSymbol, "1");
                                    int res1 = e3c.sym.Place(Utilites.CurrentSheet.ID, dx, dy);
                                    GetCathetus(ref dx, ref dy, symW * k2, -1, conln);
                                }
                            }

                        }

                    }

                }
            }
        }

        private void GetCathetus(ref double dx, ref double dy, double l, int ky, E3Line conln)
        {
            dx += l * Math.Sqrt(1 / (1 + Math.Pow((conln.y2 - conln.y1) / (conln.x2 - conln.x1), 2)));
            dy += ky * l * Math.Sqrt(1 / (1 + Math.Pow((conln.x2 - conln.x1) / (conln.y2 - conln.y1), 2)));
        }

        public void ReadSelectedSegments()
        {
            e3c.InitE3Objects(false);
            Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
            dynamic segmIds = null;
            int segmCnt = e3c.job.GetSelectedNetSegmentIds(ref segmIds);
            SelectedSegments.Clear();
            for (int i = 1; i <= segmCnt; i++)
            {
                SelectedSegments.Add(new EItemOnLayoutAdvanced(segmIds[i]));
            }
        }

        public void ReadSelectedDevices()
        {
            dynamic devIds = null;
            int devCnt = e3c.job.GetSelectedDeviceIds(ref devIds);
            SelectedDevices.Clear();
            for (int i = 1; i <= devCnt; i++)
            {
                e3c.dev.SetId(devIds[i]);
                if (e3c.dev.IsView() > 0)
                {
                    e3c.dev.SetId(e3c.dev.GetOriginalId());
                    SelectedDevices.Add(new EItemOnLayoutAdvanced(e3c.dev.GetId()));
                }
            }
        }

        public void ReadSelectedSymbols()
        {
            dynamic symIds = null;
            int symCnt = e3c.job.GetSelectedSymbolIds(ref symIds);
            SelectedSymbols.Clear();
            for (int i = 1; i <= symCnt; i++)
            {
                e3c.sym.SetId(symIds[i]);
                string symName = e3c.sym.GetSymbolTypeName();
                if (symName == eSettings.Profile.DownSymbol || symName == eSettings.Profile.UpSymbol || symName == eSettings.Profile.NodeSymbol || e3c.dev.SetId(symIds[i]) > 0)
                {
                    var newSym = new EItemOnLayoutAdvanced(symIds[i]);
                    SelectedSymbols.Add(newSym);
                }
            }
            e3c.DisposeE3Job();
        }

        #endregion

        #region Высотные отметки


        // Задание высоты участка трассы
        public bool SetHeight(string height)
        {
            bool res = true;
            double val = Utilites.ParseHeight(height);
            if (height == "" || val != eConstants.BadHeight)
            {
                e3c.InitE3Objects();
                foreach (var selSegm in SelectedSegments)
                {
                    e3c.segm.SetId(selSegm.ID);
                    e3c.segm.SetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute, height);
                }
                foreach (var selDev in SelectedDevices)
                {
                    e3c.dev.SetId(selDev.ID);
                    e3c.dev.SetAttributeValue(eSettings.Profile.DeviceHeightAttribute, height);
                }
                e3c.job.ResetHighlight();
                e3c.DisposeE3Job();
            }
            else
            {
                res = false;
            }
            return res;
        }

        public void PlaceTemplate()
        {
            e3c.InitE3Objects();
            e3c.sym.Load(eSettings.Profile.HeightTemplateSymbol, "1");
            e3c.sym.PlaceInteractively();
            e3c.DisposeE3Job();
        }

        // Выделить цветом участки трассы
        public void HighLightSegments(bool resetToDefault, string attributeName = "", string goodColor = "", string badColor = "")
        {
            e3c.InitE3Objects();
            e3c.sheet.SetId(Utilites.CurrentSheet.ID);
            dynamic segmIds = null;
            int segmCount = e3c.sheet.GetNetSegmentIds(ref segmIds);
            for (int i = 1; i <= segmCount; i++)
            {
                e3c.segm.SetId(segmIds[i]);
                if (resetToDefault == false)
                {
                    ColorsChanged = true;
                    var bl = eSettings.Profile.СolorGoodNumber;
                    if (!string.IsNullOrEmpty(goodColor) && !string.IsNullOrEmpty(badColor))
                    {
                        Color color = (Color)ColorConverter.ConvertFromString(goodColor);
                        object indexGood = 200, indexBad = 201,
                               r = color.R, g = color.G, b = color.B;
                        e3c.job.SetRGBValue(ref indexGood, ref r, ref g, ref b);

                        color = (Color)ColorConverter.ConvertFromString(badColor);
                        r = color.R; g = color.G; b = color.B;
                        e3c.job.SetRGBValue(ref indexBad, ref r, ref g, ref b);

                        var col = e3c.segm.GetLineColour();
                        if (col != (int)indexGood && col != (int)indexBad)
                            e3c.segm.SetAttributeValue(eSettings.Profile.ColorAttribute, col.ToString());

                        string val = e3c.segm.GetAttributeValue(attributeName);
                        if (string.IsNullOrEmpty(val))
                            e3c.segm.SetLineColour((int)indexBad);
                        else
                        {
                            e3c.segm.SetLineColour((int)indexGood);
                        }
                    }
                }
                else
                {
                    ColorsChanged = false;
                    if (int.TryParse(e3c.segm.GetAttributeValue(eSettings.Profile.ColorAttribute), out int oldCol))
                        e3c.segm.SetLineColour(oldCol);
                }
            }
            e3c.DisposeE3Job();
        }


        #endregion

        #region Проверка изображений
        //Проверка изображений
        public string CheckViews()
        {
            e3c.InitE3Objects(false);
            int newViews = 0;
            dynamic devIds = null;
            int devCount = e3c.job.GetAllDeviceIds(ref devIds);
            for (int i = 1; i <= devCount; i++)
            {
                e3c.dev.SetId(devIds[i]);
                //ProgressEvent(i, devCount, string.Format("Проверка номеров изображений изделий: {0} из {1}", i, devCount));
                int viewCount = e3c.dev.GetViewCount();
                if (viewCount > 0)
                {
                    dynamic viewIds = null;
                    viewCount = e3c.dev.GetViewIds(ref viewIds);
                    bool viewExist = false;
                    for (int j = 1; j <= viewCount; j++)
                    {
                        e3c.dev.SetId(viewIds[j]);
                        if (e3c.dev.GetViewNumber() == eSettings.Profile.DevNumber)
                        {
                            viewExist = true;
                            break;
                        }
                    }
                    if (!viewExist)
                    {
                        int r = e3c.dev.CreateView(devIds[i], eSettings.Profile.DevNumber, 0, eSettings.Profile.DevNumber.ToString());

                        if (r > 0)
                        {
                            newViews++;
                            e3c.app.PutInfo(0, string.Format("Изображение добавлено для {0} {1}", e3c.dev.GetComponentName(), e3c.dev.GetName()));
                        }
                        else
                        {
                            e3c.app.PutWarning(0, string.Format("Невозможно создать изображение для {0} {1}", e3c.dev.GetComponentName(), e3c.dev.GetName()));
                        }
                    }
                }
            }
            e3c.DisposeE3Job();
            return string.Format("Создано отсутствующий изображений: {0}", newViews);
        }
        #endregion

        #region Расчет длин кабелей

        public string ReadAllCables()
        {
            List<string> errorMessage = new List<string>();
            e3c.InitE3Objects();
            string St = e3c.job.GetAttributeValue(eSettings.Profile.StockAttribute);
            if (string.IsNullOrEmpty(St) || !double.TryParse(St, out StockPercent))
            {
                e3c.DisposeE3Job();
                return "Некорректный коэффициент запаса кабелей!";
            }
            Cables.Clear();
            dynamic symIds = null;
            dynamic cabIds = null;
            int symCnt = e3c.job.GetSymbolIds(ref symIds);
            int cabCnt = e3c.job.GetCableIds(ref cabIds);
            if (cabCnt == 0)
            {
                e3c.DisposeE3Job();
                return "В проекте отсутствуют кабели!";
            }
            List<int> nodeList = new List<int>();
            List<eSheet> layoutSheets = new List<eSheet>();
            for (int i = 1; i <= cabCnt; i++)
            {
                ProgressEvent(i, cabCnt, string.Format("Считывание кабелей из проекта: {0} из {1}", i, cabCnt));
                bool onLayout = false;
                var newCable = new Cable(cabIds[i]);
                if (string.IsNullOrEmpty(newCable.Type))
                    continue;
                if (double.TryParse(e3c.dev.GetAttributeValue(eSettings.Profile.LengthAttribute), out double dblTemp))
                    newCable.FinalLength = dblTemp;
                if (double.TryParse(e3c.dev.GetAttributeValue(eSettings.Profile.AdditionalLengthAttribute), out dblTemp))
                    newCable.Reserve = dblTemp;
                dynamic coreIds = null;
                int coreCnt = e3c.dev.GetAllCoreIds(ref coreIds);
                for (int coreIndex = 1; coreIndex <= coreCnt; coreIndex++)
                {
                    e3c.core.SetId(coreIds[coreIndex]);
                    dynamic viewIds = null;

                    int pinID1 = e3c.core.GetEndPinId(1);
                    int pinID2 = e3c.core.GetEndPinId(2);

                    if (pinID1 > 0 && pinID2 > 0)
                    {
                        newCable.CoreId = e3c.core.GetId();
                        e3c.core.SetId(pinID1);
                        pinID1 = 0;
                        int viewCount = e3c.core.GetViewIds(ref viewIds);
                        for (int v = 1; v <= viewCount; v++)
                        {
                            e3c.dev.SetId(viewIds[v]);
                            int viewNumber1 = e3c.dev.GetViewNumber();
                            if (viewNumber1 == eSettings.Profile.DevNumber)
                            {
                                pinID1 = viewIds[v];
                                break;
                            }
                        }
                        e3c.core.SetId(pinID2);
                        pinID2 = 0;
                        viewCount = e3c.core.GetViewIds(ref viewIds);
                        for (int v = 1; v <= viewCount; v++)
                        {
                            e3c.dev.SetId(viewIds[v]);
                            int viewNumber2 = e3c.dev.GetViewNumber();
                            if (viewNumber2 == eSettings.Profile.DevNumber)
                            {
                                pinID2 = viewIds[v];
                                break;
                            }
                        }
                        dynamic segmIds = null;
                        int netSegmCnt = e3c.core.GetNetSegmentPath(ref segmIds, pinID1, pinID2);
                        double length = 0;
                        //по сегментам
                        for (int j = 1; j <= netSegmCnt; j++)
                        {
                            EItemOnLayoutAdvanced newSegment = new EItemOnLayoutAdvanced(segmIds[j]);
                            object tmpshtid = null;
                            dynamic xarr = null;
                            dynamic yarr = null;
                            int linesegmCnt = e3c.segm.GetLineSegments(ref tmpshtid, ref xarr, ref yarr);
                            int sheetId = (int)tmpshtid;
                            eSheet currentSheet = layoutSheets.Find(f => f.ID == sheetId);
                            if (currentSheet == null)
                            {
                                currentSheet = new eSheet(sheetId);
                                currentSheet.GetFields();
                                currentSheet.GetVerticalSegments();
                                layoutSheets.Add(currentSheet);
                            }
                            newSegment.SheetName = currentSheet.Name;
                            double scale = currentSheet.Scale;
                            eField currentField = currentSheet.Fields.Find(f => f.InsidePathSegmentsIds.Contains(segmIds[j]));
                            if (currentField!=null)
                            {
                                scale = currentField.Scale;
                            }
                            length += newSegment.Length * scale;
                            newSegment.Length = newSegment.Length * scale;
                            onLayout = true;
                            newCable.NetSegments.Add(newSegment);

                            //Узлы трассы
                            dynamic nodeIds = null;
                            int nodeCount = e3c.segm.GetNodeIds(ref nodeIds);
                            for (int k = 1; k <= nodeCount; k++)
                            {
                                e3c.core.SetId(nodeIds[k]);
                                dynamic tempSegmIds = null;
                                int slaveSegmCount = e3c.core.GetNetSegmentIds(ref tempSegmIds);
                                if (slaveSegmCount > 2 && !nodeList.Contains(nodeIds[k]))
                                {
                                    nodeList.Add(nodeIds[k]);
                                    object x = null, y = null, gr = null;
                                    e3c.core.GetSchemaLocation(ref x, ref y, ref gr);
                                    e3c.sym.Load(eSettings.Profile.NodeSymbol, "1");
                                    e3c.sym.Place((int)tmpshtid, (double)x, (double)y);
                                }
                            }
                        }
                        newCable.PinID1 = pinID1;
                        newCable.PinID2 = pinID2;
                        newCable.Length = Math.Round(length, 1);
                        if (onLayout)
                            foreach (var c in eSettings.Profile.LayingTypes)
                            {
                                newCable.LengthsWithType.Add(new LengthWithType { Attr = c.LengthAttribute, Length = 0, Name = c.Name });
                            }
                        Cables.Add(newCable);
                        break;
                    }
                }
            }
            //colors
            var bl = eSettings.Profile.СolorGoodNumber;
            Color color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.СolorGoodNumber);
            object indexGood = 200, indexBad = 201, r = color.R, g = color.G, b = color.B;
            e3c.job.SetRGBValue(ref indexGood, ref r, ref g, ref b);
            color = (Color)ColorConverter.ConvertFromString(eSettings.Profile.СolorBadNumber);

            foreach (var c in Cables)
            {
                double heightDelta = 0;
                List<int> cableNodesList = new List<int>();
                int sortNumber = 1;
                List<EItemOnLayoutAdvanced> boundarySegments = new List<EItemOnLayoutAdvanced>();
                for (int i = 0; i < c.NetSegments.Count; i++)
                {
                    if (c.NetSegments[i].SortNumber == 0)
                    {
                        c.NetSegments[i].SortNumber = sortNumber;
                        sortNumber++;
                    }
                    if (c.NetSegments[i].Height == eConstants.BadHeight)
                    {
                        string er = $"{c.NetSegments[i].ID} (лист {c.NetSegments[i].SheetName})";
                        if (!errorMessage.Contains(er))
                        {
                            errorMessage.Add(er);
                            e3c.segm.SetId(c.NetSegments[i].ID);
                            e3c.app.PutError(0, $"Не задана высотная отметка для участка трассы с ID={c.NetSegments[i].ID}", c.NetSegments[i].ID);
                            r = color.R; g = color.G; b = color.B;
                            e3c.job.SetRGBValue(ref indexBad, ref r, ref g, ref b);
                            var col = e3c.segm.GetLineColour();
                            if (col != (int)indexGood && col != (int)indexBad)
                            {
                                e3c.segm.SetAttributeValue(eSettings.Profile.ColorAttribute, col.ToString());
                                e3c.segm.SetLineColour((int)indexBad);
                            }
                        }
                    }
                    if (i == 0)
                    {
                        e3c.dev.SetId(c.PinID1);
                        if (e3c.dev.IsView() > 0)
                            e3c.dev.SetId(e3c.dev.GetOriginalId());
                        double h = Utilites.ParseHeight(e3c.dev.GetAttributeValue(eSettings.Profile.DeviceHeightAttribute));
                        if (h != eConstants.BadHeight)
                        {
                            e3c.sym.SetId(c.PinID1);
                            var firstSegment = new EItemOnLayoutAdvanced(e3c.sym.GetId());
                            firstSegment.Length = Math.Abs(c.NetSegments[i].Height - h);
                            firstSegment.SortNumber = 0;
                            if (firstSegment.Laying == null)
                                firstSegment.Laying = c.NetSegments[i].Laying;
                            firstSegment.DirectionType = GetDirection(h, c.NetSegments[i].Height);
                            heightDelta = firstSegment.Length;
                            boundarySegments.Add(firstSegment);
                        }
                    }
                    else
                    {
                        var hd = c.NetSegments[i - 1].Height - c.NetSegments[i].Height;
                        string direction = GetDirection(c.NetSegments[i - 1].Height, c.NetSegments[i].Height);
                        heightDelta = Math.Abs(hd);
                        dynamic nodeIds = null;
                        e3c.segm.SetId(c.NetSegments[i - 1].ID);
                        int nodeCount = e3c.segm.GetNodeIds(ref nodeIds);
                        for (int k = 1; k <= nodeCount; k++)
                        {
                            e3c.core.SetId(nodeIds[k]);
                            dynamic tempSegmIds = null;
                            int slaveSegmCount = e3c.core.GetNetSegmentIds(ref tempSegmIds);
                            if (slaveSegmCount > 1)
                            {
                                int symID = e3c.sym.SetId(nodeIds[k]);
                                string symName = e3c.sym.GetSymbolTypeName();
                                if (!cableNodesList.Contains(symID) && heightDelta > 0 && (symName == eSettings.Profile.UpSymbol || symName == eSettings.Profile.DownSymbol || symName == eSettings.Profile.NodeSymbol))
                                {
                                    string laying = e3c.sym.GetAttributeValue(eSettings.Profile.LayingSymAttribute);
                                    var tempItem = new EItemOnLayoutAdvanced(symID)
                                    {
                                        Length = heightDelta,
                                        DirectionType = direction,
                                        SheetName = e3c.sheet.GetName(),
                                        Height = c.NetSegments.Last().Height,
                                        SortNumber = sortNumber - 1
                                    };
                                    c.NetSegments.Add(tempItem);
                                    c.NetSegments[i].SortNumber = sortNumber;
                                }
                                cableNodesList.Add(symID);
                            }
                        }
                    }
                    c.Length += heightDelta;
                }
                e3c.dev.SetId(c.PinID2);
                if (e3c.dev.IsView() > 0)
                    e3c.dev.SetId(e3c.dev.GetOriginalId());
                double hh = Utilites.ParseHeight(e3c.dev.GetAttributeValue(eSettings.Profile.DeviceHeightAttribute));
                if (hh != eConstants.BadHeight)
                {
                    e3c.sym.SetId(c.PinID2);
                    var lastSegment = new EItemOnLayoutAdvanced(e3c.sym.GetId());
                    lastSegment.Length = Math.Abs(c.NetSegments.Last().Height - hh);
                    lastSegment.SortNumber = c.NetSegments.Count + 5;
                    if (lastSegment.Laying == null)
                        lastSegment.Laying = c.NetSegments.Last().Laying;
                    lastSegment.DirectionType = GetDirection(c.NetSegments.Last().Height, hh);
                    boundarySegments.Add(lastSegment);
                    c.Length += Math.Abs(lastSegment.Length);
                }
                c.NetSegments.AddRange(boundarySegments);
                c.NetSegments = c.NetSegments.OrderBy(o => o.SortNumber).ToList();
            }
            Cables = Cables.OrderBy(o => o.Name).ToList();
            e3c.DisposeE3Job();
            if (errorMessage.Count > 0 || layoutSheets.Any(a=>!string.IsNullOrEmpty(a.ScaleStatusMessage)))
            {
                StringBuilder msg = new StringBuilder();
                if (errorMessage.Count > 0)
                    msg.AppendLine($"Не заданы высотные отметки для участков трасс: {string.Join(", ", errorMessage)}");
                if (layoutSheets.Any(a => !string.IsNullOrEmpty(a.ScaleStatusMessage)))
                    msg.AppendLine($"Невозможно рассчитать длины кабелей. Обнаружены следующие ошибки: {string.Join(", ", layoutSheets.Select(s=>s.ScaleStatusMessage).ToArray())}");
                Cables.Clear();
                return msg.ToString();
            }
            return string.Empty;
        }

        public string WriteAllCables()
        {
            e3c.InitE3Objects();
            string message = "";
            double dblStock = StockPercent / 100;
            double sp = 1 + dblStock;
            int counter = 0;
            string errorMessage = "";
            e3c.app.PutInfo(0, "Расчет длин кабелей для различных способов прокладки...");
            foreach (var c in Cables)
            {
                ProgressEvent(counter + 1, Cables.Count + 1, string.Format("Передача кабелей в проект: {0} из {1}", counter + 1, Cables.Count + 1));
                double l = c.Length;
                if (eSettings.Profile.UseAdditionalLength)
                {
                    l += c.Reserve;
                }
                int number = (int)Math.Ceiling((l) * sp);
                foreach (var s in c.NetSegments)
                {
                    if (s.Laying != null)
                    {
                        int index = c.LengthsWithType.FindIndex(f => f.Name == s.Laying.Name);
                        if (c.LengthsWithType.Count == 0 || index == -1)
                            c.LengthsWithType.Add(new LengthWithType { Length = s.Length, Attr = s.Laying.LengthAttribute });
                        else
                            c.LengthsWithType[index].Length += s.Length;
                    }
                    else
                    {
                        string er = string.Format("{1}, (лист {0}), ", s.SheetName, s.ID.ToString());
                        if (!errorMessage.Contains(er))
                        {
                            errorMessage += er;
                            e3c.segm.SetId(s.ID);
                            e3c.app.PutWarning(0, "Не задан способ прокладки для участка трассы с ID=" + er.TrimEnd(' ').TrimEnd(','), s.ID);
                            var col = e3c.segm.GetLineColour();
                            //var bl = Profile.Instance.СolorGoodNumber;
                            //if (col != Profile.Instance.СolorBadNumber && col != Profile.Instance.СolorGoodNumber)
                            //{
                            //    e3c.segm.SetAttributeValue(Profile.Instance.ColorAttribute, col.ToString());
                            //    e3c.segm.SetLineColour(Profile.Instance.СolorBadNumber);
                            //}
                        }
                    }
                }

                c.FinalLength = number;
                c.SetLayingLengths();
                e3c.dev.SetId(c.ID);
                e3c.dev.SetAttributeValue(eSettings.Profile.LengthAttribute, c.FinalLength.ToString());
                e3c.dev.SetAttributeValue(eSettings.Profile.AdditionalLengthAttribute, c.Reserve.ToString());
                var grpByAttr = c.LengthsWithType.GroupBy(g => g.Attr).Select(s => s.ToList());
                foreach (var grp in grpByAttr)
                {
                    double summary = grp.Sum(s => s.Length);
                    e3c.dev.SetAttributeValue(grp.First().Attr, summary.ToString());
                }
                counter++;
            }
            if (!string.IsNullOrEmpty(errorMessage))
            {
                message = "Не заданы способы прокладки для участков трасс: " + errorMessage.TrimEnd(' ').TrimEnd(',') + "\nВозможен некорректный расчет длин кабелей для различных способов прокладки. Подробности см. в окне сообщений.";
            }
            e3c.app.PutInfo(0, "Расчет завершен");
            e3c.DisposeE3Job();
            return message;
        }

        public void JumpToID(int number)
        {
            e3c.InitE3Objects();
            var sel = Cables[number].CoreId;
            e3c.job.JumpToID(sel);
            e3c.DisposeE3Job();
        }

        private string GetDirection(double current, double next)
        {
            var hd = next - current;
            string direction = "↑";
            if (hd < 0)
                direction = "↓";
            return direction;
        }
        #endregion
    }

}
