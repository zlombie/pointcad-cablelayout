﻿using Layout.Area;
using Layout.Configuration;
using Layout.Service;
using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;


namespace Layout
{
    public delegate void ProgressHandler(int cnt, int max, string text = "");
    public delegate void StatusTextHandler(string text = "");
    internal class MagicAttribute : Attribute { }
    internal class NoMagicAttribute : Attribute { }

    static class Utilites
    {


        internal const string RussianAlphabet = "ЫАБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЭЮЯ";

        internal static string TempFile;
        internal static ePathSegmentItem SelectedItem { get; set; }
        internal static eSheet CurrentSheet { get; set; }

        //удаление старых элементов
        internal static void DeleteOldItems(string delAttributeVal,  string delAttributeName, bool deleteGraphic, bool deleteSymbols, bool deleteSegments)
        {
            if (!string.IsNullOrEmpty(delAttributeVal))
            {
                dynamic symIds = new object();
                int symCnt;
                e3c.sheet.SetId(CurrentSheet.ID);

                dynamic groupIds = null;
                int grCount = e3c.sheet.GetGroupIds(ref groupIds);
                for (int i = 1; i <= grCount; i++)
                {
                    e3c.group.SetId(groupIds[i]);
                    string g = e3c.group.GetAttributeValue(delAttributeName);
                    if (!string.IsNullOrEmpty(g) && g.StartsWith(delAttributeVal))
                    {
                        e3c.group.DeleteContents();
                    }
                }

                if (deleteGraphic)
                {
                    symCnt = e3c.sheet.GetGraphIds(ref symIds);
                    for (int i = 1; i <= symCnt; i++)
                    {
                        e3c.graph.SetId(symIds[i]);
                        string g = e3c.graph.GetAttributeValue(delAttributeName);
                        if (!string.IsNullOrEmpty(g) && g.StartsWith(delAttributeVal))
                        {
                            e3c.graph.Delete();
                        }
                    }
                }

                if (deleteSymbols)
                {
                    symCnt = e3c.sheet.GetSymbolIds(ref symIds);
                    for (int i = 1; i <= symCnt; i++)
                    {
                        e3c.sym.SetId(symIds[i]);
                        string g = e3c.sym.GetAttributeValue(delAttributeName);
                        if (!string.IsNullOrEmpty(g) && g.StartsWith(delAttributeVal))
                        {
                            e3c.sym.Delete();
                        }
                    }
                }

                if (deleteSegments)
                {
                    symCnt = e3c.sheet.GetNetSegmentIds(ref symIds);
                    for (int i = 1; i <= symCnt; i++)
                    {
                        e3c.segm.SetId(symIds[i]);
                        string g = e3c.segm.GetAttributeValue(delAttributeName);
                        if (!string.IsNullOrEmpty(g) && g.StartsWith(delAttributeVal))
                        {
                            e3c.segm.Delete();
                        }
                    }
                }
            }
        }

        internal static void DeleteSegmentAttributes(string attName)
        {
            dynamic attIds = null;
            int attCount = e3c.segm.GetAttributeIds(ref attIds, attName);
            for (int j = 1; j <= attCount; j++)
            {
                e3c.att.SetId(attIds[j]);
                e3c.att.Delete();
            }
        }

        internal static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }

        //Парсинг значения атрибута
        internal static double ParseHeight(string height)
        {
            var regex = new Regex(@"^[-+]?\d+\.?\d*");
            var regexBad = new Regex(@"[^0-9.,+-]");
            double res = eConstants.BadHeight;
            if (!string.IsNullOrEmpty(height))
            {
                if (regexBad.IsMatch(height))
                    return res;
                Match m = regex.Match(height);
                if (m != null)
                {
                    string strValue = m.Value.Replace(".", ",");
                    if (double.TryParse(strValue, out double dblValue))
                    {
                        res = dblValue;
                    }
                }
            }
            return res;
        }

        internal static double GetLineLenght(double x1, double x2, double y1, double y2)
        {
            return (Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)));
        }


        //Высчитывание координат
        internal static bool GetCoordinates(double xarr1, double xarr2, double yarr1, double yarr2, out double x1, out double x2, out double y1, out double y2)
        {
            if (Math.Abs(xarr1 - xarr2) < 0.001)
            {
                if (yarr1 < yarr2)
                {
                    x1 = xarr1;
                    y1 = yarr1;
                    x2 = xarr2;
                    y2 = yarr2;
                }
                else
                {
                    x2 = xarr1;
                    y2 = yarr1;
                    x1 = xarr2;
                    y1 = yarr2;
                }
                return (true);
            }
            else
            {
                if (xarr1 < xarr2)
                {
                    x1 = xarr1;
                    y1 = yarr1;
                    x2 = xarr2;
                    y2 = yarr2;
                }
                else
                {
                    x2 = xarr1;
                    y2 = yarr1;
                    x1 = xarr2;
                    y1 = yarr2;
                }
                return (false);
            }
        }

    }
}
