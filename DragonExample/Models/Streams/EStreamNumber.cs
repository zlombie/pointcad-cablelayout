﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Streams
{
    class EStreamNumber
    {
        public int NoteID { get; set; }
        public int Number { get; set; }
        public string GID { get; set; }
    }
}
