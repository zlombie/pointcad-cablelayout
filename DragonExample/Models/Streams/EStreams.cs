﻿using CableLayout.Configuration;
using CableLayout.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace CableLayout.Streams
{
    
    class EStreams
    {
        #region Кабельные потоки

        public  event ProgressHandler ProgressEvent;

        public List<EStreamCable> Cables = new List<EStreamCable>();

        public  int RefreshCount { get; set; }
        public  int ColumnsCount { get; set; }
        public  bool AltNote { get; set; }

         List<string> _oldStreamCables = new List<string>();
         List<int> _groupIds = new List<int>();

         double _selectedSymbolX = 0;
         int _selectedSegmId = 0;

        public EStreams()
        {

        }

        //Чтение потоков
        public void ReadStreams(bool disconnect = true)
        {
            e3c.InitE3Objects();
            Utilites.SelectedItem = new EItemOnLayout(0, false);
            Utilites.CurrentSheetID = e3c.job.GetActiveSheetId();
            e3c.sheet.SetId(Utilites.CurrentSheetID);
            Cables.Clear();
            dynamic selectedIds = null;
            int selSegm = e3c.job.GetSelectedNetSegmentIds(ref selectedIds);
            List<int> cablesList = new List<int>();
            if (selSegm == 1)
            {
                Utilites.SelectedItem = new EItemOnLayout(selectedIds[1], true);
                cablesList = GetCables(selectedIds[1]);
            }
            else
            {
                int symCount = e3c.job.GetSelectedSymbolIds(ref selectedIds);
                if (symCount > 0)
                {
                    Utilites.SelectedItem = new EItemOnLayout(selectedIds[1], false);
                    cablesList = GetCables(selectedIds[1]);
                }
                else
                {
                    e3c.DisposeE3ComObjects(e3c.job);
                    return;
                }
            }
            Cables = GetStreamInformation(cablesList);
            if (disconnect)
                e3c.DisposeE3ComObjects(e3c.job);
        }

        private List<int> GetCables(int itemID)
        {
            List<int> cableList = new List<int>();
            dynamic coreIds = null;
            if (e3c.segm.SetId(itemID) > 0)
            {
                int coreCount = e3c.segm.GetCoreIds(ref coreIds);
                for (int j = 1; j <= coreCount; j++)
                {
                    e3c.dev.SetId(coreIds[j]);
                    cableList.Add(e3c.dev.GetId());
                }
            }
            else if (e3c.sym.SetId(itemID) > 0)
            {
                dynamic pinIds = null;
                int pinCount = e3c.sym.GetPinIds(ref pinIds);
                for (int i = 1; i <= pinCount; i++)
                {
                    e3c.core.SetId(pinIds[i]);
                    int coreCount = e3c.core.GetCoreIds(ref coreIds);
                    for (int j = 1; j <= coreCount; j++)
                    {
                        e3c.dev.SetId(coreIds[j]);
                        if((e3c.dev.IsCable() > 0) || (e3c.dev.IsTube() > 0 && Utilites.CurrentProfile.ReadTubes))
                            cableList.Add(e3c.dev.GetId());
                    }
                }
            }
            if (cableList.Count > 0)
                return cableList.Distinct().ToList();
            return cableList;
        }

        private List<EStreamCable> GetStreamInformation(List<int> cableIds)
        {
            var tempList = new List<EStreamCable>();
            for (int i = 0; i < cableIds.Count; i++)
            {
                e3c.dev.SetId(cableIds[i]);
                var cbl = new EStreamCable();
                if (tempList.Find(f => f.ID == cbl.ID) != null)
                    continue;
                tempList.Add(cbl);
            }
            tempList = tempList.OrderBy(o => o.Type).ThenBy(o => Utilites.PadNumbers(o.Name)).ToList();
            return tempList;
        }

        public  void RefreshStreams(bool onlySelectedSegment)
        {
            RefreshCount = 0;
            e3c.InitE3Objects();
            Utilites.CurrentSheetID = e3c.job.GetActiveSheetId();
            e3c.sheet.SetId(Utilites.CurrentSheetID);

            string segmGID = "";
            if (onlySelectedSegment)
            {
                dynamic selectedsegmIds = null;
                int segmCount = e3c.job.GetSelectedNetSegmentIds(ref selectedsegmIds);
                if (segmCount == 1)
                {
                    segmGID = e3c.job.GetGidOfId(selectedsegmIds[1]);
                }
                else if (e3c.job.GetSelectedSymbolIds(ref selectedsegmIds) == 1)
                {
                    segmGID = e3c.job.GetGidOfId(selectedsegmIds[1]);
                }
                else
                {
                    //MetroFramework.MetroMessageBox.Show(Application.OpenForms[0], "Не обнаружено выделенных участков трассы!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e3c.DisposeE3ComObjects(e3c.job);
                        return;
                }

                dynamic grIds = null;
                var selGroups = e3c.job.GetSelectedGraphIds(ref grIds);
                if (selGroups > 0)
                {
                    if (e3c.group.SetId(grIds[1]) > 0)
                    {
                        e3c.group.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, segmGID);
                        e3c.app.PutWarning(0, "Обнаружен выделенный поток и участок трассы/символ. Выполняется переназначение принадлежности потока!");
                    }
                }
                else
                {
                    selGroups = e3c.job.GetSelectedTextIds(ref grIds);
                    if (selGroups > 0)
                    {
                        if (e3c.group.SetId(grIds[1]) > 0)
                        {
                            e3c.group.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, segmGID);
                            e3c.app.PutWarning(0, "Обнаружен выделенный поток и участок трассы/символ. Выполняется переназначение принадлежности потока!");
                        }
                    }
                }
            }

            dynamic groupIds = null;
            int groupCount = e3c.sheet.GetGroupIds(ref groupIds);
            bool founded = false;
            for (int i = 1; i <= groupCount; i++)
            {
                if (segmGID == "")
                    ProgressEvent(i, groupCount, string.Format("Обновление сгруппированных таблиц потоков: {0} из {1}", i, groupCount));
                e3c.group.SetId(groupIds[i]);
                string gid = e3c.group.GetAttributeValue(Utilites.CurrentProfile.GidAttribute);
                if (!string.IsNullOrEmpty(Utilites.CurrentProfile.GidAttribute))
                {
                    if (!string.IsNullOrEmpty(segmGID))
                    {
                        if (gid != segmGID)
                            continue;
                    }
                    string note = e3c.group.GetAttributeValue(Utilites.CurrentProfile.GraphAttribute);
                    if (!string.IsNullOrEmpty(note))
                    {
                        int segmID = e3c.job.GetIdOfGid(gid);
                        //Кол-во столбцов
                        string strColCount = note.Last().ToString();
                        bool res = int.TryParse(strColCount, out int colCount);
                        if (!res)
                            colCount = 2;
                        ColumnsCount = colCount;
                        if (note.StartsWith("*"))
                            addLine = true;
                        else
                            addLine = false;
                        List<int> cableList = new List<int>();
                        if (e3c.segm.SetId(segmID) > 0)
                        {
                            _selectedSegmId = segmID;
                            dynamic symIds = null;
                            int symCount = e3c.segm.GetSymbolIds(ref symIds);
                            for (int j = 1; j <= symCount; j++)
                            {
                                e3c.sym.SetId(symIds[j]);
                                string g = e3c.sym.GetAttributeValue(Utilites.CurrentProfile.GidAttribute);
                                if (e3c.job.GetIdOfGid(g) == _selectedSegmId)
                                {
                                    object x = null, y = null, grid = null;
                                    e3c.sym.GetSchemaLocation(ref x, ref y, ref grid);
                                    _selectedSymbolX = (double)x;
                                }
                            }
                            Cables.Clear();
                            dynamic coreIds = null;
                            int coreCount = e3c.segm.GetCoreIds(ref coreIds);
                            for (int j = 1; j <= coreCount; j++)
                            {
                                e3c.dev.SetId(coreIds[j]);
                                if ((e3c.dev.IsCable() > 0) || (e3c.dev.IsTube() > 0 && Utilites.CurrentProfile.ReadTubes))
                                {
                                    int id = e3c.dev.GetId();
                                    if (!cableList.Contains(id))
                                    {
                                        cableList.Add(id);
                                    }
                                }
                            }
                        }
                        else if (e3c.sym.SetId(segmID) > 0)
                        {
                            Cables.Clear();
                            _selectedSegmId = segmID;
                            object x = null, y = null, grid = null;
                            e3c.sym.GetSchemaLocation(ref x, ref y, ref grid);
                            _selectedSymbolX = (double)x;
                            e3c.dev.SetId(segmID);
                            int originalID = e3c.dev.GetOriginalId();
                            e3c.dev.SetId(originalID);
                            dynamic pinIds = null;
                            int pinCount = e3c.dev.GetPinIds(ref pinIds);
                            for (int j = 1; j <= pinCount; j++)
                            {
                                e3c.core.SetId(pinIds[j]);
                                dynamic ids = null;
                                int coreCount = e3c.core.GetCoreIds(ref ids);
                                for (int k = 1; k <= coreCount; k++)
                                {
                                    e3c.dev.SetId(ids[k]);
                                    if ((e3c.dev.IsCable() > 0) || (e3c.dev.IsTube() > 0 && Utilites.CurrentProfile.ReadTubes))
                                    {
                                        int id = e3c.dev.GetId();
                                        if (!cableList.Contains(id))
                                            cableList.Add(id);
                                    }
                                }
                            }
                        }
                        if (cableList.Count > 0)
                        {
                            dynamic itemIds = null;
                            int itemCount = e3c.group.GetItems(ref itemIds);
                            string streamString = "";
                            List<Point> Coordinates = new List<Point>();
                            _oldStreamCables.Clear();
                            for (int j = 1; j <= itemCount; j++)
                            {
                                e3c.graph.SetId(itemIds[j]);
                                if (e3c.graph.GetType() == 3)
                                {
                                    object x1 = null, x2 = null, y1 = null, y2 = null;
                                    e3c.graph.GetRectangle(ref x1, ref y1, ref x2, ref y2);
                                    e3c.graph.SetColour(204);
                                    int x = Math.Min(Convert.ToInt16(x1), Convert.ToInt16(x2));
                                    int y = Math.Max(Convert.ToInt16(y2), Convert.ToInt16(y1));
                                    var newPoint = new Point(x, y);
                                    Coordinates.Add(newPoint);
                                }
                                else if (e3c.graph.GetType() == 8)
                                {
                                    string text = e3c.graph.GetText();
                                    if (!string.IsNullOrEmpty(text))
                                    {
                                        if (text.StartsWith("Поток"))
                                            streamString = text;
                                        else
                                        {
                                            _oldStreamCables.Add(text);
                                        }
                                    }
                                }
                            }
                            double maxY = Coordinates.Max(m => m.Y);
                            double maxX = Coordinates.Min(m => m.X);

                            GetStreamInformation(cableList);
                            WriteStream(Cables, true, !string.IsNullOrEmpty(streamString), maxX, maxY, 0, 0, streamString);
                            RefreshCount++;
                            founded = true;
                        }
                        if (!founded)
                        {
                            dynamic itemIds = null;
                            int itemCount = e3c.group.GetItems(ref itemIds);
                            if (itemCount>0)
                            {
                                e3c.app.PutWarning(0, "Обнаружена непривязанная таблица кабельных потоков. Для перехода кликните 2 раза на это сообщение", itemIds[1]);
                            }
                        }
                    }
                }
            }
            GetStreamNumber(Utilites.CurrentProfile.NoteSymbol, Utilites.CurrentProfile.StreamAttribute, ' ', true);
            e3c.DisposeE3ComObjects(e3c.job);
        }

        private ESymbolBase GetNoteSymbol()
        {
            e3c.sym.SetId(0);
            var tempSym = new ESymbolBase();
            dynamic symIds = null;
            int symCount = e3c.segm.GetSymbolIds(ref symIds);
            for (int k = 1; k <= symCount; k++)
            {
                e3c.sym.SetId(symIds[k]);
                tempSym = new ESymbolBase();
                if (tempSym.Name == Utilites.CurrentProfile.StreamNoteTemplate)
                {
                    return tempSym;
                }
            }
            e3c.app.PutError(0, "Не удалось обнаружить символ привязки потока!");
            return tempSym;
        }

        bool addLine = false;

        //Отрисовка потока
        public void WriteStream(List<EStreamCable> Cables, bool isUpdate, bool outside = false, double startX = 0, double startY = 0, double anchorX=0, double anchorY = 0, string streamString = "", string prefix = "", bool deleteOldItems = true)
        {
            if (Cables.Count == 0)
                return;
            if (Cables.Last().ID != 0)
                Cables.Add(new EStreamCable { ID = 0, Type = "абырвалг", Name = "" });
            _groupIds.Clear();

            if (!isUpdate && anchorX==0)
                e3c.InitE3Objects();
            string symName = Utilites.CurrentProfile.NoteSymbol;
            int currentColor = e3c.job.GetGraphTextColour();
            string attrstr = prefix + e3c.job.GetGidOfId(_selectedSegmId);
            int streamNumb = 0;
            if (isUpdate)
            {
                if (deleteOldItems)
                    Utilites.DeleteOldItems(attrstr, true, false, false);
            }
            else
            {
                addLine = Utilites.CurrentProfile.StreamAddLine;
                if (deleteOldItems)
                    Utilites.DeleteOldItems(attrstr, true, true, false);
            }
            if (startX == 0)
                streamNumb = GetStreamNumber(Utilites.CurrentProfile.NoteSymbol, Utilites.CurrentProfile.StreamAttribute, ' ', false);

            //Символ точки привязки потока
            ESymbolBase anchorSym = new ESymbolBase();
            if (e3c.sym.SetId(_selectedSegmId) > 0) //изделие
            {
                anchorSym = new ESymbolBase();
            }
            else if (anchorX==0) //участок трассы
            {
                if (isUpdate)
                    anchorSym = GetNoteSymbol();
                else
                {
                    e3c.sym.Load(Utilites.CurrentProfile.StreamNoteTemplate, "1");
                    e3c.sym.PlaceInteractively();
                    anchorSym = new ESymbolBase();
                    e3c.sym.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
                }
            }
            
            //Если поток с выноской
            if (outside == true)
            {
                //задаем предыдущий назначенный символ как точку 1
                var masterSym = new ESymbolBase();
                if (!isUpdate)
                {
                    anchorSym = PutNote(symName, Utilites.CurrentProfile.StreamAttribute, attrstr, "Поток " + streamNumb.ToString(), Utilites.CurrentProfile.GidAttribute);
                }
                else
                {
                    dynamic symIds = null;
                    int symCount = e3c.sheet.GetSymbolIds(ref symIds);
                    for(int i=1; i<=symCount; i++)
                    {
                        e3c.sym.SetId(symIds[i]);
                        if (e3c.sym.GetSymbolTypeName()== Utilites.CurrentProfile.NoteSymbol && e3c.sym.GetAttributeValue(Utilites.CurrentProfile.GidAttribute) == attrstr)
                        {
                            anchorSym = new ESymbolBase();
                            break;
                        }
                    }
                }

                if (Math.Abs(masterSym.X-anchorSym.Left)< Math.Abs(masterSym.X - anchorSym.Right))
                    e3c.graph.CreateLine(Utilites.CurrentSheetID, masterSym.X, masterSym.Y, anchorSym.Left, anchorSym.Y);
                else
                    e3c.graph.CreateLine(Utilites.CurrentSheetID, masterSym.X, masterSym.Y, anchorSym.Right, anchorSym.Y);
                e3c.graph.SetLineColour(currentColor);
                e3c.graph.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
            }


            e3c.sym1.Load(Utilites.CurrentProfile.StreamStartSymbol, "1");
            if (startX == 0 && startY == 0)
            {
                int placeRes = e3c.sym1.PlaceInteractively();
                if (placeRes == 0)
                {
                    e3c.app.PutError(0, "Действие отменено!");
                    return;
                }
            }
            else
            {
                e3c.sym1.Place(Utilites.CurrentSheetID, startX, startY);
            }

            object fx = null, fy = null, gr = null;
            e3c.sym1.GetSchemaLocation(ref fx, ref fy, ref gr);
            double l = Utilites.CurrentProfile.StreamTypeWidth;
            double dy = (double)fy;
            double dx = (double)fx;
            double oldY = (double)fy;
            double currentX = dx + l - Utilites.CurrentProfile.StreamCableWidth;
            double maxX = currentX + (ColumnsCount - 1) * Utilites.CurrentProfile.StreamCableWidth;
            Point leftTop = new Point { X = dx, Y = dy };
            Point rightTop = new Point { X = dx+Utilites.CurrentProfile.StreamCableWidth, Y = dy };
            Point leftBottom = new Point { X = dx, Y = dy };
            Point rightBottom = new Point { X = dx, Y = dy };

            if (anchorX > 0)
                anchorSym.X = anchorX;
            if (anchorY > 0)
                anchorSym.Y = anchorY;

            e3c.sym1.Delete();
            if (outside)
            {
                string str = streamString;
                if (streamNumb > 0)
                    str = "Поток " + streamNumb.ToString();
                CreateRectWithText(Utilites.CurrentSheetID, dx - 2, dy + 5, dx + Utilites.CurrentProfile.StreamTypeWidth, oldY, attrstr, str, color:currentColor, deleteGraphic:true);
                e3c.txt.SetHyperlinkAddress("gid:" + anchorSym.GID);
            }
            string oldtype = Cables[0].Type;
            //Полка
            if (addLine && !outside)
            {
                e3c.graph.CreateLine(Utilites.CurrentSheetID, dx, oldY, dx +  Utilites.CurrentProfile.StreamCableWidth + Utilites.CurrentProfile.StreamTypeWidth, oldY);
                e3c.graph.SetLineColour(currentColor);
                e3c.graph.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
                if (Math.Abs(anchorSym.X - leftTop.X) < Math.Abs(anchorSym.X - rightTop.X))
                {
                    e3c.graph.CreateLine(Utilites.CurrentSheetID, dx, oldY - Utilites.CurrentProfile.StreamCableHeight, dx, oldY);
                }
                else
                {
                    rightTop.X = dx + Utilites.CurrentProfile.StreamTypeWidth + Utilites.CurrentProfile.StreamCableWidth;
                    e3c.graph.CreateLine(Utilites.CurrentSheetID, rightTop.X, oldY - Utilites.CurrentProfile.StreamCableHeight, rightTop.X, oldY);
                }
                e3c.graph.SetLineColour(currentColor);
                e3c.graph.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
                oldY -= Utilites.CurrentProfile.StreamCableHeight;
                dy -= Utilites.CurrentProfile.StreamCableHeight;
            }
            for (int i = 0; i < Cables.Count; i++)
            {
                var c = Cables[i];
                dynamic txtIds = new object();
                if (c.Type != oldtype)
                {
                    currentX = dx + l;
                    dy -= Utilites.CurrentProfile.StreamCableHeight;
                    if (Utilites.CurrentProfile.StreamTypeWidth > 0)
                    {
                        CreateRectWithText(Utilites.CurrentSheetID, dx, dy, dx + Utilites.CurrentProfile.StreamTypeWidth, oldY, attrstr, oldtype, 2, color: currentColor);
                    }
                    oldY = dy;
                    e3c.txt.SetHyperlinkAddress("gid:" + anchorSym.GID);
                }
                else
                {
                    if (currentX <= maxX)
                        currentX += Utilites.CurrentProfile.StreamCableWidth;
                    else
                    {
                        currentX = dx + l;
                        dy -= Utilites.CurrentProfile.StreamCableHeight;
                    }
                }
                if (i == Cables.Count - 1) break;
                int textColor = currentColor;
                if (startX > 0 && isUpdate && !_oldStreamCables.Contains(c.Name))
                {
                    textColor = 203;
                }
                CreateRectWithText(Utilites.CurrentSheetID, currentX, dy, currentX + Utilites.CurrentProfile.StreamCableWidth, dy - Utilites.CurrentProfile.StreamCableHeight, attrstr, c.Name, 2, color: textColor);
                if (rightTop.Y == dy && currentX + Utilites.CurrentProfile.StreamCableWidth >= rightTop.X)
                {
                    rightTop.X = currentX + Utilites.CurrentProfile.StreamCableWidth;
                }
                if (leftBottom.X == currentX && dy - Utilites.CurrentProfile.StreamCableHeight<=leftBottom.Y)
                {
                    leftBottom.Y = dy - Utilites.CurrentProfile.StreamCableHeight;
                }
                if (dy - Utilites.CurrentProfile.StreamCableHeight <= rightBottom.Y)
                {
                    rightBottom.Y = dy - Utilites.CurrentProfile.StreamCableHeight;
                    rightBottom.X = currentX + Utilites.CurrentProfile.StreamCableWidth;
                }             
                oldtype = c.Type;
            }

            dy += 5;

            //Линия-выноска до таблицы
            if (!outside)
            {
                if (Math.Abs(anchorSym.X - leftTop.X) < Math.Abs(anchorSym.X - rightTop.X))
                {
                    if (Math.Abs(anchorSym.Y - leftTop.Y) < Math.Abs(anchorSym.Y - leftBottom.Y))
                        e3c.graph.CreateLine(Utilites.CurrentSheetID, leftTop.X, leftTop.Y, anchorSym.X, anchorSym.Y);
                    else
                        e3c.graph.CreateLine(Utilites.CurrentSheetID, leftBottom.X, leftBottom.Y, anchorSym.X, anchorSym.Y);
                }
                else
                {
                    if (Math.Abs(anchorSym.Y - rightTop.Y) < Math.Abs(anchorSym.Y - rightBottom.Y))
                        e3c.graph.CreateLine(Utilites.CurrentSheetID, rightTop.X, rightTop.Y, anchorSym.X, anchorSym.Y);
                    else
                        e3c.graph.CreateLine(Utilites.CurrentSheetID, rightBottom.X, rightBottom.Y, anchorSym.X, anchorSym.Y);
                }
                e3c.graph.SetLineColour(currentColor);
                e3c.graph.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
                //_groupIds.Add(e3c.graph.GetId());
            }

            //Гиперссылка на выноске
            //int noteId = e3c.job.GetIdOfGid(noteSym.GID);
            //e3c.sym.SetId(noteId);
            //dynamic noteTextIds = new object();
            //int noteTextCount = e3c.sym.GetTextIds(ref noteTextIds);
            //string symTypeGID = e3c.job.GetGidOfId(_groupIds.First());
            //for (int i = 1; i <= noteTextCount; i++)
            //{
            //    e3c.txt.SetId(noteTextIds[i]);
            //    e3c.txt.SetHyperlinkAddress("gid:" + symTypeGID);
            //}

            dynamic graIds = new object[_groupIds.Count + 1];
            for (int i = 0; i < _groupIds.Count; i++)
                graIds[i + 1] = _groupIds[i];

            e3c.group.Create(graIds);
            e3c.group.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, attrstr);
            string note = "";
            if (Utilites.CurrentProfile.StreamAddLine)
                note = "*";
            note += fx.ToString() + "|" + fy.ToString() + "|" + ColumnsCount.ToString();
            e3c.group.SetAttributeValue(Utilites.CurrentProfile.GraphAttribute, note);
            if (!isUpdate && anchorX == 0)
            {
                e3c.DisposeE3ComObjects(e3c.job);
               // SendKeys.Send("{F5}");
            }
        }

        public void DeleteStream()
        {
            e3c.InitE3Objects();
            string g = e3c.job.GetGidOfId(_selectedSegmId);
            Utilites.DeleteOldItems(g, true, true, true);
            GetStreamNumber(Utilites.CurrentProfile.NoteSymbol, Utilites.CurrentProfile.StreamAttribute, ' ', true);
            e3c.DisposeE3ComObjects(e3c.job);
        }

        //Прямоугольник с текстом
        public void CreateRectWithText(int sheetID, double xl, double yl, double xr, double yr, string noteValue, string text = "", int alignment = 1, bool deleteGraphic = false, int level = 5, int color = 0)
        {
            if (!deleteGraphic)
            {
                _groupIds.Add(e3c.graph.CreateRectangle(sheetID, xl, yl, xr, yr));
                e3c.graph.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, noteValue);
                e3c.graph.SetColour(color);
                if (Utilites.CurrentProfile.Fill)
                {
                    e3c.graph.SetHatchColour(19);
                    e3c.graph.SetHatchPattern(1, 0, 0);
                }
                //e3Com.graph.SetLevel(level);
            }
            double txtX = (xl + xr) / 2;
            double txtY = (yl + yr) / 2 - 1.5;
            if (alignment != 2) txtX = xl + 2;

            int txtid = e3c.graph.CreateText(sheetID, text, txtX, txtY);
            e3c.txt.SetId(txtid);
            double w = e3c.txt.GetWidth();
            if (w > Math.Abs(xl - xr))
            {
                //e3c.txt.SetBox(Math.Abs(xl - xr)-2, Math.Abs(yl - yr));
                //e3c.txt.SetSchemaLocation(txtX, yl - e3c.txt.GetHeight());
                e3c.txt.SetMode(2);
            }
            _groupIds.Add(txtid);
            e3c.txt.SetColour(color);
            e3c.txt.SetAlignment(alignment);
            e3c.txt.SetAttributeValue(Utilites.CurrentProfile.GidAttribute, noteValue);
            //e3Com.txt.SetLevel(level);
        }

        //Установка выноски
        protected ESymbolBase PutNote(string symbolName, string gidAttribute, string gidValue, string noteAttribute, string noteValue)
        {
            e3c.sym.Load(symbolName, "1");
            e3c.sym.PlaceInteractively();
            e3c.sym.SetAttributeValue(noteAttribute, noteValue);
            e3c.sym.SetAttributeValue(gidAttribute, gidValue);
            return new ESymbolBase();
        }

        //Получение номера потока
        protected int GetStreamNumber(string symTypeName, string attrName, char splitter, bool updateNumbers)
        {
            List<EStreamNumber> numbers = new List<EStreamNumber>();
            List<EGroup> groups = new List<EGroup>();

            e3c.sheet.SetId(Utilites.CurrentSheetID);
            if (updateNumbers)
            {
                dynamic groupIds = null;
                int groupCount = e3c.sheet.GetGroupIds(ref groupIds);
                for (int i = 1; i <= groupCount; i++)
                {
                    e3c.group.SetId(groupIds[i]);
                    string gid = e3c.group.GetAttributeValue(Utilites.CurrentProfile.GidAttribute);
                    if (!string.IsNullOrEmpty(gid))
                    {
                        dynamic itemIds = null;
                        int itemCount = e3c.group.GetItems(ref itemIds);
                        for (int j = 1; j <= itemCount; j++)
                        {
                            if (e3c.txt.SetId(itemIds[j]) > 0)
                            {
                                string t = e3c.txt.GetText();
                                if (!string.IsNullOrEmpty(t) && t.StartsWith("Поток"))
                                {
                                    groups.Add(new EGroup { TxtID = itemIds[j], GID = gid });
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            symTypeName = symTypeName.ToLower();
            dynamic symIds = null;
            int symCnt = e3c.sheet.GetSymbolIds(ref symIds);
            for (int i = 1; i <= symCnt; i++)
            {
                e3c.sym.SetId(symIds[i]);
                string symtypename = e3c.sym.GetSymbolTypeName().ToLower();
                if (!symtypename.StartsWith(symTypeName)) continue;
                string number = "";
                if (splitter != 'n')
                {
                    try
                    {
                        number = e3c.sym.GetAttributeValue(attrName).Split(splitter)[1];
                    }
                    catch { }
                }
                else
                    number = e3c.sym.GetAttributeValue(attrName);
                int nmb;
                if (int.TryParse(number, out nmb))
                {

                    string gid = e3c.sym.GetAttributeValue(Utilites.CurrentProfile.GidAttribute);
                    dynamic txtIds = null;
                    int txtCount = e3c.sym.GetTextIds(ref txtIds);
                    if(txtCount>0)
                    {
                        numbers.Add(new EStreamNumber { Number = nmb, NoteID = symIds[i], GID = gid, TxtID=txtIds[1] });
                    }
                }
            }
            numbers = numbers.OrderBy(o => o.Number).ToList();
            if (updateNumbers)
            {
                for (int i = 0; i < numbers.Count; i++)
                {
                    e3c.sym.SetId(numbers[i].NoteID);
                    string val = string.Format("Поток {0}", i+1);
                    e3c.sym.SetAttributeValue(Utilites.CurrentProfile.StreamAttribute, val);
                    e3c.txt.SetId(numbers[i].TxtID);
                    e3c.txt.SetText(val);
                    var fndGr = groups.FindAll(f => f.GID == numbers[i].GID);
                    if (fndGr!=null)
                    {
                        foreach(var f in fndGr)
                        {
                            e3c.txt.SetId(f.TxtID);
                            e3c.txt.SetText(val);
                        }
                    }
                }
                return 0;
            }
            else
            {
                if (numbers.Count == 0 || numbers.First().Number > 1) return 1;
                else
                {
                    for (int i = 1; i < numbers.Count; i++)
                    {
                        if (numbers[i].Number != numbers[i - 1].Number + 1) return numbers[i - 1].Number + 1;
                    }
                    return numbers[numbers.Count - 1].Number + 1;
                }
            }

        }


        protected int GetSectionNumber(string symName, string attrName)
        {
            int res = 1;
            dynamic symIds = null;
            int symCount = e3c.sheet.GetSymbolIds(ref symIds);
            List<int> numbers = new List<int>();
            for (int i = 1; i <= symCount; i++)
            {
                e3c.sym.SetId(symIds[i]);
                if (e3c.sym.GetSymbolTypeName() == symName)
                {
                    string attr = e3c.sym.GetAttributeValue(attrName);
                    int num = 0;
                    if (int.TryParse(attr, out num))
                    {
                        numbers.Add(num);
                    }
                }
            }
            res = Enumerable.Range(1, 1000).Except(numbers).FirstOrDefault();
            return res;
        }

        #endregion


    }
}
