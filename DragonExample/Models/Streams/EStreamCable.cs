﻿using Layout.Configuration;

namespace Layout.Streams
{
    public class EStreamCable : CableBase
    {

        public double X { get; set; }
        public double Y { get; set; }
        public string Class { get; set; }
        public string Tag { get; set; }
        public int OuterDiameter { get; set; }
        public int BetweenCables { get; set; }

        public EStreamCable()
        {
            ID = e3c.dev.GetId();
            Name = e3c.dev.GetName();
            Type = e3c.dev.GetAttributeValue(eSettings.Profile.SignalAttribute);
            OuterDiameter = (int)e3c.dev.GetOuterDiameter();
        }

        internal void GetCableClass()
        {
            Tag = "";
            e3c.dev.SetId(ID);
            foreach (var ct in eSettings.Profile.CableTypes)
            {
                if (e3c.dev.GetComponentAttributeValue(ct.Attribute) == ct.Value)
                {
                    Class = ct.Name;
                    BetweenCables = ct.BetweenCables;
                    break;
                }
                else
                {
                    Class = "<Нет данных>";
                    BetweenCables = 0;
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
