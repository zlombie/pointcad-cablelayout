﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Layout.Service;
using Layout.Configuration;

namespace Layout.Streams
{
    class EStreamTable
    {
        public ENoteSymbol NoteSymbol { get; set; } //символ выноски
        public ESymbolBase AnchorSymbol { get; set; } //символ привязки
        public int Columns { get; set; }
        public bool ForDevice { get; set; }

        private List<string> _oldCables { get; set; }
        private string _gid { get; set; }

        public EStreamTable(string gid)
        {
            Columns = 0;
            _gid = gid;
            ForDevice = false;
            NoteSymbol = null;
            AnchorSymbol = null;
            _oldCables = new List<string>();
        }

        void gkh_KeyDown(object sender, KeyEventArgs e)
        {
            SendKeys.SendWait("{ESC}");
            Columns = (int)e.KeyCode - 48;
            e.Handled = true;
        }

        //Создание графики таблицы кабельных потоков
        public double DrawTable(List<EStreamCable> cables, double insertX, double insertY, string streamNumber, bool addLine)
        {
            //cables = cables.Where(f => f.AddCable == true).ToList();
            if (cables.Count == 0)
                return insertY;
            cables = cables.OrderBy(o => o.Type).ThenBy(o => Utilites.PadNumbers(o.Name)).ToList();
            List<int> groupIds = new List<int>();
            double dx = 0, dy = 0, typeW = 10, cabW = 20, rowH = 5;

            typeW = eSettings.Profile.StreamTypeWidth;
            cabW = eSettings.Profile.StreamCableWidth;
            rowH = eSettings.Profile.StreamCableHeight;

            //Отрисовка линии между символом выноски (______) и символом привязки (изделие, либо черта на линии)
            if (AnchorSymbol!=null && NoteSymbol!=null && !string.IsNullOrEmpty(streamNumber))
            {
                if (Math.Abs(AnchorSymbol.X - NoteSymbol.Left) < Math.Abs(AnchorSymbol.X - NoteSymbol.Right))
                    e3c.graph.CreateLine(Utilites.CurrentSheet.ID, AnchorSymbol.X, AnchorSymbol.Y, NoteSymbol.Left, NoteSymbol.Y);
                else
                    e3c.graph.CreateLine(Utilites.CurrentSheet.ID, AnchorSymbol.X, AnchorSymbol.Y, NoteSymbol.Right, NoteSymbol.Y);
                e3c.graph.SetLineColour(e3c.job.GetGraphTextColour());
                e3c.graph.SetAttributeValue(eSettings.Profile.GidAttribute, _gid);
            }

            //Если точка вставки 0 - то показываем превьюшку и размещаем интерактивно
            if (insertX == 0 && insertY == 0)
            {
                Columns = Columns == 0 ? cables.Count / 4 : Columns;
                Columns = Columns > 0 ? Columns : 1;
                KeyboardHook gkh = new KeyboardHook();
                for (int i = 49; i <= 57; i++)
                {
                    gkh.HookedKeys.Add((Keys)i);
                }
                gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
                var outer = Task.Factory.StartNew(() =>      // внешняя задача
                {
                    while (dx == 0)
                    {
                        CreatePreviewRectangle(Utilites.CurrentSheet.ID, (Columns) * cabW + typeW, Math.Ceiling((double)cables.Count / Columns) * rowH, ref dx, ref dy);
                    }
                });
                outer.Wait(); // ожидаем выполнения внешней задачи
                gkh.unhook();
            }
            else
            {
                Columns = Columns > 0 ? Columns : 1;
                dx = insertX;
                dy = insertY;
            }

            double currentX = dx + typeW;
            PointD leftTop = new PointD(dx, dy);
            PointD rightTop = new PointD(dx + typeW + cabW, dy);
            PointD leftBottom = new PointD(dx, dy);
            PointD rightBottom = new PointD(dx, dy);

            int grpCounter = 0;
            var grpBySignalType = cables.GroupBy(g => g.Type).Select(s => s.ToList()).ToList();
            int e3Color = e3c.job.GetGraphTextColour();
            int currentColor = e3Color;

            //Полка таблицы
            if (addLine && string.IsNullOrEmpty(streamNumber))
            {
                groupIds.Add(e3c.graph.CreateLine(Utilites.CurrentSheet.ID, dx, dy, dx + typeW + cabW, dy));
                e3c.graph.SetLineColour(currentColor);
                if (AnchorSymbol != null)
                {
                    if (Math.Abs(AnchorSymbol.X - leftTop.X) < Math.Abs(AnchorSymbol.X - rightTop.X))
                    {
                        groupIds.Add(e3c.graph.CreateLine(Utilites.CurrentSheet.ID, dx, dy - rowH, dx, dy));
                    }
                    else
                    {
                        groupIds.Add(e3c.graph.CreateLine(Utilites.CurrentSheet.ID, rightTop.X, dy - rowH, rightTop.X, dy));
                    }
                }
                else
                {
                    groupIds.Add(e3c.graph.CreateLine(Utilites.CurrentSheet.ID, rightTop.X, dy - rowH, rightTop.X, dy));
                }
                e3c.graph.SetLineColour(currentColor);
                dy -= rowH;
            }

            double cabTypeY = dy;
            double cabNameY = dy;

            foreach (var grps in grpBySignalType)
            {
                currentX = dx + typeW;
                cabNameY = cabTypeY;
                //тип сигнала
                int rowCount = (int)Math.Ceiling((double)grps.Count / Columns);
                if (typeW > 0)
                {
                    groupIds.AddRange(CreateRectWithText(Utilites.CurrentSheet.ID, dx, cabTypeY, currentX, (cabTypeY - rowCount * rowH), _gid, currentColor, grps.First().Type, 2));
                    if (e3c.txt.GetWidth() > 4) e3c.txt.SetMode(2);
                    if (AnchorSymbol!=null)
                        e3c.txt.SetHyperlinkAddress("gid:" + AnchorSymbol.GID);
                }
                for (int i = 0; i < grps.Count; i++)
                {
                    if (insertX > 0 && _oldCables.Count>0 && !_oldCables.Contains(grps[i].Name))
                    {
                        currentColor = 203;
                    }
                    else
                    {
                        currentColor = e3Color;
                    }
                    if (grpCounter == 0 && !addLine)
                    {
                        if (currentX + cabW > rightTop.X)
                            rightTop.X = currentX + cabW;
                    }
                    groupIds.AddRange(CreateRectWithText(Utilites.CurrentSheet.ID, currentX, cabNameY, currentX + cabW, cabNameY - rowH, _gid, currentColor, grps[i].Name, 2));
                    cabNameY -= rowH;
                    if (cabNameY <= cabTypeY - rowCount * rowH)
                    {
                        cabNameY = cabTypeY;
                        currentX += cabW;
                    }
                    if (grpCounter == grpBySignalType.Count - 1)
                    {
                        rightBottom.X = currentX;
                    }
                }
                cabTypeY -= rowCount * rowH;

                grpCounter++;
            }

            leftBottom.Y = cabTypeY;
            rightBottom.Y = cabTypeY;

            if (!string.IsNullOrEmpty(streamNumber) && NoteSymbol.NoteNumberAttributeID>0)
            {
                e3c.sym.SetId(NoteSymbol.ID);
                e3c.att.SetId(NoteSymbol.NoteNumberAttributeID);
                int txtID = e3c.att.DisplayValueAt(Utilites.CurrentSheet.ID, dx, dy + 1);
                e3c.txt.SetId(txtID);
                FormatText(_gid, currentColor, 1);
                e3c.txt.SetHyperlinkAddress("gid:" + NoteSymbol.GID);
                e3c.txt.SetFontName(e3c.job.GetGraphTextFontName());
                e3c.txt.SetHeight(e3c.job.GetGraphTextHeight());
                e3c.txt.SetMode(e3c.job.GetGraphTextMode());
                e3c.txt.SetStyle(e3c.job.GetGraphTextStyle());
                groupIds.Add(txtID);
                
            }
            else if (AnchorSymbol!=null)
            {
                //Размещение текста изделия
                PointD p = new PointD();
                int algmnt = 1;
                if (Utilites.GetLineLenght(AnchorSymbol.X, leftTop.X, AnchorSymbol.Y, leftTop.Y) < Utilites.GetLineLenght(AnchorSymbol.X, rightTop.X, AnchorSymbol.Y, rightTop.Y))
                {
                    p = leftTop;
                    if (!ForDevice && Utilites.GetLineLenght(AnchorSymbol.X, leftBottom.X, AnchorSymbol.Y, leftBottom.Y) < Utilites.GetLineLenght(AnchorSymbol.X, p.X, AnchorSymbol.Y, p.Y))
                        p = leftBottom;
                }
                else
                {
                    p = rightTop;
                    algmnt = 3;
                    if (!ForDevice && Utilites.GetLineLenght(AnchorSymbol.X, rightBottom.X, AnchorSymbol.Y, rightBottom.Y) < Utilites.GetLineLenght(AnchorSymbol.X, p.X, AnchorSymbol.Y, p.Y))
                        p = rightBottom;
                }
                groupIds.Add(e3c.graph.CreateLine(Utilites.CurrentSheet.ID, AnchorSymbol.X, AnchorSymbol.Y, p.X, p.Y));
                e3c.graph.SetColour(currentColor);
            }

            dynamic graIds = new object[groupIds.Count + 1];
            for (int i = 0; i < groupIds.Count; i++)
                graIds[i + 1] = groupIds[i];
            e3c.group.Create(graIds);
            e3c.group.SetAttributeValue(eSettings.Profile.GidAttribute, _gid);
            string note = Columns.ToString();
            e3c.group.SetAttributeValue(eSettings.Profile.GraphAttribute, note);
            return leftBottom.Y - rowH;
        }

        public static int CreatePreviewRectangle(int sheetID, double width, double height, ref double groupX, ref double groupY)
        {
            double oldLineWidth = e3c.job.GetLineWidth();
            e3c.job.SetLineWidth(1);
            List<int> ids = new List<int>();
            int res = 0;
            ids.Add(e3c.graph.CreateRectangle(sheetID, 0, 0, width, height));
            ids.Add(e3c.graph.CreateLine(sheetID, 0, 0, width, height));
            ids.Add(e3c.graph.CreateLine(sheetID, 0, height, width, 0));

            dynamic graIds = new object[ids.Count + 1];
            for (int i = 0; i < ids.Count; i++)
                graIds[i + 1] = ids[i];
            int grId = e3c.group.Create(ref graIds, "1");
            e3c.group.Unplace();
            dynamic grids = null;
            int grcnt = e3c.job.GetUnplacedGroupIds(ref grids);
            e3c.group.SetId(grids[grcnt]);
            int oldGrcnt = e3c.job.GetGroupIds(ref grids);
            res = e3c.group.PlaceInteractively();
            grcnt = e3c.job.GetGroupIds(ref grids);
            if (grcnt > oldGrcnt)
            {
                object x = null, y = null, z = null;
                e3c.group.GetLocation(ref x, ref y, ref z, ref z);
                groupX = (double)x;
                groupY = (double)y + height;
            }
            e3c.group.DeleteContents();
            e3c.job.SetLineWidth(oldLineWidth);
            return res;
        }

        //Обновление таблицы
        public void UpdateTable(int groupID, List<EStreamCable> cables)
        {
            e3c.group.SetId(groupID);
            //Кол-во столбцов
            string note = e3c.group.GetAttributeValue(eSettings.Profile.GraphAttribute);
            int colCount = 2;
            if (!string.IsNullOrEmpty(note))
            {
                string strColCount = note.Last().ToString();
                if (!int.TryParse(strColCount, out colCount))
                    colCount = 2;
            }
            Columns = colCount;
            _oldCables = GetCablesFromTable(out string streamNumber, out double maxX, out double maxY);
            Utilites.DeleteOldItems(_gid, eSettings.Profile.GidAttribute, true, false, false);
            DrawTable(cables, maxX, maxY, streamNumber, eSettings.Profile.StreamAddLine);
        }

        //Прямоугольник с текстом
        public List<int> CreateRectWithText(int sheetID, double xl, double yl, double xr, double yr, string noteValue, int color, string text = "", int alignment = 1, bool deleteGraphic = false)
        {
            var groupIds = new List<int>();
            if (!deleteGraphic)
            {
                groupIds.Add(e3c.graph.CreateRectangle(sheetID, xl, yl, xr, yr));
                e3c.graph.SetAttributeValue(eSettings.Profile.GidAttribute, noteValue);
                e3c.graph.SetColour(color);
                if (eSettings.Profile.Fill)
                {
                    e3c.graph.SetHatchColour(19);
                    e3c.graph.SetHatchPattern(1, 0, 0);
                }
            }
            double txtX = (xl + xr) / 2;
            double txtY = (yl + yr) / 2 - 1.5;
            if (alignment != 2) txtX = xl + 2;

            int txtid = e3c.graph.CreateText(sheetID, text, txtX, txtY);
            e3c.txt.SetId(txtid);
            double w = e3c.txt.GetWidth();
            if (w > Math.Abs(xl - xr) + 2)
            {
                e3c.txt.SetBox(Math.Abs(xl - xr) - 2, Math.Abs(yl - yr) - 1);
                e3c.txt.SetSchemaLocation(txtX, yl - e3c.txt.GetHeight() - 1);
            }
            groupIds.Add(txtid);
            FormatText(noteValue, color, alignment);
            return groupIds;
        }

        private static void FormatText(string noteValue, int color, int alignment)
        {
            e3c.txt.SetAttributeValue(eSettings.Profile.GidAttribute, noteValue);
            e3c.txt.SetAlignment(alignment);
            e3c.txt.SetColour(color);
        }



        //Считывание имен кабелей из графической таблицы
        private List<string> GetCablesFromTable(out string streamNumber, out double maxX, out double maxY)
        {
            List<string> tableCables = new List<string>();
            dynamic itemIds = null;
            int itemCount = e3c.group.GetItems(ref itemIds);
            streamNumber = "";
            List<PointD> Coordinates = new List<PointD>();
            for (int j = 1; j <= itemCount; j++)
            {
                if (e3c.graph.SetId(itemIds[j]) > 0)
                {
                    if (e3c.graph.GetType() == 3)
                    {
                        object x1 = null, x2 = null, y1 = null, y2 = null;
                        e3c.graph.GetRectangle(ref x1, ref y1, ref x2, ref y2);
                        e3c.graph.SetColour(13);
                        int x = Math.Min(Convert.ToInt16(x1), Convert.ToInt16(x2));
                        int y = Math.Max(Convert.ToInt16(y2), Convert.ToInt16(y1));
                        var newPoint = new PointD(x, y);
                        Coordinates.Add(newPoint);
                    }
                    else if (e3c.graph.GetType() == 8)
                    {
                        string text = e3c.graph.GetText();
                        if (!string.IsNullOrEmpty(text))
                        {
                            if (text.StartsWith("Поток"))
                                streamNumber = text;
                            else
                                tableCables.Add(text);
                        }
                    }
                }
            }
            if (Coordinates.Count > 0)
            {
                maxY = Coordinates.Max(m => m.Y);
                maxX = Coordinates.Min(m => m.X);
                if (eSettings.Profile.StreamAddLine && string.IsNullOrEmpty(streamNumber))
                    maxY += eSettings.Profile.StreamCableHeight;
            }
            else
            {
                maxX = 0;
                maxY = 0;
            }
            //Поиск символа привязки
            if (!string.IsNullOrEmpty(streamNumber))
            {
                dynamic symIds = null;
                int symCount = e3c.sheet.GetSymbolIds(ref symIds);
                for (int i = 1; i <= symCount; i++)
                {
                    e3c.sym.SetId(symIds[i]);
                    if (e3c.sym.GetSymbolTypeName() == eSettings.Profile.NoteSymbol && e3c.sym.GetAttributeValue(eSettings.Profile.GidAttribute) == _gid)
                    {
                        NoteSymbol = new ENoteSymbol(eSettings.Profile.StreamAttribute);
                        break;
                    }
                }
            }
            return tableCables;
        }

        //Поиск символа привязки
        private ESymbolBase GetNoteSymbol()
        {
            e3c.sym.SetId(0);
            var tempSym = new ESymbolBase();
            dynamic symIds = null;
            int symCount = e3c.segm.GetSymbolIds(ref symIds);
            for (int k = 1; k <= symCount; k++)
            {
                e3c.sym.SetId(symIds[k]);
                tempSym = new ESymbolBase();
                if (tempSym.Name == eSettings.Profile.StreamNoteTemplate)
                {
                    return tempSym;
                }
            }
            e3c.app.PutError(0, "Не удалось обнаружить символ привязки потока!");
            return tempSym;
        }
    }
}
