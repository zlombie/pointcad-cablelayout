﻿using System;
using System.Collections.Generic;

namespace Layout
{
    public class ENoteSymbol : ESymbolBase
    {
        public ENoteSymbol(string noteAttribute)
        {
            dynamic attIds = null;
            int attCount = e3c.sym.GetAttributeIds(ref attIds, noteAttribute);
            if (attCount > 0)
            {
                NoteNumberAttributeID = attIds[1];
            }
            dynamic txtIds = null;
            int txtCount = e3c.sym.GetTextIds(ref txtIds);
            for(int i=1; i<=txtCount; i++)
            {
                e3c.txt.SetId(txtIds[i]);
                TxtIDs.Add(new Tuple<int, int>(txtIds[i], e3c.txt.GetTypeId()));
            }
        }

        public int NoteNumberAttributeID { get; private set; }
        public List<Tuple<int, int>> TxtIDs { get; private set; } = new List<Tuple<int, int>>();

    }

}
