﻿using Layout.Area;
using Layout.Configuration;
using Layout.Enums;
using Pointcad.Dragon.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Layout.Streams
{   
    class EStreamBuilder
    {
        private event ProgressHandler ProgressEvent;
        private event StatusTextHandler StatusEvent;

        public List<EStreamCable> Cables = new List<EStreamCable>();

        public EStreamBuilder(ProgressHandler progress, StatusTextHandler statusTextHandler)
        {
            ProgressEvent += progress;
            StatusEvent += statusTextHandler;
        }

        public int RefreshCount { get; set; }

        #region Чтение потоков

        //Считывание данных
        public void ReadStreams(bool disconnect = true)
        {
            try
            {
                Cables = new List<EStreamCable>();
                e3c.InitE3Objects();
                Utilites.SelectedItem = new ePathSegmentItem(0);
                List<int> cablesList = new List<int>();
                switch (Utilites.SelectedItem.Type)
                {
                    case ItemType.None:
                        return;
                    default:
                        e3c.InitE3Objects();
                        cablesList = GetCables(Utilites.SelectedItem.ID);
                        break;
                }
                Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
                Cables = GetStreamInformation(cablesList);
                if (disconnect)
                    e3c.DisposeE3Job();
            }
            catch (Exception ex)
            {
                MessageWindow.ShowDialog($"{ex.Message}\n{ex.StackTrace}", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            }
        }

        //Получение кабелей для элемента
        private List<int> GetCables(int itemID)
        {
            List<int> cableList = new List<int>();
            dynamic coreIds = null;
            if (e3c.segm.SetId(itemID) > 0)
            {
                int coreCount = e3c.segm.GetCoreIds(ref coreIds);
                for (int j = 1; j <= coreCount; j++)
                {
                    e3c.dev.SetId(coreIds[j]);
                    cableList.Add(e3c.dev.GetId());
                }
            }
            else if (e3c.sym.SetId(itemID) > 0)
            {
                dynamic pinIds = null;
                int pinCount = e3c.sym.GetPinIds(ref pinIds);
                for (int i = 1; i <= pinCount; i++)
                {
                    e3c.core.SetId(pinIds[i]);
                    int coreCount = e3c.core.GetCoreIds(ref coreIds);
                    for (int j = 1; j <= coreCount; j++)
                    {
                        e3c.dev.SetId(coreIds[j]);
                        if ((e3c.dev.IsCable() > 0) || (e3c.dev.IsTube() > 0 && eSettings.Profile.ReadTubes))
                            cableList.Add(e3c.dev.GetId());
                    }
                }
            }
            if (cableList.Count > 0)
                return cableList.Distinct().ToList();
            return cableList;
        }

        //Вся информация для потока
        private List<EStreamCable> GetStreamInformation(List<int> cableIds)
        {
            var tempList = new List<EStreamCable>();
            for (int i = 0; i < cableIds.Count; i++)
            {
                e3c.dev.SetId(cableIds[i]);
                var cbl = new EStreamCable();
                if (tempList.Find(f => f.ID == cbl.ID) != null)
                    continue;
                tempList.Add(cbl);
            }
            tempList = tempList.OrderBy(o => o.Type).ThenBy(o => Utilites.PadNumbers(o.Name)).ToList();
            return tempList;
        }

        //Получение номера потока
        protected string GetStreamNumber(string attrName, bool fromSegment, char splitter, bool updateNumbers, bool isAlphabetical, params string[] symTypeName)
        {
            List<EStreamNumber> numbers = new List<EStreamNumber>();
            List<EGroup> groups = new List<EGroup>();
            
            e3c.sheet.SetId(Utilites.CurrentSheet.ID);

            int res = 0;
            if (updateNumbers)
            {
                dynamic groupIds = null;
                int groupCount = e3c.sheet.GetGroupIds(ref groupIds);
                for (int i = 1; i <= groupCount; i++)
                {
                    e3c.group.SetId(groupIds[i]);
                    string gid = e3c.group.GetAttributeValue(eSettings.Profile.GidAttribute);
                    if (!string.IsNullOrEmpty(gid))
                    {
                        dynamic itemIds = null;
                        int itemCount = e3c.group.GetItems(ref itemIds);
                        for (int j = 1; j <= itemCount; j++)
                        {
                            if (e3c.txt.SetId(itemIds[j]) > 0)
                            {
                                string t = e3c.txt.GetText();
                                if (!string.IsNullOrEmpty(t) && t.StartsWith("Поток"))
                                {
                                    groups.Add(new EGroup { TxtID = itemIds[j], GID = gid });
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            dynamic symIds = null;
            int symCnt = e3c.sheet.GetSymbolIds(ref symIds);
            for (int i = 1; i <= symCnt; i++)
            {
                e3c.sym.SetId(symIds[i]);
                string symtypename = e3c.sym.GetSymbolTypeName();
                if (!symTypeName.Any(a=>a==symtypename))
                    continue;
                string number = "";
                if (splitter != 'n')
                {
                    var splArr = e3c.sym.GetAttributeValue(attrName).Split(splitter);
                    if (splArr.Length > 1)
                        number = splArr[1];
                }
                else
                    number = e3c.sym.GetAttributeValue(attrName);
                if (fromSegment)
                {
                    int segmId = e3c.sym.GetTargetObjectId();
                    e3c.segm.SetId(segmId);
                    number = e3c.segm.GetAttributeValue(attrName);
                }
                if (!isAlphabetical)
                {
                    if (int.TryParse(number, out int nmb))
                    {
                        string gid = e3c.sym.GetAttributeValue(eSettings.Profile.GidAttribute);
                        numbers.Add(new EStreamNumber { Number = nmb, NoteID = symIds[i], GID = gid });
                    }
                }
                else
                {
                    if (Utilites.RussianAlphabet.Contains(number))
                    {
                        string gid = e3c.sym.GetAttributeValue(eSettings.Profile.GidAttribute);
                        numbers.Add(new EStreamNumber { Number = Utilites.RussianAlphabet.IndexOf(number), NoteID = symIds[i], GID = gid });
                    }
                }
            }
            numbers = numbers.OrderBy(o => o.Number).ToList();

            if (updateNumbers)
            {
                for (int i = 0; i < numbers.Count; i++)
                {
                    e3c.sym.SetId(numbers[i].NoteID);
                    string val = string.Format("Поток {0}", i + 1);
                    e3c.sym.SetAttributeValue(eSettings.Profile.StreamAttribute, val);
                    var fndGr = groups.FindAll(f => f.GID == numbers[i].GID);
                    if (fndGr != null)
                    {
                        foreach (var f in fndGr)
                        {
                            e3c.txt.SetId(f.TxtID);
                            e3c.txt.SetText(val);
                        }
                    }
                }
                res = 0;
            }
            else
            {
                if (numbers.Count == 0 || numbers.First().Number > 1)
                    res = 1;
                else
                {
                    for (int i = 1; i < numbers.Count; i++)
                    {
                        if (numbers[i].Number != numbers[i - 1].Number + 1)
                        {
                            res = numbers[i - 1].Number + 1;
                            break;
                        }
                    }
                    res = res==0 ? numbers[numbers.Count - 1].Number + 1 : res;
                }
            }
            if (isAlphabetical)
            {
                if (res<Utilites.RussianAlphabet.Length)
                {
                    return Utilites.RussianAlphabet[res].ToString();
                }
                else
                {
                    return "-";
                }
            }
            else
            {
                return res.ToString();
            }
        }

        #endregion

        #region Отрисовка потоков

        public void DrawStream(bool outside)
        {
            e3c.InitE3Objects();
            var newTable = new EStreamTable(Utilites.SelectedItem.GID);
            string streamNumber = "";
            Utilites.DeleteOldItems(Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, true, true, false);
            if (outside) //поток с выноской
            {
                streamNumber = $"Поток {GetStreamNumber(eSettings.Profile.StreamAttribute, false, ' ', false, false, eSettings.Profile.NoteSymbol)}";
                StatusEvent($"Текущий номер потока: {streamNumber}");
                if (Utilites.SelectedItem.Type == ItemType.Device)
                {
                    e3c.sym.SetId(Utilites.SelectedItem.ID);
                    newTable.AnchorSymbol = new ESymbolBase();
                    newTable.NoteSymbol = PutNote(eSettings.Profile.NoteSymbol, "", Utilites.SelectedItem.GID, eSettings.Profile.StreamAttribute, streamNumber);
                    newTable.ForDevice = true;
                }
                else
                {
                    e3c.sym.Load(eSettings.Profile.StreamNoteTemplate, "1");
                    int res = e3c.sym.PlaceInteractively();
                    if (res > 0)
                    {
                        e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, Utilites.SelectedItem.GID);
                        newTable.AnchorSymbol = new ESymbolBase();
                        newTable.NoteSymbol = PutNote(eSettings.Profile.NoteSymbol, "", Utilites.SelectedItem.GID, eSettings.Profile.StreamAttribute, streamNumber);
                    }
                }
            }
            else //поток по месту
            {
                if (Utilites.SelectedItem.Type == ItemType.Device)
                {
                    e3c.sym.SetId(Utilites.SelectedItem.ID);
                    newTable.AnchorSymbol = new ESymbolBase();
                    newTable.ForDevice = true;
                }
                else
                {
                    e3c.sym.Load(eSettings.Profile.StreamNoteTemplate, "1");
                    int res = e3c.sym.PlaceInteractively();
                    if (res > 0)
                    {
                        e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, Utilites.SelectedItem.GID);
                        newTable.AnchorSymbol = new ESymbolBase();
                    }
                }
            }
            if (newTable.AnchorSymbol!=null)
                newTable.DrawTable(Cables, 0, 0, streamNumber, eSettings.Profile.StreamAddLine);
            e3c.DisposeE3Job();
        }

        //Установка выноски
        protected ENoteSymbol PutNote(string mainNoteSymbol, string verticalNoteSymbol, string masterGID, string attributeName, string noteValue="")
        {
            int res = e3c.sym.Load(mainNoteSymbol, "CCCC");
            if (res == 0)
                MessageWindow.ShowDialog($"Тип символа '{mainNoteSymbol}' не найден в базе данных!", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
            res = e3c.sym.PlaceInteractively();
            if (res > 0)
            {
                if (!string.IsNullOrEmpty(verticalNoteSymbol))
                {
                    int targetId = e3c.sym.GetTargetObjectId();
                    if (targetId>0)
                    {
                        e3c.line.SetId(targetId);
                        dynamic xarr = null;
                        dynamic yarr = null;
                        dynamic zarr = null;
                        e3c.line.GetCoordinates(ref xarr, ref yarr, ref zarr, ref zarr);
                        E3Line ln = new E3Line();
                        ln.IsVertical = Utilites.GetCoordinates(xarr[1], xarr[2], yarr[1], yarr[2], out ln.x1, out ln.x2, out ln.y1, out ln.y2);
                        if (ln.IsVertical)
                        {
                            ESymbolBase oldNoteSym = new ESymbolBase();
                            oldNoteSym.GetCoordinates();
                            e3c.sym.Delete();
                            res = e3c.sym.Load(verticalNoteSymbol, "CCCC");
                            if (res == 0)
                                MessageWindow.ShowDialog($"Тип символа '{verticalNoteSymbol}' не найден в базе данных!", "", "Ошибка", icon: Pointcad.Dragon.Windows.MessageWindowImages.Warning);
                            res = e3c.sym.Place(oldNoteSym.SheetID, oldNoteSym.X, oldNoteSym.Y);
                        }
                    }
                }
                e3c.sym.SetId(res);
                e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, masterGID);
                if (!string.IsNullOrEmpty(noteValue))
                {
                    e3c.sym.SetAttributeValue(attributeName, noteValue);
                }
                else
                {
                    e3c.sym.DeleteAttribute(attributeName);
                }
                dynamic txtIds = null;
                int txtCount = e3c.sym.GetTextIds(ref txtIds);
                for (int i = 1; i <= txtCount; i++)
                {
                    e3c.txt.SetId(txtIds[i]);
                    e3c.txt.SetRotation(0);
                }
                return new ENoteSymbol(attributeName);
            }
            else
                return null;
        }

        protected void PlaceAttributeTexts(int masterAttId, string masterAttName, ENoteSymbol noteSymbol, ESymbolBase sectionSymbol)
        {
            e3c.sym.SetId(sectionSymbol.ID);
            dynamic txtIds = null;
            int txtCount = e3c.sym.GetTextIds(ref txtIds);
            int textType = 0;
            for (int i = 1; i <= txtCount; i++)
            {
                e3c.att.SetId(txtIds[i]);
                if (e3c.att.GetInternalName() == masterAttName || e3c.att.GetName() == masterAttName)
                {
                    e3c.txt.SetId(txtIds[i]);
                    textType = e3c.txt.GetTypeId();
                    object x = null, y = null, gr = null;
                    e3c.txt.GetSchemaLocation(ref x, ref y, ref gr);
                    e3c.att.SetId(masterAttId);
                    int txtID = e3c.att.DisplayValueAt(Utilites.CurrentSheet.ID, (double)x, (double)y, sectionSymbol.ID);
                    CopyTextProperties(txtIds[i], txtID);
                }
            }
            //if (noteSymbol!=null && textType > 0 && noteSymbol.TxtIDs.Count > 0)
            //{
            //    var noteTextIds = noteSymbol.TxtIDs.FindAll(f => f.Item2 == textType);
            //    foreach (var n in noteTextIds)
            //    {
            //        e3c.txt.SetId(n.Item1);
            //        object x = null, y = null, gr = null;
            //        e3c.txt.GetSchemaLocation(ref x, ref y, ref gr);
            //        e3c.att.SetId(masterAttId);
            //        int txtID = e3c.att.DisplayValueAt(Utilites.CurrentSheet, (double)x, (double)y, noteSymbol.ID);
            //        CopyTextProperties(n.Item1, txtID);
            //    }
            //}

            void CopyTextProperties(int sourceID, int destinationID)
            {
                e3c.txt.SetId(sourceID);
                int alignment = e3c.txt.GetAlignment();
                int color = e3c.txt.GetColour();
                string font = e3c.txt.GetFontName();
                double height = e3c.txt.GetHeight();
                int mode = e3c.txt.GetMode();
                //double rot = e3c.txt.GetRotation();
                int style = e3c.txt.GetStyle();
                e3c.txt.SetId(destinationID);
                e3c.txt.SetAlignment(alignment);
                e3c.txt.SetColour(color);
                e3c.txt.SetFontName(font);
                e3c.txt.SetHeight(height);
                e3c.txt.SetMode(mode);
                e3c.txt.SetRotation(0);
                e3c.txt.SetStyle(style);
            }
        }

        #endregion

        #region Обновление потоков

        public string UpdateStreams(bool onlySelectedSegment)
        {
            if (string.IsNullOrEmpty(eSettings.Profile.GidAttribute))
                return "В профиле не определен атрибут для хранения GID связанного элемента!";
            RefreshCount = 0;
            string segmGID = "";
            if (onlySelectedSegment)
            {
                switch (Utilites.SelectedItem.Type)
                {
                    case ItemType.None:
                        return "Не обнаружено выделенных участков трассы!";
                    default:
                        segmGID = Utilites.SelectedItem.GID;
                        break;
                }
            }
            e3c.InitE3Objects();
            Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
            if (segmGID != "")
            {
                AssignTableToGID(segmGID);
            }
            //если выбрано несколько листов
            dynamic sheetIds = null;
            int selSheetCount = 0;
            selSheetCount = e3c.job.GetTreeSelectedSheetIds(ref sheetIds);
            if (selSheetCount < 2)
            {
                sheetIds = new object[2];
                sheetIds[0] = 0;
                sheetIds[1] = Utilites.CurrentSheet.ID;
                selSheetCount = 1;
            }
            Cables.Clear();
            var oldSheetId = Utilites.CurrentSheet.ID;
            List<int> badGroups = new List<int>();
            bool founded = false;

            for (int s = 1; s <= selSheetCount; s++)
            {
                e3c.sheet.SetId(sheetIds[s]);
                Utilites.CurrentSheet = new eSheet(sheetIds[s]);
                dynamic groupIds = null;
                int groupCount = e3c.sheet.GetGroupIds(ref groupIds);
                for (int i = 1; i <= groupCount; i++)
                {
                    if (segmGID == "")
                        ProgressEvent(i, groupCount, string.Format("Чтение сгруппированных таблиц с выбранных листов: {0} из {1}", i, groupCount));
                    e3c.group.SetId(groupIds[i]);
                    string gid = e3c.group.GetAttributeValue(eSettings.Profile.GidAttribute);
                    if (!string.IsNullOrEmpty(gid))
                    {
                        //Для одиночного обновления
                        if (!string.IsNullOrEmpty(segmGID) && gid != segmGID)
                        {
                            continue;
                        }
                        var currentTable = new EStreamTable(gid);
                        int masterID = e3c.job.GetIdOfGid(gid);
                        List<int> cableList = new List<int>();

                        if (e3c.segm.SetId(masterID) > 0)
                        {
                            dynamic symIds = null;
                            int symCount = e3c.segm.GetSymbolIds(ref symIds);
                            for (int j = 1; j <= symCount; j++)
                            {
                                e3c.sym.SetId(symIds[j]);
                                string g = e3c.sym.GetAttributeValue(eSettings.Profile.GidAttribute);
                                if (g == gid)
                                {
                                    currentTable.AnchorSymbol = new ESymbolBase();
                                    break;
                                }
                            }
                            cableList = GetCables(masterID);
                        }
                        else if (e3c.sym.SetId(masterID) > 0)
                        {
                            currentTable.AnchorSymbol = new ESymbolBase();
                            currentTable.ForDevice = true;
                            cableList = GetCables(masterID);
                        }
                        else
                        {
                            string nm = e3c.group.GetName();
                            if (nm == "Потоки")
                            {
                                badGroups.Add(groupIds[i]);
                            }
                        }

                        if (cableList.Count > 0)
                        {
                            currentTable.UpdateTable(groupIds[i], GetStreamInformation(cableList));
                            RefreshCount++;
                            founded = true;
                        }
                        //Если обнаружена непривязанная таблица потоков
                        if (!founded)
                        {
                            dynamic itemIds = null;
                            int itemCount = e3c.group.GetItems(ref itemIds);
                            if (itemCount > 0)
                            {
                                e3c.app.PutWarning(0, "Обнаружена непривязанная таблица кабельных потоков. Для перехода кликните 2 раза на это сообщение", itemIds[1]);
                            }
                        }
                    }
                }
            }
            GetStreamNumber(eSettings.Profile.StreamAttribute, false, ' ', true, false, eSettings.Profile.NoteSymbol);
            e3c.DisposeE3Job();
            return "";
        }

        private void AssignTableToGID(string segmGID)
        {
            dynamic grIds = null;
            var selGroups = e3c.job.GetSelectedGraphIds(ref grIds);
            if (selGroups > 0)
            {
                if (e3c.group.SetId(grIds[1]) > 0)
                {
                    e3c.group.SetAttributeValue(eSettings.Profile.GidAttribute, segmGID);
                    e3c.app.PutWarning(0, "Обнаружен выделенный поток и участок трассы/символ. Выполняется переназначение принадлежности потока!");
                }
            }
            else
            {
                selGroups = e3c.job.GetSelectedTextIds(ref grIds);
                if (selGroups > 0)
                {
                    if (e3c.group.SetId(grIds[1]) > 0)
                    {
                        e3c.group.SetAttributeValue(eSettings.Profile.GidAttribute, segmGID);
                        e3c.app.PutWarning(0, "Обнаружен выделенный поток и участок трассы/символ. Выполняется переназначение принадлежности потока!");
                    }
                }
            }
        }
        #endregion

        #region Удаление потоков / разрезов
        public void DeleteStream()
        {
            e3c.InitE3Objects();
            Utilites.DeleteOldItems(Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, true, true, false);
            GetStreamNumber(eSettings.Profile.StreamAttribute, false, ' ', true, false, eSettings.Profile.NoteSymbol);
            e3c.DisposeE3Job();
        } 
        #endregion

    }
}
