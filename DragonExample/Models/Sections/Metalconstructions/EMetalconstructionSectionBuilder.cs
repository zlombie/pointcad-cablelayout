﻿using Layout.Configuration;
using Layout.Streams;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Metalconstructions
{
    class EMetalconstructionSectionBuilder: EStreamBuilder
    {
        public EMetalconstructionWithStands Construction = null;

        private List<EDuctWithCables> _ducts = new List<EDuctWithCables>();
        private string gidprefix = "Instrumentation_metal ";

        public EMetalconstructionSectionBuilder(ProgressHandler progress, StatusTextHandler statusText):base(progress, statusText)
        {
          eSettings.Profile.DuctTypes = new ObservableCollection<ETypeDuct>(eSettings.Profile.DuctTypes.OrderBy(o => o.S));
        }

        #region Чтение
        public string ReadSection(string constructionCode)
        {
            _ducts.Clear();
            ReadStreams(false);
            string res = "";
            if (string.IsNullOrEmpty(eSettings.Profile.MetalconstructionParametersAttribute))
                return "В настройках не определен атрибут для хранения параметров металлоконструкций!";
            if (eSettings.Profile.MetalconstructionTypes.Count==0)
                return "В настройках не определены типы металлоконструкций!";
            if (eSettings.Profile.StandTypes.Count == 0)
                return "В настройках не определены типы кабельных стоек!";
            if (eSettings.Profile.ShelfTypes.Count == 0)
                return "В настройках не определены типы кабельных полок!";
            if (eSettings.Profile.DuctTypes.Count == 0)
                return "В настройках не определены типы коробов!";
            if (Cables.Exists(e => e.OuterDiameter == 0))
                return "Обнаружены кабели с незаданным диаметром, расчет сечения невозможен!";
            Construction = new EMetalconstructionWithStands(constructionCode);
            Construction.Section = Utilites.SelectedItem.DuctSectionNumber;
            Cables.ForEach(f => f.GetCableClass());
            var groupedCables = Cables.GroupBy(g => g.Class).Select(s => s.ToList()).ToList();
            foreach (var g in groupedCables)
            {
                GetDuctsForCableList(g, ref _ducts);
            }
            Construction.AddDucts(_ducts, $"Instrumentation_metal { Utilites.SelectedItem.GID}");
            return res;
        }

        //раскладка кабелей по коробам
        private void GetDuctsForCableList(List<EStreamCable> cableList, ref List<EDuctWithCables> ductList)
        {
            try
            {
                if (cableList.Count == 0)
                    return;
                var difTypeCables = cableList.Where(w => !string.IsNullOrEmpty(w.Tag)).GroupBy(g => g.Tag).OrderBy(o => o.First().Tag);
                var emptyTypeCables = cableList.Where(w => string.IsNullOrEmpty(w.Tag)).ToList();
                var currentDuctList = new List<EDuctWithCables>();
                foreach (var cabType in difTypeCables)
                {
                    var cbT = cabType.OrderBy(o => o.Type).ThenBy(o => o.Name).ToList();
                    if (cabType.Key != "+")
                    {
                        var newDuct = new EDuctWithCables();
                        var fnd = ductList.Find(f => f.ReserveTag == cabType.Key);
                        if (fnd != null)
                            newDuct = fnd;
                        foreach (var c in cbT)
                        {
                            newDuct.Cables.Add(c);
                            if (newDuct.Fullness >= eSettings.Profile.FullPercent)
                            {
                                int index = eSettings.Profile.DuctTypes.IndexOf(newDuct.DuctType);
                                if (index < eSettings.Profile.DuctTypes.Count - 1)
                                {
                                    newDuct.DuctType = eSettings.Profile.DuctTypes[index + 1];
                                }
                                else
                                {
                                    newDuct.Cables.Remove(c);
                                    currentDuctList.Add(newDuct);
                                    newDuct = new EDuctWithCables();
                                    newDuct.Cables.Add(c);
                                }
                            }
                        }
                        if (fnd == null)
                            currentDuctList.Add(newDuct);
                    }
                    else
                    {
                        foreach (var c in cbT)
                        {
                            var fnd = _ducts.Find(f => f.Cables.Find(ff => ff.Name == c.Name) != null);
                            if (fnd != null && !currentDuctList.Contains(fnd))
                            {
                                currentDuctList.Add(fnd);
                                _ducts.Remove(fnd);
                            }
                        }
                    }
                }
                if (currentDuctList.Count == 0)
                    currentDuctList.Add(new EDuctWithCables());

                //установка оптимального размера для коробов (на потом, для обновления или копирования сечения)
                //if (SetOptimalDuct)
                //{
                //    foreach (var d in currentDuctList)
                //    {
                //        d.SetBestSize(_fullPercent);
                //    }
                //}

                int start = 0;
                for (int i = 0; i < currentDuctList.Count; i++)
                {
                    for (int j = start; j < emptyTypeCables.Count; j++)
                    {
                        if (emptyTypeCables[j].Tag != "+")
                        {
                            currentDuctList[i].Cables.Add(emptyTypeCables[j]);
                            if (currentDuctList[i].Fullness >= eSettings.Profile.FullPercent)
                            {
                                int index = eSettings.Profile.DuctTypes.IndexOf(currentDuctList[i].DuctType);
                                if (index < eSettings.Profile.DuctTypes.Count - 1)
                                {
                                    currentDuctList[i].DuctType = eSettings.Profile.DuctTypes[index + 1];
                                }
                                else
                                {
                                    currentDuctList[i].Cables.Remove(emptyTypeCables[j]);
                                    currentDuctList.Add(new EDuctWithCables());
                                    start = j;
                                    break;
                                }
                            }
                            emptyTypeCables[j].Tag = "+";
                        }
                    }
                }

                //Все оставшиеся кабели
                var notAdded = emptyTypeCables.FindAll(f => f.Tag != "+");
                if (notAdded.Count > 0)
                {
                    currentDuctList.Add(new EDuctWithCables());
                    foreach (var n in notAdded)
                    {
                        currentDuctList.Last().Cables.Add(n);
                        if (currentDuctList.Last().Fullness >= eSettings.Profile.FullPercent)
                        {
                            int index = eSettings.Profile.DuctTypes.IndexOf(currentDuctList.Last().DuctType);
                            if (index < eSettings.Profile.DuctTypes.Count - 1)
                            {
                                currentDuctList.Last().DuctType = eSettings.Profile.DuctTypes[index + 1];
                            }
                            else
                            {
                                currentDuctList.Last().Cables.Remove(n);
                                currentDuctList.Add(new EDuctWithCables());
                                currentDuctList.Last().Cables.Add(n);
                            }
                        }
                    }
                }
                ductList.AddRange(currentDuctList.FindAll(f => f.Cables.Count > 0));
            }
            catch (Exception ex)
            {
                e3c.app.PutError(0, $"Ошибка, нажмите кнопку 'Удалить разрез' и повторите действие!\n{ex.Message}");
            }
        }


        #endregion

        #region Запись
        public void DrawSection()
        {
            e3c.InitE3Objects();
            Utilites.DeleteOldItems(gidprefix + Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, false, true, false);
            e3c.segm.SetId(Utilites.SelectedItem.ID);
            e3c.segm.DeleteAttribute(eSettings.Profile.DuctSectionNumberAttribute);
            Utilites.SelectedItem.DuctSectionNumber = GetStreamNumber(eSettings.Profile.DuctSectionNumberAttribute, true, 'n', false, eSettings.Profile.MetalAlphabeticNumerate, eSettings.Profile.DuctSectionSymbolVert, eSettings.Profile.DuctSectionSymbolHor);
            ENoteSymbol noteSym = PutNote(eSettings.Profile.DuctSectionSymbolHor, eSettings.Profile.DuctSectionSymbolVert, gidprefix + Utilites.SelectedItem.GID, eSettings.Profile.DuctSectionNumberAttribute);
            if (noteSym != null)
            {
                e3c.sym.Load(Construction.Type.Symbol, "CCCC");
                Construction.Section = Utilites.SelectedItem.DuctSectionNumber;
                int res = e3c.sym.PlaceInteractively();
                if (res > 0)
                //Отрисовка разреза металлоконструкции
                {
                    e3c.segm.SetId(Utilites.SelectedItem.ID);
                    int attID = e3c.segm.SetAttributeValue(eSettings.Profile.DuctSectionNumberAttribute, Utilites.SelectedItem.DuctSectionNumber);
                    ESymbolBase sectionSym = new ESymbolBase();
                    PlaceAttributeTexts(attID, eSettings.Profile.DuctSectionNumberAttribute, noteSym, sectionSym);
                    e3c.app.PutInfo(0, "Символ разреза размещен");
                    e3c.sym.SetId(sectionSym.ID);
                    Construction.DrawConstruction();
                }
            }
            e3c.DisposeE3Job();
        }

        //Построение разреза
        public void BuildDuctSection()
        {
            WriteDuctsAttributes();
        }


        //Запись коробов в атрибуты участка трассы
        private void WriteDuctsAttributes()
        {
            e3c.InitE3Objects();
            e3c.segm.SetId(Utilites.SelectedItem.ID);

            e3c.segm.SetAttributeValue(eSettings.Profile.MetalconstructionParametersAttribute, Construction.ToString());
            Utilites.DeleteSegmentAttributes(eSettings.Profile.DuctParametersAttribute);
            Construction.SaveToSegment();
        }
        #endregion

        internal void DeleteSection()
        {
            e3c.segm.SetId(Utilites.SelectedItem.ID);
            e3c.segm.DeleteAttribute(eSettings.Profile.MetalconstructionParametersAttribute);
            e3c.segm.DeleteAttribute(eSettings.Profile.DuctParametersAttribute);
            e3c.segm.DeleteAttribute(eSettings.Profile.TrenchSectionAttribute);
            Utilites.DeleteOldItems(gidprefix + Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, false, true, false);
        }
    }
}
