﻿using e3;
using System.Collections.Generic;

namespace Layout
{
    public class ESlotSymbol : ESymbolBase
    {
        public List<InsertPoint> Slots = new List<InsertPoint>();

        public ESlotSymbol()
        {
            dynamic pinIds = null;
            int pinCount = e3c.sym.GetPinIds(ref pinIds);
            for (int i = 1; i <= pinCount; i++)
            {
                Slots.Add(new InsertPoint(pinIds[i]));
            }
        }
    }

    public struct InsertPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public InsertPoint(int pinId)
        {
            e3c.core.SetId(pinId);
            object x = null, y = null, grid = null;
            e3c.core.GetSchemaLocation(ref x, ref y, ref grid);
            X = (double)x;
            Y = (double)y;
        }
    }
}
