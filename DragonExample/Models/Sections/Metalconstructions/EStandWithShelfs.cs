﻿using Layout.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Layout
{
    class EStandWithShelfs
    {
        public ESlotSymbol StandSymbol { get; set; }
        public List<EShelfWithDucts> Shelfs;
        public string Code { get; private set; }
        public string Section { get; set; }

        public EStandWithShelfs(ETypeStand standType, string code)
        {
            Shelfs = new List<EShelfWithDucts>();
            Code = code;
        }

        public void AddShelfs(List<EDuctWithCables> ducts, ETypeShelf shelfType)
        {
            var grpByShelf = ducts.GroupBy(g => g.Location).Select(s => s.ToList());
            foreach (var shlf in grpByShelf)
            {
                var newShelf = new EShelfWithDucts(shelfType);
                newShelf.Ducts.AddRange(shlf);
                Shelfs.Add(newShelf);
            }
        }

        public void Draw(ETypeStand standType, ETypeShelf shelfType, double x, double y, string rot)
        {
            e3c.app.PutInfo(0, "Размещение стойки...");
            e3c.sym.Load(standType.Symbol, "1");
            e3c.sym.Place(Utilites.CurrentSheet.ID, x, y, rot);
            e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, Code);
            e3c.sym.SetAttributeValue(eSettings.Profile.DuctSectionNumberAttribute, Section);
            StandSymbol = new ESlotSymbol();
            //расстановка полок
            int index = 0;
            foreach (var insP in StandSymbol.Slots)
            {
                e3c.sym.Load(shelfType.Symbol, "1");
                e3c.sym.Place(Utilites.CurrentSheet.ID, insP.X, insP.Y, rot);
                e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, $"{Code}{index}");
                e3c.sym.SetAttributeValue(eSettings.Profile.DuctSectionNumberAttribute, Section);
                if (Shelfs.Count > index)
                {
                    Shelfs[index].Symbol = new ESlotSymbol();
                    Shelfs[index].Draw(rot, $"{Code}{index}=");
                }
                index++;
            }
        }

        public void SaveAttributes(string side)
        {
            for (int i = 0; i < Shelfs.Count; i++)
            {
                if (Shelfs[i].Ducts.Count > 0)
                {
                    int ductCounter = 0;
                    for (int j = 0; j < Shelfs[i].Ducts.Count; j++)
                    {
                        string cabString = "";
                        string abyr = "";
                        var d = Shelfs[i].Ducts[j];
                        foreach (var c in d.Cables)
                        {
                            cabString += "|" + c.Name;
                            if (cabString.Length > 220)
                            {
                                e3c.segm.AddAttributeValue(eSettings.Profile.DuctParametersAttribute, $"{side}{i}={ductCounter}{abyr}${d.Name}:{cabString}");
                                cabString = "";
                            }
                        }
                        e3c.segm.AddAttributeValue(eSettings.Profile.DuctParametersAttribute, $"{side}{i}={ductCounter}{abyr}${d.Name}:{cabString}");
                        ductCounter++;
                    }
                }
            }
        }
    }
}
