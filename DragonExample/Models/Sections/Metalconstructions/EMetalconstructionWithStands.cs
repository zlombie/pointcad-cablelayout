﻿using Layout.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout
{
    class EMetalconstructionWithStands
    {
        public EStandWithShelfs LeftStand { get; set; }
        public EStandWithShelfs RightStand { get; set; }

        public ETypeMetalconstruction Type { get; set; }
        public ETypeShelf ShelfType { get; set; }
        public ETypeStand StandType { get; set; }
        public int Sides { get; set; }
        public int ShelfCount { get; set; }

        public string Code { get; private set; }
        public string Section { get; set; }
        public bool NewLayout { get; private set; }
        private ESlotSymbol _constructionSymbol { get; set; }

        public string ConstructionParameters { get { return $"Металлоконструкция: тип: {Type.Name.ToLower()}, сторон: {Sides}, полок: {ShelfCount}, ширина полок: {ShelfType.Width}"; } }
        //Конструктор
        public EMetalconstructionWithStands(string code)
        {
            _constructionSymbol = null;
            LeftStand = null;
            RightStand = null;
            Type = null;
            ShelfType = null;
            StandType = null;
            ShelfCount = 1;
            Sides = 1;
            var spl = code.Split('#');
            if (spl.Length == 4)
            {
                NewLayout = false;
                var fnd = eSettings.Profile.MetalconstructionTypes.FirstOrDefault(f => f.Name == spl[0]) ?? eSettings.Profile.MetalconstructionTypes.First();
                Type = fnd;
                int.TryParse(spl[1], out int s);
                Sides = s;
                int.TryParse(spl[2].Replace(" мм", ""), out int w);
                ShelfType = eSettings.Profile.ShelfTypes.FirstOrDefault(f => f.Width == w) ?? eSettings.Profile.ShelfTypes.First();
                int.TryParse(spl[3], out int c);
                StandType = eSettings.Profile.StandTypes.FirstOrDefault(f => f.ShelfCount == c) ?? eSettings.Profile.StandTypes.First();
            }
            else
            {
                NewLayout = true;
                Type = eSettings.Profile.MetalconstructionTypes.First();
                StandType = eSettings.Profile.StandTypes.First();
                ShelfType = eSettings.Profile.ShelfTypes.First();
                Sides = 1;
            }
        }

        public override string ToString()
        {
            return $"{Type.Name}#{Sides}#{ShelfType.Width} мм#{StandType.ShelfCount}";
        }

        // Распределение полок по стойкам
        public void AddDucts(List<EDuctWithCables> ducts, string code)
        {
            Code = code;
            //Если в кодировке есть короба, не разложенные по полкам (а вдруг?) , то раскладываем их заново
            if (ducts.Exists(e => string.IsNullOrEmpty(e.Location)))
            {
                var shelfs = GetShelfs(ducts);
                if (shelfs.Count > 0)
                {
                    RightStand = new EStandWithShelfs(StandType, $"{Code}|R");
                    if (Sides == 2)
                    {
                        LeftStand = new EStandWithShelfs(StandType, $"{Code}|L");
                        RightStand.Shelfs.Add(shelfs[0]);
                        for (int i = 1; i < shelfs.Count; i++)
                        {
                            switch ((i + 1) % 2)
                            {
                                case (0):
                                    LeftStand.Shelfs.Add(shelfs[i]);
                                    break;
                                case (1):
                                    RightStand.Shelfs.Add(shelfs[i]);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        RightStand.Shelfs = shelfs;
                    }
                }
                //if (Sides == 1)
                //    ShelftCount = shelfs.Count;
                //else
                //    ShelftCount = (int)Math.Ceiling((double)shelfs.Count / 2);
            }
            //Если все прописано в кодировке, то раслкадываем в соответствии с ней
            else
            {
                var rightDucts = ducts.FindAll(f => f.Location.StartsWith("R"));
                if (rightDucts.Count > 0)
                {
                    RightStand = new EStandWithShelfs(StandType, $"{Code}|R");
                    RightStand.AddShelfs(rightDucts, ShelfType);
                }
                var leftDucts = ducts.FindAll(f => f.Location.StartsWith("L"));
                if (leftDucts.Count > 0)
                {
                    LeftStand = new EStandWithShelfs(StandType, $"{Code}|L");
                    LeftStand.AddShelfs(leftDucts, ShelfType);
                }
            }
        }

        //Отрисовка конструкций
        public void DrawConstruction()
        {
            _constructionSymbol = new ESlotSymbol();
            e3c.app.PutInfo(0, $"Количество слотов символа разреза {_constructionSymbol.Slots.Count}");
            e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, Code);
            //Добавление стоек, полок, коробов
            if (RightStand != null && _constructionSymbol.Slots.Count > 0)
            {
                e3c.app.PutInfo(0, "Построение правой стороны...");
                RightStand.Section = Section;
                RightStand.Draw(StandType, ShelfType, _constructionSymbol.Slots[0].X, _constructionSymbol.Slots[0].Y, "");
            }
            if (LeftStand != null && _constructionSymbol.Slots.Count > 1)
            {
                e3c.app.PutInfo(0, "Построение левой стороны...");
                RightStand.Section = Section;
                LeftStand.Draw(StandType, ShelfType, _constructionSymbol.Slots[1].X, _constructionSymbol.Slots[1].Y, "Y");
            }
        }

        public void SaveToSegment()
        {
            if (RightStand != null)
            {
                RightStand.SaveAttributes("R");
            }
            if (LeftStand != null)
            {
                LeftStand.SaveAttributes("L");
            }
        }

        //Раскладка коробов по полкам
        private List<EShelfWithDucts> GetShelfs(List<EDuctWithCables> ducts)
        {
            var newShelf = new EShelfWithDucts(ShelfType);
            List<EShelfWithDucts> shelfs = new List<EShelfWithDucts> { newShelf };
            foreach (var d in ducts)
            {
                double width = d.DuctType.Width;
                double additionalWidth = 0;

                //короб 220 и полка с не-220
                //if (d.Is220 == true && newShelf.HasNon220)
                //{
                //    additionalWidth = 100;
                //}
                if (newShelf.FreeWidth > width + additionalWidth)
                    newShelf.Ducts.Add(d);
                else
                {
                    var fnd = shelfs.FindAll(f => f.FreeWidth > width);
                    if (fnd.Count > 0)
                    {
                        //if (!d.Is220 == true)
                        //{
                        //    fnd = fnd.FindAll(f => !f.Has220);
                        //}
                        //else
                        //{
                        //    fnd = fnd.FindAll(f => !f.HasNon220 || (f.HasNon220 && f.FreeWidth > width + additionalWidth));
                        //}
                        if (fnd.Count > 0)
                        {
                            fnd = fnd.OrderByDescending(o => o.FreeWidth).ToList();
                            fnd.First().Ducts.Add(d);
                        }
                        else
                        {
                            newShelf = new EShelfWithDucts(ShelfType);
                            shelfs.Add(newShelf);
                            newShelf.Ducts.Add(d);
                        }
                    }
                    else
                    {
                        newShelf = new EShelfWithDucts(ShelfType);
                        shelfs.Add(newShelf);
                        newShelf.Ducts.Add(d);
                    }
                }
            }
            ShelfCount = (int)Math.Ceiling((double)shelfs.Count / Sides);
            StandType = eSettings.Profile.StandTypes.FirstOrDefault(f => f.ShelfCount == ShelfCount);
            if (StandType==null)
            {
                //Обработчик
                return new List<EShelfWithDucts>();
            }
            return shelfs;
        }
    }
}
