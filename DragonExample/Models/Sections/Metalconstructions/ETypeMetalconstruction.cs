﻿using System;
using Layout;

[Serializable]
public class ETypeMetalconstruction:eItemWithMaterialComponents
{
    public string Symbol { get; set; }
    public int MaxSides { get; set; }

    public ETypeMetalconstruction()
    {
        MaxSides = 1;
    }


    public override string ToString()
    {
        return Name;
    }
}

