﻿using System;

namespace Layout
{
    //Тип короба
    [Serializable]
    public class ETypeDuct:eItemWithMaterialComponents
    {
        public double S { get { return Height * Width; }}
        public string Symbol { get; set; }
        public bool UseDuct { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        //public double Length { get; set; }

        public ETypeDuct(double w, double h)
        {
            Width = w;
            Height = h;
            UseDuct = true;
        }

        public ETypeDuct()
        {

        }
    }
}
