﻿using System;
using Layout;

[Serializable]
public class ETypeShelf : eItemWithMaterialComponents
{
    public int Width { get; set; }
    public new string Name { get { return $"Полка {Width} мм"; } }
    public string Symbol { get; set; }

    public override string ToString()
    {
        return Name;
    }
}

