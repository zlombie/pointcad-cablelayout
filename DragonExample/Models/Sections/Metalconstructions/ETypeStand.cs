﻿using System;
using Layout;

[Serializable]
public class ETypeStand: eItemWithMaterialComponents
{
    public int ShelfCount { get; set; }
    public string Symbol { get; set; }

    public ETypeStand()
    {
        ShelfCount = 1;
    }
}
