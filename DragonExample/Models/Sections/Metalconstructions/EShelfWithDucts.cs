﻿using Layout.Configuration;
using Layout.Streams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout
{
    class EShelfWithDucts
    {
        public List<EDuctWithCables> Ducts { get; set; }
        public ESlotSymbol Symbol { get; set; }
        public ETypeShelf ShelfType { get; set; }

        public double FreeWidth
        {
            get
            {
                return ShelfType.Width - Ducts.Sum(s => s.DuctType.Width);
            }
        }

        public EShelfWithDucts(ETypeShelf shelfType)
        {
            ShelfType = shelfType;
            Ducts = new List<EDuctWithCables>();
            Symbol = null;
        }

        public EShelfWithDucts(string code)
        {
            Ducts = new List<EDuctWithCables>();
            Symbol = null;
        }

        public void Draw(string rot, string code)
        {
            if (Symbol != null)
            {
                EDuctWithCables oldDuct = new EDuctWithCables();
                
                double currentX = Symbol.Slots.Count>0 ? Symbol.Slots.First().X : Symbol.X;
                double currentY = Symbol.Slots.Count > 0 ? Symbol.Slots.First().Y : Symbol.Y;

                //Направление и масштаб
                int direction = 1;
                if (!string.IsNullOrEmpty(rot)) direction = -1;
                int index = 0;
                foreach (var duct in Ducts)
                {
                    e3c.DisposeE3Job();
                    e3c.InitE3Objects();
                    //duct.Cables.Sort(new CableComparator());
                    //if (oldDuct.Is220 == false && duct.Is220 == true)
                    //    currentX += 10 * direction * scale;
                    e3c.sym.Load(duct.DuctType.Symbol, "1");
                    e3c.sym.Place(Utilites.CurrentSheet.ID, currentX, currentY, rot);
                    e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, $"{code}{index}");
                    duct.DuctSymbol = new ESlotSymbol();
                    currentX += duct.DuctSymbol.Width * direction;

                    var newStream = new EStreamTable($"{code}{index}");
                    newStream.AnchorSymbol = duct.DuctSymbol;
                    newStream.AnchorSymbol.X = duct.DuctSymbol.Slots.Count > 0 ? duct.DuctSymbol.Slots.First().X : duct.DuctSymbol.X;
                    newStream.AnchorSymbol.Y = duct.DuctSymbol.Slots.Count > 0 ? duct.DuctSymbol.Slots.First().Y : duct.DuctSymbol.Y;
                    newStream.ForDevice = false;
                    newStream.Columns = 2;
                    newStream.DrawTable(duct.Cables, 0, 0, "", false);
                    oldDuct = duct;
                    index++;
                }
            }
        }
    }
}
