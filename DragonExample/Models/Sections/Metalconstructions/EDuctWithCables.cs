﻿using System;
using System.Collections.Generic;
using System.Linq;
using e3;
using Layout.Configuration;
using Layout.Streams;

namespace Layout
{
    //Короб с кабелями
    public class EDuctWithCables
    {
        public List<EStreamCable> Cables = new List<EStreamCable>();
        public ETypeDuct DuctType { get; set; }
        public ESlotSymbol DuctSymbol { get; set; }
        public bool IsExisted { get; set; }
        public string Name { get { return $"Короб {DuctType.Width}x{DuctType.Height}"; } }
        public string CableString { get { return string.Join("\n", Cables); } }
        public string Location { get; set; }
        public string CablesClass { get; set; }
        public string ReserveTag { get; set; }
        public string FullnessString { get { return $"{Fullness} %"; } }

        public int Fullness
        {
            get
            {
                if (Cables.Count > 0)
                {
                    double d = Cables.Sum(s => Math.Pow(s.OuterDiameter, 2));
                    return (int)(d / DuctType.S * 100);
                }
                else
                    return 0;
            }
        }

        internal EDuctWithCables()
        {
            Cables = new List<EStreamCable>();
            DuctType = eSettings.Profile.DuctTypes.First();
            ReserveTag = "";
            Location = "";
            CablesClass = "";
            DuctSymbol = new ESlotSymbol();
        }

        public void SetBestSize(int fullPercent)
        {
            DuctType = eSettings.Profile.DuctTypes.First();
            foreach (var c in Cables)
            {
                double d = Cables.Sum(s => Math.Pow(s.OuterDiameter, 2));
                for (int i = 0; i < eSettings.Profile.DuctTypes.Count; i++)
                {
                    if (d / (fullPercent / 100) < eSettings.Profile.DuctTypes[i].S)
                    {
                        DuctType = eSettings.Profile.DuctTypes[i];
                        break;
                    }
                }
            }
        }
    }
}
