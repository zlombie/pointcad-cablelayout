﻿using System;
using Layout;

[Serializable]
public class TrenchType: eItemWithMaterialComponents
{
    public string Description { get; set; }
    public int Width { get; set; }
    public int Depth { get; set; }
    public int Reserve { get; set; }
    public int RealWidth { get { return Width - Reserve; }  }
    public string Symbol { get; set; }


    public override string ToString()
    {
        if (Width > 0 && Depth > 0)
            return $"{Name}: ({Width} х {Depth} мм)";
        else
            return Name;
    }

    public TrenchType()
    {

    }
}