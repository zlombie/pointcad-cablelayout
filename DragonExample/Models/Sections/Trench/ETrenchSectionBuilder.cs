﻿using Layout.Configuration;
using Layout.Streams;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Layout.Trench
{
    class ETrenchSectionBuilder: EStreamBuilder
    {
        public const string gidPrefix = "Instrumentation_trench ";

        public TrenchType CurrentTrench = null;
        public ObservableCollection<TrenchType> AvaibleTrenches { get; set; }

        private double _width;
        private List<List<EStreamCable>> groupedCables = new List<List<EStreamCable>>();

        public ETrenchSectionBuilder(ProgressHandler progress, StatusTextHandler statusText) :base(progress, statusText)
        {
        }

        public string ReadTrench()
        {
            string message = "";
            AvaibleTrenches = new ObservableCollection<TrenchType>();
            CurrentTrench = null;
            if (eSettings.Profile.TrenchTypes.Count == 0)
                return ("В настройках не определены типы траншей!");
            if (Utilites.SelectedItem.Height > 0)
                return ("Для построения разреза траншеи высотная отметка участка трассы должна быть ниже нуля!");
            ReadStreams(false);
            if (Cables.Count > 0)
            {
                if (Cables.Exists(e => e.OuterDiameter == 0))
                    return ("Наружный диаметр задан не для всех кабелей на участке трассы!");
                Cables.ForEach(f => f.GetCableClass());
                groupedCables = Cables.GroupBy(g => g.Class).Select(s => s.ToList()).ToList();
                _width = 0;
                if (groupedCables.Count() > 1)
                {
                    //сумма диаметров
                    _width += groupedCables.First().Sum(s => s.OuterDiameter);
                    _width += (groupedCables.First().Count() - 1) * (groupedCables.First().First().BetweenCables);

                    for (int i = 0; i < groupedCables.Count() - 1; i++)
                    {
                        string type1 = groupedCables[i].First().Class;
                        string type2 = groupedCables[i + 1].First().Class;
                        var fnd = eSettings.Profile.BetweenTypes.FirstOrDefault(f => (f.Class1 == type1 && f.Class2 == type2) || (f.Class1 == type2 && f.Class2 == type1));
                        if (fnd != null)
                        {
                            _width += fnd.BetweenCables;
                        }
                        _width += groupedCables[i + 1].Sum(s => s.OuterDiameter);
                        _width += (groupedCables[i + 1].Count() - 1) * (groupedCables[i + 1].First().BetweenCables);
                    }
                }
                else
                {
                    _width += Cables.Sum(s => s.OuterDiameter) + (Cables.First().BetweenCables * (Cables.Count - 1));
                }
                AvaibleTrenches = new ObservableCollection<TrenchType>(eSettings.Profile.TrenchTypes.Where(f => f.RealWidth >= _width && f.Depth > Math.Abs(Utilites.SelectedItem.Height * 1000)));
                //Текущая траншея
                if (AvaibleTrenches.Count > 0)
                {
                    e3c.segm.SetId(Utilites.SelectedItem.ID);
                    string s = e3c.segm.GetAttributeValue(eSettings.Profile.TrenchParametersAttribute);
                    var tmpfnd = AvaibleTrenches.FirstOrDefault(f => f.Name == s);
                    if (tmpfnd != null)
                    {
                        CurrentTrench = tmpfnd;
                        message = $"Траншея, {CurrentTrench.Name}, ширина: {CurrentTrench.Width}, глубина: {CurrentTrench.Depth}";
                    }
                    else
                    {
                        message = $"Параметры траншеи не заданы!";
                        CurrentTrench = AvaibleTrenches.First();
                    }
                }
                if (CurrentTrench != null)
                {
                    return message;
                }
                else
                {
                    return ("В настройках не обнаружено траншеи подходящего размера!");
                }
            }
            return string.Empty;
        }


        public void BuildTrench()
        {
            if (CurrentTrench != null)
            {
                e3c.segm.SetId(Utilites.SelectedItem.ID);
                e3c.segm.SetAttributeValue(eSettings.Profile.TrenchParametersAttribute, CurrentTrench.Name);
            }
        }

        //разрез траншеи
        public void DrawTrench()
        {
            if (CurrentTrench != null)
            {
                Utilites.DeleteOldItems(gidPrefix + Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, true, true, false);
                e3c.segm.DeleteAttribute(eSettings.Profile.TrenchSectionAttribute);
                Utilites.SelectedItem.TrenchSectionNumber = GetStreamNumber(eSettings.Profile.TrenchSectionAttribute, true, 'n', false, eSettings.Profile.TrenchAlphabeticNumerate, eSettings.Profile.TrenchSectionSymbolVert, eSettings.Profile.TrenchSectionSymbolHor);
                e3c.segm.SetId(Utilites.SelectedItem.ID);
                ENoteSymbol noteSym = PutNote(eSettings.Profile.TrenchSectionSymbolHor, eSettings.Profile.TrenchSectionSymbolVert, gidPrefix + Utilites.SelectedItem.GID, eSettings.Profile.TrenchSectionAttribute);
                if (noteSym != null)
                {
                    int res = e3c.sym.Load(CurrentTrench.Symbol, "CCCC");
                    if (res > 0)
                    {
                        res = e3c.sym.PlaceInteractively();
                        if (res > 0)
                        {
                            int attID = e3c.segm.SetAttributeValue(eSettings.Profile.TrenchSectionAttribute, Utilites.SelectedItem.TrenchSectionNumber);
                            
                            ESymbolBase sectionSym = new ESymbolBase();
                            e3c.sym.SetAttributeValue(eSettings.Profile.GidAttribute, gidPrefix + Utilites.SelectedItem.GID);
                            e3c.sym.SetAttributeValue(eSettings.Profile.TrenchCableDepthAttribute, Math.Abs(Utilites.SelectedItem.Height * 1000).ToString());

                            PlaceAttributeTexts(attID, eSettings.Profile.TrenchSectionAttribute, noteSym, sectionSym);

                            double startX = sectionSym.X - _width / 2 * (eSettings.Profile.TrenchScale);
                            double currentX = startX;
                            double currentY = sectionSym.Y - eSettings.Profile.TrenchStreamY;
                            for (int i = 0; i < groupedCables.Count; i++)
                            {
                                int counter = 0;
                                foreach (var c in groupedCables[i])
                                {
                                    currentX += (double)c.OuterDiameter / 2 * (eSettings.Profile.TrenchScale);
                                    e3c.graph.CreateCircle(Utilites.CurrentSheet.ID, currentX, sectionSym.Y, (double)c.OuterDiameter/2 * eSettings.Profile.TrenchScale);
                                    e3c.graph.SetHatchPattern(1, 0, 0);
                                    e3c.graph.SetAttributeValue(eSettings.Profile.GidAttribute, gidPrefix + Utilites.SelectedItem.GID);
                                    currentX += (double)c.OuterDiameter / 2 * eSettings.Profile.TrenchScale;
                                    counter++;
                                    if (counter < groupedCables[i].Count)
                                        currentX += (double)c.BetweenCables * eSettings.Profile.TrenchScale;
                                }

                                e3c.graph.CreateLine(Utilites.CurrentSheet.ID, (currentX + startX) / 2, sectionSym.Y, (currentX + startX) / 2, currentY);
                                e3c.graph.SetAttributeValue(eSettings.Profile.GidAttribute, gidPrefix + Utilites.SelectedItem.GID);
                                e3c.graph.SetLineColour(e3c.job.GetGraphTextColour());

                                var newStream = new EStreamTable(gidPrefix + Utilites.SelectedItem.GID);
                                double streamX = (currentX + startX) / 2 - eSettings.Profile.StreamCableWidth - eSettings.Profile.StreamTypeWidth;
                                currentY = newStream.DrawTable(groupedCables[i], streamX, currentY, "", false);

                                if (i < groupedCables.Count - 1)
                                {
                                    string type1 = groupedCables[i].First().Class;
                                    string type2 = groupedCables[i + 1].First().Class;
                                    var fnd = eSettings.Profile.BetweenTypes.FirstOrDefault(f => (f.Class1 == type1 && f.Class2 == type2) || (f.Class1 == type2 && f.Class2 == type1));
                                    if (fnd != null)
                                    {
                                        currentX += (double)fnd.BetweenCables * (eSettings.Profile.TrenchScale);
                                    }
                                }
                                startX = currentX;
                            }
                        }
                    }
                }
            }
        }

        public void DeleteSection()
        {
            e3c.segm.SetId(Utilites.SelectedItem.ID);
            e3c.segm.DeleteAttribute(eSettings.Profile.TrenchParametersAttribute);
            e3c.segm.DeleteAttribute(eSettings.Profile.TrenchSectionAttribute);
            Utilites.DeleteOldItems(gidPrefix + Utilites.SelectedItem.GID, eSettings.Profile.GidAttribute, true, true, false);
        }
    }
}
