﻿using Layout.Area;
using Layout.Configuration;
using Layout.Metalconstructions;
using Layout.Streams;
using Layout.Trench;
using Pointcad.Dragon.Windows;
using System;
using System.Text;

namespace Layout
{
    class AllSectionBuilder
    {
        public ETrenchSectionBuilder TrenchSection { get; set; }
        public EMetalconstructionSectionBuilder MetalconstructionSection { get; set; }

        public string Message { get; set; }
        private StringBuilder typeBuilder = new StringBuilder();

        private ProgressHandler Progress;
        private StatusTextHandler StatusTextHandler;

        public AllSectionBuilder(ProgressHandler progress, StatusTextHandler statusText)
        {
            Message = "<Нет данных>";
            Progress = progress;
            StatusTextHandler = statusText;
        }

        public void ReadSection()
        {
            TrenchSection = null;
            MetalconstructionSection = null;
            Message = "<Нет данных>";
            typeBuilder.Clear();
            e3c.InitE3Objects();
            Utilites.CurrentSheet = new eSheet(e3c.job.GetActiveSheetId());
            Utilites.SelectedItem = new ePathSegmentItem(0);
            if (Utilites.SelectedItem.Laying != null)
            {
                switch (Utilites.SelectedItem.Laying.SectionType)
                {
                    case "Траншея":
                        TrenchSection = new ETrenchSectionBuilder(Progress, StatusTextHandler);
                        Message = TrenchSection.ReadTrench();
                        break;
                    case "Металлоконструкции":
                        MetalconstructionSection = new EMetalconstructionSectionBuilder(Progress, StatusTextHandler);
                        Message = MetalconstructionSection.ReadSection(Utilites.SelectedItem.ConstructionParams);
                        break;
                }
            }
            e3c.DisposeE3Job();
        }

        public void BuildSection(bool draw)
        {
            try
            {
                e3c.InitE3Objects();
                if (TrenchSection != null)
                {
                    TrenchSection.BuildTrench();
                    if (draw)
                        TrenchSection.DrawTrench();
                }
                else if (MetalconstructionSection != null)
                {
                    MetalconstructionSection.BuildDuctSection();
                    if (draw)
                        MetalconstructionSection.DrawSection();
                }
                e3c.DisposeE3Job();
            }
            catch (Exception ex)
            {

            }
        }


        public void DeleteSection()
        {
            try
            {
                e3c.InitE3Objects();
                if (TrenchSection != null)
                {
                    TrenchSection.DeleteSection();
                }
                else if (MetalconstructionSection != null)
                {
                    MetalconstructionSection.DeleteSection();
                }
                e3c.DisposeE3Job();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
