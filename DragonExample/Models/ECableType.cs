﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


[Serializable]
public class CableType
{
    public string Name { get; set; }
    public string Attribute { get; set; }
    public string Value { get; set; }
    public int BetweenCables { get; set; }

    public override string ToString()
    {
        return Name;
    }
}

[Serializable]
public class BetweenType
{
    private string class1;

    public string Class1
    {
        get
        {
            return class1;
        }
        set
        {
            class1 = value;
        }
    }


    public string Class2 { get; set; }
    public int BetweenCables { get; set; }
}