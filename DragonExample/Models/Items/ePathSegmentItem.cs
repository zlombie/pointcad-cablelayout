﻿using e3;
using Layout.Configuration;
using Layout.Enums;
using Layout.Service;
using System.Linq;

namespace Layout
{
    public class ePathSegmentItem : eBaseItem
    {
        public ItemType Type { get; set; }
        public double Height { get; set; }

        private string _heightString;
        public string HeightString
        {
            get
            {
                return Height != eConstants.BadHeight ? _heightString : eConstants.NullString;
            }
        }
        public string DuctSectionNumber { get; set; }
        public string TrenchSectionNumber { get; set; }

        public LayingType Laying { get; set; }
        public string ConstructionParams { get; set; }
        public string TrenchParams { get; set; }

        public ePathSegmentItem(int id)
        {
            if (id == 0)
            {
                dynamic selectedIds = null;
                int selSegm = e3c.job.GetSelectedNetSegmentIds(ref selectedIds);
                if (selSegm == 1)
                {
                    id = selectedIds[1];
                }
                else
                {
                    int symCount = e3c.job.GetSelectedSymbolIds(ref selectedIds);
                    if (symCount > 0)
                    {
                        id = selectedIds[1];
                    }
                }
            }

            Type = ItemType.NetSegment;
            ID = id;
            GID = e3c.job.GetGidOfId(ID);
            if (e3c.segm.SetId(id) > 0)
            {
                Type = ItemType.NetSegment;
                DuctSectionNumber = e3c.segm.GetAttributeValue(eSettings.Profile.DuctSectionNumberAttribute);
                TrenchSectionNumber = e3c.segm.GetAttributeValue(eSettings.Profile.TrenchSectionAttribute);
                string laying = e3c.segm.GetAttributeValue(eSettings.Profile.LayingTypeAttribute);
                Laying = eSettings.Profile.LayingTypes.FirstOrDefault(f => f.Name == laying);
                _heightString = e3c.segm.GetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute);
                Height = Utilites.ParseHeight(_heightString);
                ConstructionParams = e3c.segm.GetAttributeValue(eSettings.Profile.MetalconstructionParametersAttribute);
                TrenchParams = e3c.segm.GetAttributeValue(eSettings.Profile.TrenchParametersAttribute);
            }
            else if (e3c.sym.SetId(id) > 0 || e3c.dev.SetId(id) > 0)
            {
                string symName = e3c.sym.GetSymbolTypeName();
                if (symName == eSettings.Profile.UpSymbol || symName == eSettings.Profile.DownSymbol)
                {
                    Type = ItemType.HeightSymbol;
                    Laying = eSettings.Profile.LayingTypes.FirstOrDefault(f => f.Name == e3c.sym.GetAttributeValue(eSettings.Profile.LayingSymAttribute));
                }
                else
                {
                    Type = ItemType.Device;
                    DuctSectionNumber = string.Empty;
                    TrenchSectionNumber = string.Empty;
                    if (e3c.dev.SetId(id) > 0)
                    {
                        _heightString = e3c.dev.GetAttributeValue(eSettings.Profile.DeviceHeightAttribute);
                        Height = Utilites.ParseHeight(_heightString);
                    }
                    else
                    {
                        Height = 0;
                    }
                }
            }
        }

    }

}


