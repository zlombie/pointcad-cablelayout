﻿using Layout.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Area
{
    public class eArea : eBaseItem
    {
        private string _scaleString;
        public string ScaleString
        {
            get
            {
                return string.IsNullOrEmpty(_scaleString) ? eConstants.NullString : _scaleString;
            }
            set
            {
                if (_scaleString!=value)
                {
                    if (value == eConstants.NullString)
                        value = string.Empty;
                    _scaleString = value;
                    ParseScaleString();
                }
            }
        }

        private double _scale;
        public double Scale
        {
            get
            {
                return _scale;
            }
            private set
            {
                if (value == 0)
                {
                    ScaleStatusMessage = $"Невозможно определить масштаб листа {Name}";
                    _scaleString = eConstants.NullString;
                    e3c.app.PutError(0, ScaleStatusMessage, ID);
                }
                if (_scale != value)
                {
                    _scale = value;
                }
            }
        }


        public string ScaleStatusMessage { get; protected set; }

        private void ParseScaleString()
        {
            var strarr = ScaleString.Split(':');
            if (strarr.Length < 2 || !double.TryParse(strarr[1], out double scale))
            {
                Scale = 0;
            }
            else
            {
                Scale = scale / 1000;
            }
        }
    }
}
