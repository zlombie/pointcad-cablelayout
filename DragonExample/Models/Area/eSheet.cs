﻿using Layout.Configuration;
using Layout.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Area
{
    class eSheet : eArea
    {

        #region Constructor
        public eSheet(int sheetId)
        {
            ID = e3c.sheet.SetId(sheetId);
            Name = e3c.sheet.GetName();
            ScaleString = e3c.sheet.GetAttributeValue(eSettings.Profile.ScaleAttribute);
        } 
        #endregion

        public List<eField> Fields { get; private set; }
        public List<EItemOnLayoutAdvanced> HorizontalSegments { get; private set; }
        public List<EItemOnLayoutAdvanced> VerticalSegments { get; private set; }
        public List<EItemOnLayoutAdvanced> Devices = new List<EItemOnLayoutAdvanced>();
        public void Preview3D()
        {
            e3c.InitE3Objects();
            e3c.sheet.SetId(ID);
            GetHorizontalSegments();
            Utilites.TempFile = $"{System.IO.Path.GetTempFileName()}.png";
            e3c.sheet.ExportImage("png", 1, Utilites.TempFile);
            object xmin = null, ymin = null, ymax = null, xmax = null;
            e3c.sheet.GetWorkingArea(ref xmin, ref ymin, ref xmax, ref ymax);
            double w = (double)xmax - (double)xmin;
            double h = (double)ymax - (double)ymin;
            foreach(var segm in HorizontalSegments)
            {
                if (segm.Height == eConstants.BadHeight)
                    segm.Height = 0;
                e3c.segm.SetId(segm.ID);
                object shid = null, x = null, y = null;
                int lineCount = e3c.segm.GetLineSegments(ref shid, ref x, ref y);
                dynamic xarr = x;
                dynamic yarr = y;
                if (lineCount > 1)
                {
                    for (int j = 2; j <= lineCount; j++)
                    {
                        var line = new ELine();
                        float x1 = (float)xarr[j - 1];
                        float y1 = (float)h - (float)yarr[j - 1];
                        float x2 = (float)xarr[j];
                        float y2 = (float)h - (float)yarr[j];
                        System.Drawing.PointF p1 = new System.Drawing.PointF(x1, y1);
                        System.Drawing.PointF p2 = new System.Drawing.PointF(x2, y2);
                        line.P1 = p1;
                        line.P2 = p2;
                        segm.Lines.Add(line);
                    }
                }
            }
            e3c.DisposeE3Job();
        }

        public void GetHorizontalSegments()
        {
            e3c.sheet.SetId(ID);
            HorizontalSegments = new List<EItemOnLayoutAdvanced>();
            dynamic segmIds = null;
            int segmCnt = e3c.sheet.GetNetSegmentIds(ref segmIds);
            for (int i = 1; i <= segmCnt; i++)
            {
                HorizontalSegments.Add(new EItemOnLayoutAdvanced(segmIds[i]));
            }
        }

        public void GetVerticalSegments()
        {
            VerticalSegments = new List<EItemOnLayoutAdvanced>();
            e3c.sheet.SetId(ID);
            dynamic symIds = null;
            int symCount = e3c.sheet.GetSymbolIds(ref symIds);
            for (int i = 1; i <= symCount; i++)
            {
                e3c.sym.SetId(symIds[i]);
                string symName = e3c.sym.GetSymbolTypeName();
                if (symName == eSettings.Profile.UpSymbol || symName == eSettings.Profile.DownSymbol || symName == eSettings.Profile.ChangeTypeSymbol)
                {
                    dynamic pinIds = null;
                    int pinCount = e3c.sym.GetPinIds(ref pinIds);
                    if (pinCount > 0)
                    {
                        e3c.core.SetId(pinIds[1]);
                        int segmCount = e3c.core.GetNetSegmentIds(ref pinIds);
                        if (segmCount == 2)
                        {
                            e3c.segm.SetId(pinIds[1]);
                            string attr1 = e3c.segm.GetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute);
                            e3c.segm.SetId(pinIds[2]);
                            string attr2 = e3c.segm.GetAttributeValue(eSettings.Profile.NetSegmentHeightAttribute);
                            double val1 = Utilites.ParseHeight(attr1);
                            double val2 = Utilites.ParseHeight(attr2);
                            if (val1 != eConstants.BadHeight && val2 != eConstants.BadHeight)
                            {
                                var val = Math.Abs(val1 - val2);
                                if (val == 0)
                                {
                                    e3c.app.PutWarning(0, string.Format("У символа с ID={0} значение высотного перехода равно нулю", symIds[i]), symIds[i]);
                                }
                                else
                                {
                                    var tempVertSegm = new EItemOnLayoutAdvanced(symIds[i]);
                                    tempVertSegm.Length = val;
                                    VerticalSegments.Add(tempVertSegm);
                                }
                                string delta = val.ToString().Replace(',', '.');
                                if (delta.Length == 1)
                                    delta += ".000";
                                if (delta.Length == 3)
                                    delta += "00";
                                if (delta.Length == 3)
                                    delta += "0";
                                string res = "";
                                if (symName == eSettings.Profile.UpSymbol)
                                    res = "+" + delta.ToString();
                                else
                                    res = "-" + delta.ToString();
                                e3c.sym.SetAttributeValue(eSettings.Profile.SymbolHeightAttribute, res);
                            }
                        }
                    }
                }
            }
        }

        public void GetFields()
        {
            Fields = new List<eField>();
            dynamic fieldIds = null;
            int fldCnt = e3c.sheet.GetSymbolIds(ref fieldIds);
            for (int i = 1; i <= fldCnt; i++)
            {
                if (e3c.fld.SetId(fieldIds[i])>0)
                {
                    Fields.Add(new eField(fieldIds[i], this));
                }
            }
        }

        public void GetAllDevices()
        {
            dynamic devIds = null;
            e3c.sheet.SetId(Utilites.CurrentSheet.ID);
            int symCnt = e3c.sheet.GetSymbolIds(ref devIds);
            Devices.Clear();
            for (int i = 1; i <= symCnt; i++)
            {
                e3c.dev.SetId(devIds[i]);
                if (e3c.dev.IsView() > 0)
                {
                    e3c.dev.SetId(e3c.dev.GetOriginalId());
                    Devices.Add(new EItemOnLayoutAdvanced(e3c.dev.GetId()));
                }
            }
        }

        public List<eMaterialDevice> GetMaterials()
        {
            List<eMaterialDevice> materials = new List<eMaterialDevice>();
            GetFields();
            GetHorizontalSegments();
            GetVerticalSegments();
            List<EItemOnLayoutAdvanced> layingSegments = new List<EItemOnLayoutAdvanced>();
            List<EItemOnLayoutAdvanced> sectionSegments = new List<EItemOnLayoutAdvanced>();
            //Горизонтальные участки
            foreach (var tempSegm in HorizontalSegments)
            {
                double currentScale = Scale;
                eField currentField = Fields.Find(f => f.InsidePathSegmentsIds.Contains(tempSegm.ID));
                if (currentField != null)
                {
                    currentScale = currentField.Scale;
                }
                tempSegm.Length = tempSegm.Length * currentScale;
                if (tempSegm.Laying != null && tempSegm.Length >= eSettings.Profile.CalcMinLength)
                {
                    if (tempSegm.Laying.Materials.Count > 0)
                    {
                        layingSegments.Add(tempSegm);
                    }
                    if (tempSegm.Laying.SectionType == "Металлоконструкции")
                    {
                        sectionSegments.Add(tempSegm);
                    }
                    if (tempSegm.Laying.SectionType == "Траншея")
                    {
                        sectionSegments.Add(tempSegm);
                    }
                }
            }
            //Вертикальные участки
            foreach (var tempSegm in VerticalSegments)
            {
                if (tempSegm.Laying != null && tempSegm.Length >= eSettings.Profile.CalcMinLength)
                {
                    if (tempSegm.Laying.Materials.Count > 0)
                    {
                        layingSegments.Add(tempSegm);
                    }
                    if (tempSegm.Laying.SectionType == "Металлоконструкции")
                    {
                        sectionSegments.Add(tempSegm);
                    }
                    if (tempSegm.Laying.SectionType == "Траншея")
                    {
                        sectionSegments.Add(tempSegm);
                    }
                }
            }


            //Расчет материалов для участков трасс
            foreach (var segm in layingSegments)
            {
                //Материалы способов прокладки
                foreach (var mcomp in segm.Laying.Materials)
                {
                    var fnd = materials.FirstOrDefault(f => f.Component.Component == mcomp.Component);
                    if (fnd == null)
                    {
                        materials.Add(new eMaterialDevice(mcomp, segm.Length, 1));
                    }
                    else
                    {
                        fnd.Quantity += mcomp.GetQuantity(segm.Length, 1);
                        fnd.Segments++;
                    }
                }
            }
            //Материалы для металлоконструкций и траншей
            foreach (var segm in sectionSegments)
            {
                segm.GetSectionMaterials();
                //Материалы разрезов (траншеи и металлоконструкции)
                foreach (var material in segm.NonLayingMaterials)
                {
                    var fnd = materials.FirstOrDefault(f => f.Component.Component == material.Component);
                    if (fnd == null)
                    {
                        materials.Add(new eMaterialDevice(material, segm.Length, 1));
                    }
                    else
                    {
                        fnd.Quantity += material.GetQuantity(segm.Length, 1);
                        fnd.Segments++;
                    }
                }
            }
            e3c.DisposeE3Job();
            materials.ForEach(f => f.RoundQuantity());
            return materials;
        }

        public List<string> GetAllHeights()
        {
            e3c.InitE3Objects();
            List<string> res = new List<string>();
            GetHorizontalSegments();
            if (HorizontalSegments.Count > 0)
            {
                res.AddRange(Utilites.CurrentSheet.HorizontalSegments.FindAll(f => !string.IsNullOrEmpty(f.HeightString) && f.HeightString != eConstants.NullString).Select(s => s.HeightString));
            }
            GetAllDevices();
            res.AddRange(Devices.FindAll(f => !string.IsNullOrEmpty(f.HeightString) && f.HeightString!=eConstants.NullString).Select(s => s.HeightString));
            res = res.Distinct().ToList();
            res.Sort();
            e3c.DisposeE3Job();
            return res;
        }

        public void ToE3()
        {
            e3c.InitE3Objects();
            e3c.sheet.SetId(ID);
            e3c.sheet.SetAttributeValue(eSettings.Profile.ScaleAttribute, ScaleString);
            e3c.DisposeE3Job();
        }
    }
}
