﻿using Layout.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Layout.Area
{
    class eField : eArea
    {
        public eField(int fieldId, eSheet sheet)
        {
            InsidePathSegmentsIds = new List<int>();
            ID = e3c.fld.SetId(fieldId);
            Name = e3c.fld.GetName();
            Sheet = sheet;
            ScaleString = e3c.fld.GetAttributeValue(eSettings.Profile.ScaleAttribute);
            dynamic fieldSegmIds = null;
            int segmcnt = e3c.fld.GetInsideNetSegmentIds(ref fieldSegmIds);
            for (int j = 1; j <= segmcnt; j++)
            {
                InsidePathSegmentsIds.Add(fieldSegmIds[j]);
            }
        }

        public List<int> InsidePathSegmentsIds { get; private set; } 
        public eSheet Sheet { get; private set; }

        public new double Scale
        {
            get
            {
                if (base.Scale != 0)
                {
                    ScaleStatusMessage = string.Empty;
                    return base.Scale;
                }
                ScaleStatusMessage = $"Невозожно определить масштаб для области листа ID={ID}. При расчетах будет использован машстаб листа {Sheet.Name}";
                e3c.app.PutWarning(0, ScaleStatusMessage, ID);
                return Sheet.Scale;
            }
        }

    }
}
